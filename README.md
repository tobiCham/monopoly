## Overview
A maths based educational version of Monopoly. Questions along the way will test your maths ability with basic calculations.

## Features
* 3 different levels of difficulty
* 6 available pieces
* Multiple games with save files
* 25 different chance and community chest cards
* Fully automated Computer AI
* Trades
* Mortgaging
* Building hotels and apartments

Images can be found in the [demo folder](/demo)!

## Requirements
Java 8 or higher. Download via the "Downloads" tab