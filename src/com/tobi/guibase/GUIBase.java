package com.tobi.guibase;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints.Key;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 * init() needs to be called first, followed by open() to start running the
 * application
 */
public abstract class GUIBase {
	
	private boolean initialized = false;
	private JFrame frame;
	private JEditorPane pane;
	private int updateFps = 30, renderFps = 30;
	private Mouse mouse;
	private boolean maintainAspectRatio = false;
	private Color backgroundColor = Color.WHITE;
	private boolean maximised, fullscreen;
	private String title;
	private int renderX, renderY, renderWidth, renderHeight;
	private boolean ignoresFocused = false;
	private RenderSetting renderSetting = RenderSetting.DEFAULT;
	
	private UpdateTicker updateTicker;
	private UpdateTicker renderTicker;
	
	private int initialWidth, initialHeight;
	
	public GUIBase() {
		//Initialise the look and feel which sets the styling depending on what OS is running it
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.err.println("Failed to load look and feel: " + e.getMessage());
		}
	}
	
	/**
	 * Initialises the application. Needs to be called before open() can be called.
	 * Automatically calls the loadData() method.
	 */
	public void init(int width, int height, boolean resizable, boolean maximised, boolean fullscreen, String title) {
		initialWidth = width;
		initialHeight = height;
		this.maximised = maximised;
		this.fullscreen = fullscreen;
		this.title = title;
		frame = new JFrame();
		
		//Every time the paint method is called, it renders the display
		pane = new JEditorPane() {
			
			public void paint(Graphics g) {
				interalRender(g);
			}
		};
		if (fullscreen) frame.setUndecorated(true); //Remove the border if full screen
		pane.setPreferredSize(new Dimension(width, height)); //Set the size to the size specified
		frame.add(pane);
		
		frame.pack();
		
		frame.setTitle(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(resizable);
		
		frame.addWindowListener(new WindowEvents(this));
		
		//Initialise the keyboard listener
		Keyboard keyboard = new Keyboard(this);
		pane.addKeyListener(keyboard);
		
		//Initialise the mouse listener
		mouse = new Mouse(this);
		pane.addMouseListener(mouse);
		pane.addMouseMotionListener(mouse);
		pane.addMouseWheelListener(mouse);
		
		loadData();
		initialized = true;
	}
	
	//Applies rendering hints which change how the game is rendered
	public static void applySettings(Graphics2D g, RenderSetting setting) {
		for(Key key : setting.getRenderOptions().keySet()) {
			g.setRenderingHint(key, setting.getRenderOptions().get(key));
		}
	}
	
	/**
	 * Init must be called before this method. Opens the window and sets it visible, and also starts the update thread
	 * @throws IllegalStateException if not initialised;
	 */
	public void open() {
		if(!initialized) throw new IllegalStateException("Must be initialized before method can be called");
		
		if (frame == null || frame.isShowing()) return;
		
		//Create the updater for rendering.
		renderTicker = new UpdateTicker(renderFps, (time) -> {
			//Only render the game if either the window is focused or ignoreFocused is true
			if (frame.isFocused() || ignoresFocused) pane.repaint();
		});
		//Create the updater for the game update
		updateTicker = new UpdateTicker(updateFps, (time) -> {
			//Only update the game if either the window is focused or ignoreFocused is true
			if (frame.isFocused() || ignoresFocused) update(time);
		});
		
		if (maximised) frame.setExtendedState(JFrame.MAXIMIZED_BOTH); //Maximise the window if the option is enabled
		frame.setLocationRelativeTo(null); //Centre the window in the screen
		
		//Start the updaters
		renderTicker.start();
		updateTicker.start();
		
		frame.setVisible(true);
		if (fullscreen) {
			//If the full screen option is set, set the window full screen.
			 GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
			 if(device.isDisplayChangeSupported()) {
				 device.setDisplayMode(new DisplayMode(initialWidth, initialHeight, device.getDisplayMode().getBitDepth(), device.getDisplayMode().getRefreshRate()));
			 }
			 if(device.isFullScreenSupported()) device.setFullScreenWindow(frame);
		}
	}
	
	private void interalRender(Graphics g2) {
		Graphics2D g = (Graphics2D) g2;
		
		//Apply the current render settings to the graphics object
		applySettings(g, renderSetting);
		
		int paneWidth = pane.getWidth();
		int paneHeight = pane.getHeight();
		
		//Clear the background
		g.clearRect(0, 0, paneWidth, paneHeight);
		g.setColor(backgroundColor);
		g.fillRect(0, 0, pane.getWidth(), pane.getHeight());
		
		//If the frame isn't resizable, render to the screen and return
		if (!frame.isResizable()) {
			render(g);
			return;
		}
		
		float scaleFactorX;
		float scaleFactorY;
		
		//If the program should maintain its aspect ratio, do some calculations to offset the rendering and set the scale
		//so that the game is rendered in the centre of the screen
		if (maintainAspectRatio) {
			float scaleFactor = (float) paneWidth / getInitialWidth();
			int newHeight = (int) (getInitialHeight() * scaleFactor);
			if (newHeight > paneHeight) scaleFactor = (float) paneHeight / getInitialHeight();
			int newWidth = (int) (scaleFactor * getInitialWidth());
			newHeight = (int) (scaleFactor * getInitialHeight());
			
			renderX = (paneWidth / 2) - (newWidth / 2);
			renderY = (paneHeight / 2) - (newHeight / 2);
			renderWidth = newWidth;
			renderHeight = newHeight;
			scaleFactorX = scaleFactor;
			scaleFactorY = scaleFactor;
		} else {
			//Otherwise, set the scale so that the rendering is stretched to the size of the screen
			renderX = 0;
			renderY = 0;
			renderWidth = pane.getWidth();
			renderHeight = pane.getHeight();
			scaleFactorX = (float) renderWidth / getInitialWidth();
			scaleFactorY = (float) renderHeight / getInitialHeight();
		}
		
		//Translate and scale where everything is being rendered
		g.translate(renderX, renderY);
		g.scale(scaleFactorX, scaleFactorY);
		
		//Ensures that only area that should be visible can be rendered to
		g.setClip(0, 0, getInitialWidth(), getInitialHeight());
		//Renders the whole game
		render(g);
		//Undo the scale and translation
		g.scale(1f/scaleFactorX, 1f/scaleFactorY);
		g.translate(-renderX, -renderY);
	}
	
	/**
	 * Will render the game FPS times per second
	 */
	public abstract void render(Graphics g);
	
	/**
	 * Will try to update FPS times per second
	 */
	public abstract void update(int updateTime);
	
	public int getUpdateFps() {
		return updateFps;
	}

	public void setUpdateFps(int updateFps) {
		this.updateFps = updateFps;
		if(updateTicker != null) updateTicker.setFPS(updateFps);
	}

	public int getRenderFps() {
		return renderFps;
	}

	public void setRenderFps(int renderFps) {
		this.renderFps = renderFps;
		if(renderTicker != null) renderTicker.setFPS(renderFps);
	}

	public JFrame getFrame() {
		return frame;
	}
	
	public UpdateTicker getUpdateTicker() {
		return updateTicker;
	}
	
	public UpdateTicker getRenderTicker() {
		return renderTicker;
	}
	
	public JEditorPane getRenderPane() {
		return pane;
	}
	
	public Mouse getMouse() {
		return mouse;
	}
	
	public int getInitialWidth() {
		return initialWidth;
	}
	
	public int getInitialHeight() {
		return initialHeight;
	}
	
	public boolean doesMaintainAspectRatio() {
		return maintainAspectRatio;
	}
	
	public void setMaintainAspectRatio(boolean maintainAspectRatio) {
		this.maintainAspectRatio = maintainAspectRatio;
	}
	
	public Color getBackgroundColor() {
		return backgroundColor;
	}
	
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
	public String getTitle() {
		return title;
	}
	
	public int getRenderX() {
		return renderX;
	}
	
	public int getRenderY() {
		return renderY;
	}
	
	public int getRenderWidth() {
		return renderWidth;
	}
	
	public int getRenderHeight() {
		return renderHeight;
	}
	
	public boolean doesIgnorsFocused() {
		return ignoresFocused;
	}

	public void setIgnoresFocused(boolean ignoresFocussed) {
		this.ignoresFocused = ignoresFocussed;
	}

	public RenderSetting getRenderSetting() {
		return renderSetting;
	}

	public void setRenderSetting(RenderSetting renderSetting) {
		this.renderSetting = renderSetting;
	}

	public void resize(int newWidth, int newHeight) {
		this.initialWidth = newWidth;
		this.initialHeight = newHeight;
	}
	
	protected void loadData() {}
	
	public void keyPress(int keycode, char ch) {}
	
	public void keyRelease(int keycode, char ch) {}
	
	public void mouseClick(int mouseButton) {}
	
	public void mouseRelease(int mouseButton) {}
	
	public void mouseDrag() {}
	
	public void mouseScroll(int scrollPosition) {}
	
	public void windowClosed() {}
}
