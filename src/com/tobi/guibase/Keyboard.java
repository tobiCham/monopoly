package com.tobi.guibase;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

public class Keyboard implements KeyListener {
	
	private GUIBase app; //Instance to forward keyboard events on to
	public static Set<Integer> keysDown = new HashSet<>(); //Set of all the key IDs which are currently down
	
	public Keyboard(GUIBase application) {
		this.app = application;
	}

	@Override
	public void keyPressed(KeyEvent event) {
		//Add the key to the set of keys down, and forward the event to the game
		keysDown.add(event.getKeyCode());
		app.keyPress(event.getKeyCode(), event.getKeyChar());
	}
	
	@Override
	public void keyReleased(KeyEvent event) {
		//Remove the key from the set of keys down, and forward the event to the game
		keysDown.remove(event.getKeyCode());
		app.keyRelease(event.getKeyCode(), event.getKeyChar());
	}
	
	@Override
	public void keyTyped(KeyEvent event) {}
	
	/**
	 * @return Whether the key is currently pressed
	 */
	public static boolean isKeyDown(int key) {
		return keysDown.contains(key);
	}
	
	/**
	 * Removes the key from the list of keys which are down
	 */
	public static void releaseKey(int key) {
		keysDown.remove(key);
	}
	
	public static Set<Integer> getKeysDown() {
		return keysDown;
	}
	
	public static void releaseAllKeys() {
		keysDown.clear();
	}
}
