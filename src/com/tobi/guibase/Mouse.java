package com.tobi.guibase;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.HashSet;
import java.util.Set;

public class Mouse extends MouseAdapter implements MouseWheelListener {
	
	private int mouseX; //X-coordinate of the mouse
	private int mouseY; //Y-coordinate of the mouse
	public static final int MOUSE_LEFT = 1;
	public static final int MOUSE_MIDDLE = 2;
	public static final int MOUSE_RIGHT = 3;
	private Set<Integer> buttonsDown = new HashSet<>(); //Set of the mouse buttons which are downs
	private GUIBase base; //Instance to forward events to
	
	public Mouse(GUIBase base) {
		this.base = base;
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		//When the mouse is moved, update the coordinates
		mouseX = e.getX();
		mouseY = e.getY();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		//When the mouse is dragged, add the button which is down for the drag to the list of buttons down,
		//and update the mouse coordinates
		buttonsDown.add(e.getButton());
		mouseX = e.getX();
		mouseY = e.getY();
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		//When a button is clicked, update the mouse coordinates, add the button clicked to the list of buttons down,
		//and forward the event to the game
		mouseX = e.getX();
		mouseY = e.getY();
		buttonsDown.add(e.getButton());
		base.mouseClick(e.getButton());
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		//When a button is released on the mouse, update the mouse coordinates, remove the button from the list of buttons down,
		//and forward the event to the game
		mouseX = e.getX();
		mouseY = e.getY();
		buttonsDown.remove(e.getButton());
		base.mouseRelease(e.getButton());
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		//When the mouse is scrolled, update the mouse coordinates and forward the scroll event to the game
		mouseX = e.getX();
		mouseY = e.getY();
		base.mouseScroll(e.getWheelRotation());
	}
	
	/**
	 * This returns the exact X coordinate of the mouse; doesn't matter about
	 * window size.
	 */
	public int getExactX() {
		return mouseX;
	}
	
	/**
	 * This returns the exact Y coordinate of the mouse; doesn't matter about
	 * window size.
	 */
	public int getExactY() {
		return mouseY;
	}
	
	/**
	 * This returns the relative X coordinate of the mouse.
	 */
	public int getX() {
		//If the game can't be resized, the mouse coordinates are the correct coordinates
		if(!base.getFrame().isResizable()) return mouseX;
		
		if (base.doesMaintainAspectRatio()) {
			//If aspect ratio is maintained, calculate where the x-coordinate is relative to the render area
			double presumeX = (double) mouseX - (base.getRenderX());
			double ratio = (double) base.getRenderWidth() / base.getInitialWidth();
			return (int) (presumeX / ratio);
		} else {
			//Otherwise, scale the x-coordinate so it is relative to the render area
			double ratio = base.getRenderPane().getWidth() / (double) base.getInitialWidth();
			double relative = (double) mouseX / ratio;
			return (int) relative;
		}
	}
	
	/**
	 * This returns the relative Y coordinate of the mouse.
	 */
	public int getY() {
		//If the game can't be resized, the mouse coordinates are the correct coordinates
		if(!base.getFrame().isResizable()) return mouseY;
				
		if (base.doesMaintainAspectRatio()) {
			//If aspect ratio is maintained, calculate where the y-coordinate is relative to the render area
			double presumeY = (double) mouseY - (base.getRenderY());
			double ratio = (double) base.getRenderHeight() / base.getInitialHeight();
			return (int) (presumeY / ratio);
		} else {
			//Otherwise, scale the y-coordinate so it is relative to the render area
			double ratio = base.getRenderPane().getHeight() / (double) base.getInitialHeight();
			double relative = (double) mouseY / ratio;
			return (int) relative;
		}
	}
	
	public void setX(int x) {
		this.mouseX = x;
	}
	
	public void setY(int y) {
		this.mouseY = y;
	}
	
	public void releaseButton(int button) {
		buttonsDown.remove(button);
	}
	
	public void releaseAllButtons() {
		buttonsDown.clear();
	}
	
	public boolean isButtonDown(int button) {
		return buttonsDown.contains(button);
	}
}
