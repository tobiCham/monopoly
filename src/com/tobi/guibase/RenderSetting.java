package com.tobi.guibase;

import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.util.HashMap;
import java.util.Map;

public class RenderSetting {

	//Map of all the render options for this object
	private Map<Key, Object> renderOptions = new HashMap<>();
	private String name; //Name of the setting
	
	public RenderSetting(String name) {
		this.name = name;
	}
	
	public RenderSetting(Map<Key, Object> options, String name) {
		this.renderOptions.putAll(options);
		this.name = name;
	}
	
	public RenderSetting addOption(Key key, Object value) {
		renderOptions.put(key, value);
		return this;
	}
	
	public Map<Key, Object> getRenderOptions() {
		return renderOptions;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public static final RenderSetting PERFORMANCE = new RenderSetting("Performance");
	public static final RenderSetting DEFAULT = new RenderSetting("Default");
	public static final RenderSetting BETTER = new RenderSetting("Better");
	public static final RenderSetting GOOD = new RenderSetting("Good");
	public static final RenderSetting FANTASTIC = new RenderSetting("Fantastic");
	public static final RenderSetting ULTRA = new RenderSetting("Ultra");
	
	//Set up the in built render settings. Each setting has different settings which affect how the game is rendered
	static {
		ULTRA.addOption(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		ULTRA.addOption(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		ULTRA.addOption(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		ULTRA.addOption(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		
		ULTRA.addOption(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		ULTRA.addOption(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		ULTRA.addOption(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
		ULTRA.addOption(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		
		FANTASTIC.addOption(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		FANTASTIC.addOption(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		FANTASTIC.addOption(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		FANTASTIC.addOption(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		
		FANTASTIC.addOption(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		FANTASTIC.addOption(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		FANTASTIC.addOption(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
		FANTASTIC.addOption(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		
		GOOD.addOption(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		GOOD.addOption(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		GOOD.addOption(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		GOOD.addOption(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		
		GOOD.addOption(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
		GOOD.addOption(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
		GOOD.addOption(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
		GOOD.addOption(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
		
		BETTER.addOption(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		BETTER.addOption(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		BETTER.addOption(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		BETTER.addOption(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		
		BETTER.addOption(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		BETTER.addOption(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		BETTER.addOption(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
		BETTER.addOption(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		
		PERFORMANCE.addOption(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		PERFORMANCE.addOption(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		PERFORMANCE.addOption(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		PERFORMANCE.addOption(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
		
		PERFORMANCE.addOption(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
		PERFORMANCE.addOption(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
		PERFORMANCE.addOption(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
		PERFORMANCE.addOption(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
	}
}
