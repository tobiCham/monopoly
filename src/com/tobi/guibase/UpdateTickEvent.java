package com.tobi.guibase;

public interface UpdateTickEvent {
	
	/**
	 * Called when the {@link UpdateTicker} updates
	 * @param time Time, in nano seconds, of the last update
	 */
	void onTickEvent(int time);
}
