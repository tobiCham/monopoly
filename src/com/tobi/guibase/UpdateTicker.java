package com.tobi.guibase;
import java.util.ArrayList;
import java.util.List;

public class UpdateTicker {
	
	private int fps = 30; //Target FPS, default to 30
	private UpdateTickEvent updateTickEvent; //Event to run each tick
	private Thread updateThread; //Use a separate thread so it does not lock the main thread
	private boolean capFps = true; //Whether to set an FPS limit, true by default
	private List<Integer> currentTickTimes = new ArrayList<Integer>(); //List of previous times - used to calculate the delay
	private int useTime = 1000000000 / fps; //Time to between each update
	private boolean stopping = false; //Whether to stop the update ticker
	
	public UpdateTicker(int fps, UpdateTickEvent updateTickEvent) {
		this.fps = fps;
		this.updateTickEvent = updateTickEvent;
	}
	
	public void start() {
		if(updateThread != null) return;
		
		stopping = false;
		useTime = 1000000000 / fps; //Set the delay based on the FPS
		
		//Set the update thread to a new thread, and start it
		updateThread = new Thread(() -> startUpdate()); 
		updateThread.start();
	}
	
	public void stop() {
		if(updateThread == null || !updateThread.isAlive()) return; //If the ticker isn't running, cancel
		stopping = true; //Set stopping to true to stop the update loop
		
		//Try to stop the thread
		try {
			updateThread.join(1);
			updateThread.interrupt();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		updateThread = null;
	}
	
	public void setFPS(int newFps) {
		//Change the FPS, and clear the currentTickTimes
		this.fps = newFps;
		useTime = 1000000000 / fps;
		currentTickTimes.clear();
	}
	
	private void startUpdate() {
		while(!stopping) {
			long time1 = System.nanoTime(); //Get the current system time
			if(useTime == 0) useTime = 1; //If the use time is 0, set it to 1 to prevent a divide by zero exception
			updateTickEvent.onTickEvent(useTime); //Call the tick event with the last update time
			long time2 = System.nanoTime(); //Get the current system time after calling the tick event
			int last = (int) (time2 - time1);
			
			//If the FPS isn't being capped, the last update time is the time taken to call the event
			if(!capFps) {
				useTime = last;
				continue;
			}
			currentTickTimes.add(last); //Add the last time taken to the list of tickTimes
			
			//If the currentTickTimes size is more than 5 times the fps (should be 5 seconds worth), remove the first element
			if(currentTickTimes.size() > fps * 5) currentTickTimes.remove(0);
			
			//The time to delay if the tick event takes no time
			int idealTime = 1000000000 / fps;
			
			long total = 0; //Sums all the tick times
			for(int t : currentTickTimes) total += t;
			
			//Calculates the average time for a tick to occur
			int average = (int) (total / currentTickTimes.size());
			
			//The time to wait is the ideal time minus the average time for a tick
			int toWait = idealTime - average;
			
			//If toWait is less than 0, then the ticker is not able to update fast enough, so don't delay
			if(toWait <= 0) {
				useTime = last;
				continue;
			}
			delay(toWait); //Delay the time calculated
			
			//Set the use time to the time taken for the whole loop, as this is the time between each update which
			//should be forwarded to the tick event next update
			long time3 = System.nanoTime();
			useTime = (int) (time3 - time1);
	
		}
	}
	
	/**
	 * Waits the number of nanoseconds specified
	 */
	private void delay(long nano) {
		long time = System.nanoTime(); //Gets the current time
		
		while(true) {
			//Constantly loop until the time elapsed is more than the time specified as a parameter
			long current = System.nanoTime();
			long elapsed = current - time;
			if(elapsed >= nano) return;
		}
	}
	
	public void setCapFPS(boolean cap) {
		this.capFps = cap;
	}
	
	public boolean doesCapFPS() {
		return capFps;
	}
}
