package edu.monopoly;

public class AIPercentages {

	//List of percentages to work with in the AI. 0.5 = 50%.
	
	public static final float ANSWER_JAIL = 0.6f; //60% chance of correctly answering a question while in Jail
	public static final float BUILD = 1f; //70% chance of building houses on a turn, if able to.
	public static final float UNMORTGAGE = 0.65f; //65% chance of unmortgaging a property on a turn
	public static final float INITIATE_TRADE = 0.4f; //40% chance of starting a trade.
}
