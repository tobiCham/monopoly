package edu.monopoly;

public enum Difficulty {

	//List of difficulties in the game. No data associated, simply constants to use.
	EASY, MEDIUM, HARD
}
