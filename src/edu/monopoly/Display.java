package edu.monopoly;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.tobi.guibase.GUIBase;
import com.tobi.guibase.RenderSetting;

import edu.monopoly.graphic.Transition;
import edu.monopoly.screen.Screen;
import edu.monopoly.util.GameResources;

public class Display extends GUIBase {

	private Monopoly game;
	
	public Display(Monopoly game) {
		this.game = game;
	}
	
	@Override
	public void render(Graphics g) {
		g.setFont(new Font(GameResources.font.getFontName(), Font.PLAIN, 12)); //Set the font size to 12, using the font loaded in
		
		//Clears the background to black
		g.setColor(Color.black);
		g.fillRect(0, 0, getInitialWidth(), getInitialHeight());
		
		try {
			//Renders the selected screen if it isn't null
			if (Screen.getSelectedScreen() != null) Screen.getSelectedScreen().render(g);
			Transition.updateAll(g, game); //Update and render all the transitions
		} catch(Exception e) {
			e.printStackTrace(); //Print out the error on the console if there were any during rendering the screen or animations
		}
	}

	@Override
	public void update(int updateTime) {
		try {
			//Update the selected screen if it isn't null
			if (Screen.getSelectedScreen() != null) Screen.getSelectedScreen().update(updateTime);
		} catch(Exception e) {
			e.printStackTrace(); //Print out the error on the console if there were any during updating the screen
		}
	}
	
	//These methods below are called from the super class (GUIBase) whenever an event occurs from the keyboard or mouse
	//Each of them will simply forward the event onto the current screen, if there is one.
	@Override
	public void keyPress(int keycode, char ch) {
		Screen screen = Screen.getSelectedScreen();
		if(screen == null) return; 
		screen.keyPressed(keycode, ch);
	}
	
	@Override
	public void keyRelease(int keycode, char ch) {
		Screen screen = Screen.getSelectedScreen();
		if(screen == null) return; 
		screen.keyReleased(keycode, ch);
	}
	
	@Override
	public void mouseClick(int mouseButton) {
		Screen screen = Screen.getSelectedScreen();
		if(screen == null) return; 
		screen.mouseClicked(mouseButton);
	}
	
	@Override
	public void mouseRelease(int mouseButton) {
		Screen screen = Screen.getSelectedScreen();
		if(screen == null) return; 
		screen.mouseReleased(mouseButton);
	}
	
	@Override
	public void mouseScroll(int scrollPosition) {
		Screen screen = Screen.getSelectedScreen();
		if(screen == null) return; 
		screen.mouseScrolled(scrollPosition);
	}
	
	/**
	 * Changes the render settings, and also will notify the board class.
	 */
	@Override
	public void setRenderSetting(RenderSetting renderSetting) {
		RenderSetting current = getRenderSetting();
		super.setRenderSetting(renderSetting);
		
		if(game.getCurrentGame() != null) {
			game.getCurrentGame().getBoard().renderSettingChanged(current, renderSetting);
		}
	}
}
