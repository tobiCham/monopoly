package edu.monopoly;

//List of constants for the game
public class MoneyValues {

	public static int PASS_GO = 200; //When a player passes go
	
	public static int PAY_GET_OUT_OF_JAIL = 50; //After 3 failed attempts at a question, are forced to pay this
	
	public static int INCOME_TAX = 200; //Money to pay when landing on Income Tax
	public static int LUXURY_TAX = 100; //Money to pay when landing on Luxury Tax
}
