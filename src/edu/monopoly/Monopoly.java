package edu.monopoly;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.util.List;

import com.tobi.guibase.RenderSetting;

import edu.monopoly.card.Cards;
import edu.monopoly.player.PieceType;
import edu.monopoly.property.Properties;
import edu.monopoly.saving.GameProperties;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenLoading;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.Screens;
import edu.monopoly.screen.options.ScreenOptions;
import edu.monopoly.sound.SoundOptions;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.FileUtils;
import edu.monopoly.util.FontLoader;
import edu.monopoly.util.GameResources;

public class Monopoly {

	private static Monopoly game;
	
	private ScreenPlay currentGame; //Current game in progress. Can be null
	private Display display;

	//Main method, and entry point of the application. Creates a new monopoly instance, and then starts the game
	public static void main(String[] args) {
		game = new Monopoly();
		game.load();
	}

	private void load() {
		display = new Display(this); //Create an instance of the display class
		loadProperties(); //Loads in game settings from the properties file.
		
		GameResources.font = FontLoader.loadFont("Font.ttf"); //Loads in the font used in the game
		
		//Initialise the loading screen, then open the window
		ScreenLoading loading = new ScreenLoading(this);
		loading.init(); 
		openWindow();
		
		//Get the corresponding RenderSetting which the game shows from the settings properties file
		String name = GameProperties.getProperties().getString("quality");
		RenderSetting settings = ScreenOptions.getSetting(name);
		
		//Apply the render settings to the display. If its null, e.g. an invalid name was defined in the config, the Good option is used by default
		getDisplay().setRenderSetting(settings == null ? RenderSetting.GOOD : settings);
		
		Screen.setSelectedScreen(loading); //Set the current screen to the loading screen, then begin loading
		
		loading.setLoadingMessage("Loading Screens");
		Screens.init();
		
		//Loop through each screen defined in the Screens class and initialise it, then update the loading progress
		List<Screen> screens = Screens.getScreens();
		for(int i = 0; i < screens.size(); i++) {
			Screen screen = screens.get(i);
			screen.init();
			float percentage = (i + 1) / (float) screens.size();
			loading.setLoading(percentage * 0.6f);
		}
		loading.setLoading(0.1f);
		
		//Finally set the loading screen variable to the current loading screen. Could not do this before as initialising it in
		//Loop above would have reset loading information and progress
		Screens.LOADING = loading;
		
		//Load music
		loading.setLoadingMessage("Loading Music");
		Sounds.loadMusic(); 
		loading.setLoading(0.2f);
		
		//Load sound effects
		loading.setLoadingMessage("Loading Sound");
		Sounds.loadSounds();
		loading.setLoading(0.3f);
		
		//Load the pieces including their icons
		loading.setLoadingMessage("Loading Pieces");
		PieceType.init();
		loading.setLoading(0.355f);
		
		//Load chance and community chest cards
		loading.setLoadingMessage("Loading Cards");
		Cards.load();
		loading.setLoading(0.4f);
		
		loading.setLoadingMessage("Loading Properties");
		Properties.init();
		
		//Enable or disable music and sound based on the value in the config
		if(!GameProperties.getProperties().getBoolean("music")) SoundOptions.setMusicEnabled(false);
		if(!GameProperties.getProperties().getBoolean("sound")) SoundOptions.setSoundEnabled(false);
		
		Screen.setSelectedScreen(Screens.TITLE); //Loading is done! Enter into the title screen
		
	}
	
	private void loadProperties() {
		File saveFolder = FileUtils.getGameDirectory();
		//Generates the game folder if it doesn't already exist
		if(saveFolder.getParentFile() != null && !saveFolder.getParentFile().exists()) saveFolder.getParentFile().mkdirs();
		File propertiesFile = new File(saveFolder, "settings.prop");
		//Load in the properties from the file using the GameProperties class
		GameProperties.load(propertiesFile);
	}
	
	private void openWindow() {
		//Create a display 1024 pixels wide and 1024 pixels tall. 
		//As defined in the constructor, the window is resizable and set to maximised
		display.init(1024, 1024, true, true, false, "Edu-Monopoly");
		display.setMaintainAspectRatio(true); //Set the game to keep the aspect ratio. This puts the borders on the sides
		display.setBackgroundColor(Color.BLACK); //Set the default background color to black
		display.setUpdateFps(30); //Set the frames per second to limit at 30 for both rendering and updating game loop
		display.setRenderFps(30);
		display.getFrame().setIconImage(ScreenLoading.getLoadingImage()); //Set the icon for the window
		
		//Setting the size of the frame before opening it will set the size of the window to default to when the window is made smaller
		//In the beginning of the game, the display is square, and so the default size of the window is the size of the display the user
		//has, divided by 1.2 so it is slightly smaller than the size of their display. This also accomodates for toolbars.
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		display.getFrame().setSize((int) (screenSize.height / 1.2), (int) (screenSize.height / 1.2));
		
		//Finally, open the display, which will start the game loop and set the window to visible
		display.open();
	}
	
	public Display getDisplay() { return display; }
	
	public ScreenPlay getCurrentGame() { return currentGame; }
	public void setCurrentGame(ScreenPlay currentGame) { this.currentGame = currentGame; }

	public static Monopoly getGameInstance() { return game; }
}
