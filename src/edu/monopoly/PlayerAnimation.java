package edu.monopoly;

import edu.monopoly.board.Board;
import edu.monopoly.player.Player;
import edu.monopoly.saving.ConfigSection;
import edu.monopoly.screen.ScreenPlay;

public class PlayerAnimation {

	private int spaces, initialSpace;
	private final float time;
	private float timeIn = 0;
	private boolean finished = false;
	private Player player;
	private ScreenPlay screen;
	
	public PlayerAnimation(ScreenPlay screen, Player player, int spacesMoving) {
		this.spaces = spacesMoving;
		this.time = spacesMoving * 0.35f;
		this.initialSpace = player == null ? 0 : player.getBoardPosition();
		this.player = player;
		this.screen = screen;
	}
	
	public void update(double time) {
		if(finished) return; //No further updating required if already finished animation
		
		timeIn += (time / 1000000000f); //Add the last update time to the time in (divide to convert to seconds)
		
		if(timeIn >= this.time) {  //If the timeIn is more than or equal to the animation time, finish the animation
			this.timeIn = this.time;
			finished = true;
			player.setBoardPosition(initialSpace + spaces);
		} else { 
			//Otherwise, get the current space in the animation and move the piece to that. 
			//If it wasn't already on the space, call the playerPass method
			int newSpace = getBoardPosition();
			if(player.getBoardPosition() != newSpace) {
				player.setBoardPosition(getBoardPosition());
				screen.playerPass(player);
			}
		}
	}
	
	/**
	 * @return The board position of the piece currently being animated based on the time into the animation
	 */
	public int getBoardPosition() {
		return ((int) ((timeIn / time) * spaces) + initialSpace) % 40;
	}

	public boolean hasFinished() {
		return finished;
	}

	public Player getPlayer() {
		return player;
	}
	
	public int getSpacesToMove() {
		return spaces;
	}
	
	/**
	 * Method to save data for the animation
	 */
	public void save(ConfigSection section) {
		//Saves the values of the variables:
		// - spaces
		// - initialSpace
		// - timeIn
		// - finished
		// and also which player is being animated (0 for human player, 1 for computer 1, 2 for computer 2, 3 for computer 3
		section.set("spaces", spaces);
		section.set("initialSpace", initialSpace);
		section.set("timeIn", timeIn);
		section.set("finished", finished);
		Board board = screen.getBoard();
		int id = 0;
		if(board.getComputerPlayer1() == player) id = 1;
		if(board.getComputerPlayer2() == player) id = 2;
		if(board.getComputerPlayer3() == player) id = 3;
		section.set("playerID", id);
	}
	
	/**
	 * Method to load data for the animation
	 */
	public void load(ConfigSection section) {
		this.spaces = section.getInteger("spaces");
		this.initialSpace = section.getInteger("initialSpace");
		this.timeIn = section.getFloat("timeIn");
		this.finished = section.getBoolean("finished");
		int playerID = section.getInteger("playerID");
		if(playerID == 1) player = screen.getBoard().getComputerPlayer1();
		else if(playerID == 2) player = screen.getBoard().getComputerPlayer2();
		else if(playerID == 3) player = screen.getBoard().getComputerPlayer3();
		else player = screen.getBoard().getHumanPlayer();
	}
}
