package edu.monopoly;

public class Question {

	//Instance variables
	private String question;
	private int answer;
	private Difficulty difficulty; //Difficulty of the question, either Easy, Medium or Hard
	
	public Question(Difficulty difficulty, String question, int answer) {
		this.question = question;
		this.answer = answer;
		this.difficulty = difficulty;
	}

	public String getQuestion() {
		return question;
	}

	public int getAnswer() {
		return answer;
	}
	
	public Difficulty getDifficulty() {
		return difficulty;
	}
	
	@Override
	public String toString() {
		return question + ": " + answer;
	}
}
