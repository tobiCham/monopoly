package edu.monopoly;

import java.util.Random;

/**
 * Generates Maths questions for either easy, medium or hard
 */
public class QuestionManager {

	private static Random random = new Random();
	
	/**
	 * @param begin Starting number
	 * @param end Ending number
	 * @return A random number between the two numbers, inclusive
	 */
	private static int generateRandom(int begin, int end) {
		return random.nextInt(end - begin + 1) + begin;
	}
	
	/**
	 * Generates a new addition question
	 * @param begin Starting number
	 * @param end Ending number
	 * @param difficulty Difficulty to use
	 * @return A new question, with two random numbers between the begin and end, inclusive
	 */
	private static Question generateAddition(int begin, int end, Difficulty difficulty) {
		int numb1 = generateRandom(begin, end);
		int numb2 = generateRandom(begin, end);
		return new Question(difficulty, numb1 + " + " + numb2 + " = ?", numb1 + numb2);
	}
	
	/**
	 * Generates a new subtraction question
	 * @param begin Starting number
	 * @param end Ending number
	 * @param difficulty Difficulty to use
	 * @return A new question, with two random numbers between the begin and end, inclusive
	 */
	private static Question generateSubtraction(int begin, int end, Difficulty difficulty) {
		int numb1 = generateRandom(begin, end);
		int numb2 = generateRandom(begin, end);
		//If the second number is larger than the first number, swap the numbers.
		//This prevents a negative answer, for example if 4 and 10 is generated, the answer would -6
		//By swapping, the answer would be 6
		if(numb2 > numb1) {
			int temp = numb1;
			numb1 = numb2;
			numb2 = temp;
		}
		return new Question(difficulty, numb1 + " - " + numb2 + " = ?", numb1 - numb2);
	}
	
	/**
	 * Generates a new multiplication question
	 * @param begin Starting number
	 * @param end Ending number
	 * @param difficulty Difficulty to use
	 * @return A new question, with two random numbers between the begin and end, inclusive
	 */
	private static Question generateMultiplication(int begin, int end, Difficulty difficulty) {
		int numb1 = generateRandom(begin, end);
		int numb2 = generateRandom(begin, end);
		return new Question(difficulty, numb1 + " x " + numb2 + " = ?", numb1 * numb2);
	}
	
	/**
	 * Generates a new division question
	 * @param begin Starting number
	 * @param end Ending number
	 * @param difficulty Difficulty to use
	 * @return A new question, with two random numbers between the begin and end, inclusive
	 */
	private static Question generateDivision(int begin, int end, Difficulty difficulty) {
		int numb1 = generateRandom(begin, end);
		int numb2 = generateRandom(begin, end);
		return new Question(difficulty, (numb1 * numb2) + " � " + numb1 + " = ?", numb2);
	}
	
	//Generates an easy random question, out of Addition, Subtraction and Multiplication
	private static Question generateEasy() {
		int operation = random.nextInt(3);
		if(operation == 0) return generateMultiplication(2, 12, Difficulty.EASY);
		else if(operation == 1) return generateAddition(10, 50, Difficulty.EASY);
		else return generateSubtraction(10, 50, Difficulty.EASY);
	}
	
	//Generates a medium random question, out of Addition, Subtraction, Multiplication and Division
	private static Question generateMedium() {
		int operation = random.nextInt(4);
		if(operation == 0) return generateAddition(10, 50, Difficulty.MEDIUM);
		if(operation == 1) return generateSubtraction(10, 50, Difficulty.MEDIUM);
		if(operation == 2) return generateMultiplication(2, 12, Difficulty.MEDIUM);
		return generateDivision(2, 12, Difficulty.MEDIUM);
	}
	
	//Generates a hard random question, out of Addition, Subtraction, Multiplication and Division
	private static Question generateHard() {
		int operation = random.nextInt(4);
		if(operation == 0) return generateAddition(1, 1000, Difficulty.HARD);
		if(operation == 1) return generateSubtraction(1, 1000, Difficulty.HARD);
		if(operation == 2) return generateMultiplication(2, 20, Difficulty.HARD);
		return generateDivision(2, 20, Difficulty.HARD);
	}
	
	/**
	 * Generates a new question based on the difficulty. 
	 * @param difficulty Difficulty of question
	 * @return A new question
	 */
	public static Question generateRandomQuestion(Difficulty difficulty) {
		if(difficulty == Difficulty.EASY) return generateEasy();
		if(difficulty == Difficulty.MEDIUM) return generateMedium();
		else return generateHard();
	}
}
