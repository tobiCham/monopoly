package edu.monopoly;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class Test {

	private static List<File> files = new ArrayList<>();
	
    public static void main(String[] args) throws Exception {
        File file = new File("C:/Users/Tobi/Desktop/Pdf code");
        recursiveFind(file);
    }

    private static void recursiveFind(File file) {
    	if(file.isFile()) {
    		files.add(file);
    		copy(file, new File("C:/Users/Tobi/Desktop/All PDFs/" + file.getName()));
    	}
        File[] fs = file.listFiles();
        if(fs == null) return;
        for(File f : fs) {
            recursiveFind(f);
        }
    }
    
    private static void copy(File f1, File f2) {
    	try {
    		InputStream input = new FileInputStream(f1);
    		OutputStream output = new FileOutputStream(f2);
    		
    		while(true) {
    			byte[] buffer = new byte[1024];
    			int len = input.read(buffer, 0, buffer.length);
    			if(len < 0) break;
    			output.write(buffer, 0, len);
    		}
    		
    		output.flush();
    		output.close();
    		input.close();
    		
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
}
