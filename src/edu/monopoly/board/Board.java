package edu.monopoly.board;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.tobi.guibase.Mouse;
import com.tobi.guibase.RenderSetting;

import edu.monopoly.Difficulty;
import edu.monopoly.player.ComputerPlayer;
import edu.monopoly.player.HumanPlayer;
import edu.monopoly.player.PieceType;
import edu.monopoly.player.Player;
import edu.monopoly.property.ColoredProperty;
import edu.monopoly.property.Properties;
import edu.monopoly.property.Property;
import edu.monopoly.property.PropertyColors;
import edu.monopoly.property.PurchasableProperty;
import edu.monopoly.property.TransportProperty;
import edu.monopoly.property.UtilityProperty;
import edu.monopoly.saving.ConfigSection;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.Screens;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.ImageLoader;

public class Board {

	private Map<Integer, Property> properties = new HashMap<>();
	private Image boardImage;
	private Player currentPlayer;
	private HumanPlayer player;
	private ComputerPlayer ai1, ai2, ai3;
	private ScreenPlay screen;
	private PurchasableProperty overlay = null;
	private Difficulty difficulty;

	//Size for the player pieces to be rendered on the board
	private static final int PLAYER_SIZE = 120;
	
	public Board(ScreenPlay screen, String playerName, PieceType piece, Difficulty difficulty) {
		this.screen = screen;
		this.difficulty = difficulty;
		
		resetProperties(); //Clears the board and re-adds all the properties
		
		Screens.LOADING.setLoading(0.35f);
		Screens.LOADING.setLoadingMessage("Loading board");
		loadBoardImage(); //Loads in the board image
		Screens.LOADING.setLoading(0.75f);
		
		Screens.LOADING.setLoadingMessage("Loading players...");
		player = new HumanPlayer(piece, playerName, new Color(255, 150, 0));
		
		//Create a list of all the pieces which can be used, then remove the peice the player chose
		List<PieceType> typesLeft = new ArrayList<>();
		Collections.addAll(typesLeft, PieceType.values());
		typesLeft.remove(piece);

		Random random = new Random();
		//For each of the pieces, randomly select a piece from the list, then remove the selection from the list
		//So that the same piece isn't selected by two players at once.
		PieceType type1 = typesLeft.get(random.nextInt(typesLeft.size()));
		typesLeft.remove(type1);
		
		PieceType type2 = typesLeft.get(random.nextInt(typesLeft.size()));
		typesLeft.remove(type2);
		
		PieceType type3 = typesLeft.get(random.nextInt(typesLeft.size()));
		typesLeft.remove(type3);
		
		ai1 = new ComputerPlayer(type1, "Computer 1", PropertyColors.RED.getColor());
		ai2 = new ComputerPlayer(type2, "Computer 2", PropertyColors.DARK_BLUE.getColor());
		ai3 = new ComputerPlayer(type3, "Computer 3", PropertyColors.GREEN.getColor());
		currentPlayer = player; //The first player to go is the human player
		Screens.LOADING.setLoading(0.9f);
	}
	
	//Saves data to the config
	public void save(ConfigSection section) {
		int currentID = 0;
		if(currentPlayer == ai1) currentID = 1;
		if(currentPlayer == ai2) currentID = 2;
		if(currentPlayer == ai3) currentID = 3;
		section.set("currentPlayer", currentID);
		section.set("difficulty", difficulty);
	}
	
	//Loads data from the config
	public void load(ConfigSection section) {
		int currentID = section.getInteger("currentPlayer");
		if(currentID == 0) currentPlayer = player;
		else if(currentID == 1) currentPlayer = ai1;
		else if(currentID == 2) currentPlayer = ai2;
		else currentPlayer = ai3;
		this.difficulty = section.getType("difficulty", Difficulty.class);
	}
	
	public void render(Graphics g) {
		//Renders the image of the board to the size of the board (1920 x 1920)
		g.drawImage(boardImage, 0, 0, 1920, 1920, null);
		
		int size = PLAYER_SIZE;
		for(Player p : getPlayers()) {
			Rectangle bounds = getBounds(p.getBoardPosition());
			//Jail
			if(p.getBoardPosition() == 10) {
				if(p.isInJail()) bounds = new Rectangle(140, 1750, 0, 0);
				else bounds = new Rectangle(50, 1860, 0, 0);
			}
			//Draws the player piece on the board
			GraphicUtils.renderMaintainAspect(g, p.getPiece().getIcon(), bounds.x + (bounds.width / 2) - (size / 2), bounds.y + (bounds.height / 2) - (size / 2), size, size);
		}
		
		for(Player p : getPlayers()) {
			g.setColor(p.getColor());
			for(PurchasableProperty property : p.getProperties()) {
				Rectangle propertyBounds = getBounds(property);
				//Overlay a gray rectangle if the property is mortgaged
				if(property.isMortgaged()) {
					g.setColor(new Color(0, 0, 0, 100));
					GraphicUtils.fillRectangle(g, propertyBounds);
				}
				if(property instanceof ColoredProperty && p.hasBuildings((ColoredProperty) property)) {
					//If the property has buildings on it, render them here
					int houses = p.getBuildings((ColoredProperty) property);
					int houseSize = 30;
					int gap = 4;
					int length = houses == 1 || houses == 5 ? houseSize : (houses * houseSize) + (gap * (houses - 1));
					int offSet = (154 / 2) - (length / 2);
					int space = getSpace(property);
					g.setColor(Color.green);
					if(houses != 5) {
						//Draw the houses on the board as green squares.
						for(int i = 0; i < houses; i++) {
							if(space > 0 && space < 10) g.fillRect(propertyBounds.x + offSet + (i * (gap + houseSize)), propertyBounds.y + 13, houseSize, houseSize);
							if(space > 10 && space < 20) g.fillRect(propertyBounds.x + 201, propertyBounds.y + offSet + (i * (gap + houseSize)), houseSize, houseSize);
							if(space > 20 && space < 30) g.fillRect(propertyBounds.x + offSet + (i * (gap + houseSize)), propertyBounds.y + 201, houseSize, houseSize);
							if(space > 30 && space < 40) g.fillRect(propertyBounds.x + 13, propertyBounds.y + offSet + (i * (gap + houseSize)), houseSize, houseSize);
						}
					} else {
						g.setColor(Color.red);
						//Draw the hotel as a red square
						if(space > 0 && space < 10) g.fillRect(propertyBounds.x + offSet, propertyBounds.y + 13, houseSize, houseSize);
						if(space > 10 && space < 20) g.fillRect(propertyBounds.x + 201, propertyBounds.y + offSet, houseSize, houseSize);
						if(space > 20 && space < 30) g.fillRect(propertyBounds.x + offSet, propertyBounds.y + 201, houseSize, houseSize);
						if(space > 30 && space < 40) g.fillRect(propertyBounds.x + 13, propertyBounds.y + offSet, houseSize, houseSize);
					}
				}
				//Draws a rectangle with an 8 pixel border around the property on the board with the player's colour
				g.setColor(p.getColor());
				propertyBounds.x += 4;
				propertyBounds.y += 4;
				propertyBounds.height -= 8;
				propertyBounds.width -= 8;
				((Graphics2D) g).setStroke(new BasicStroke(8));
				GraphicUtils.drawRectangle(g, propertyBounds);
			}
		}
		
		if(overlay != null) {
			//Renders a black rectangle around the overlay property
			g.setColor(Color.black);
			Graphics2D g2d = (Graphics2D) g;
			g2d.setStroke(new BasicStroke(10));
			GraphicUtils.drawRectangle(g2d, getBounds(overlay));
		}
	}
	
	public void mouseClick(int button, int x, int y) {
		Rectangle mouseRect = new Rectangle(x - 1, y - 1, 2, 2);
		for(int pos : properties.keySet())  {
			Rectangle rect = getBounds(pos);
			//If mouse is inside the bounding box for the property on the board, show information for the property
			if(rect.intersects(mouseRect)) {
				Property property = properties.get(pos);
				if(property instanceof PurchasableProperty) {
					if(button == Mouse.MOUSE_LEFT) screen.showPropertyInfo((PurchasableProperty) property);
					break;
				}
			}
		}
	}
	
	/**
	 * Called when a render setting is changed in options. 
	 */
	public void renderSettingChanged(RenderSetting oldSetting, RenderSetting newSetting) {
		if(oldSetting == newSetting) return;
		loadBoardImage();
	}
	
	/**
	 * Loads the board image from resources. Will return differing image sizes depending on the current render settings.
	 */
	private void loadBoardImage() {
		RenderSetting setting = screen.getGame().getDisplay().getRenderSetting();
		int currentSize = boardImage != null ? boardImage.getWidth(null) : 0; //The current size of the image, or 0 if it is null

		//Performance is 1080x1080 pixels
		//Fantastic or Ultra is the original image
		//Anything else is 1920x1920 pixels
		
		if(setting == RenderSetting.PERFORMANCE && currentSize != 1080) boardImage = ImageLoader.resize(ImageLoader.loadImage("board.png"), 1080, 1080);
		else if(setting == RenderSetting.FANTASTIC || setting == RenderSetting.ULTRA) boardImage = ImageLoader.loadImage("board.png");
		else if(currentSize != 1920) boardImage = ImageLoader.resize(ImageLoader.loadImage("board.png"), 1920, 1920);
	}
	
	/**
	 * @return The bounding box for the property on the board. 
	 */
	public Rectangle getBounds(int boardPosition) {
		int width = 154, height = 246;
		int gap = 4;
		
		//0 for Go, 10 for Jail, 20 for free Parking, 30 for Go to Jail
		if(boardPosition == 0) return new Rectangle(1920 - height, 1920 - height, height, height);
		if(boardPosition == 10) return new Rectangle(0, 1920 - height, height, height);
		if(boardPosition == 20) return new Rectangle(0, 0, height, height);
		if(boardPosition == 30) return new Rectangle(1920 - height, 0, height, height);
		
		//Some maths to calculate the bounds. Not really important
		int rel = 9 - (boardPosition % 10);
		if(boardPosition > 0 && boardPosition < 10)  return new Rectangle((rel * (width + gap) + height + gap), 1920 - height, width, height);		
		if(boardPosition > 10 && boardPosition < 20) return new Rectangle(0, (rel * (width + gap)) + height + gap, height, width);
		
		rel = 8 - rel;
		if(boardPosition > 20 && boardPosition < 30) return new Rectangle((rel * (width + gap) + height + gap), 0, width, height);
		if(boardPosition > 30 && boardPosition < 40) return new Rectangle(1920 - height, (rel * (width + gap)) + height + gap, height, width);
		
		return new Rectangle(0, 0, 0, 0);
	}
	
	/**
	 * @return The bounds for a property. If the property isn't on the board, an empty rectangle with x=0, y=0, width=0, height=0
	 */
	public Rectangle getBounds(Property prop) {
		for(int space : properties.keySet()) if(properties.get(space) == prop) return getBounds(space);
		return new Rectangle(0, 0, 0, 0);
	}
	
	/**
	 * @return The board position for a property, or -1 if the property isn't on the board
	 */
	public int getSpace(Property property) {
		for(int space : properties.keySet()) {
			Property prop = properties.get(space);
			if(prop == property) return space;
		}
		return -1;
	}
	
	/**
	 * Adds all the properties on to the board
	 */
	private void resetProperties() {
		Properties.reset();
		properties.clear();
		
		List<Property> props = Properties.getProperties();
		for(int i = 0; i < props.size(); i++) {
			this.properties.put(i, props.get(i));
		}
	}
	
	//Calculates the rent owned when a player lands on a property
	public int getRentOwed(int diceRolled, Player landing, PurchasableProperty property) {
		Player owner = getOwner(property);
		//No rent owned if the property doesn't have an owner, the owner is the player who landed, or the property is mortgaged
		if(owner == null || owner == landing || property.isMortgaged()) return 0;
		int value = 0;
		//If the property is a Utility, return a rent based on the number rolled on the dice
		if(getColor(property) == PropertyColors.UTILITY) {
			List<PurchasableProperty> utilities = getProperties(PropertyColors.UTILITY);
			if(getOwner(utilities.get(0)) == owner && getOwner(utilities.get(1)) == owner) value = 10 * diceRolled;
			else value = 4 * diceRolled;
		} else if(getColor(property) == PropertyColors.TRANSPORT) {
			//If the property is transport, return a rent based ont he number owned
			int owned = getPropertiesOwned(PropertyColors.TRANSPORT, owner).size();
			return property.getRent(owned);
		} else if(doesPlayerOwnMonopoly(owner, getColor(property))) {
			//If the player owns all of the properties of that colour, then either double the rent.
			//However, if the player has buildings on the property, return the rent owned for that building
			if(property instanceof ColoredProperty) {
				ColoredProperty colored = (ColoredProperty) property;
				if(owner.hasBuildings(colored)) value = colored.getBuildingRent(owner.getBuildings(colored));
				else value = property.getRent() * 2;
			} else value = property.getRent() * 2;
		} else value = property.getRent();
		//If the owner of the property is in Jail, half the rent
		if(owner.isInJail()) return value / 2;
		return value;
	}
	
	/**
	 * @return Whether the player owns all the properties of that colour
	 */
	public boolean doesPlayerOwnMonopoly(Player player, PropertyColors color) {
		for(PurchasableProperty property : getProperties(color)) if(getOwner(property) != player) return false;
		return true;
	}
	
	/**
	 * @return The PropertyColor for the property, or null if no corresponding colour
	 */
	public PropertyColors getColor(Property property) {
		PropertyColors color = null;
		if(property instanceof TransportProperty) color = PropertyColors.TRANSPORT;
		else if(property instanceof UtilityProperty) color = PropertyColors.UTILITY;
		else if(property instanceof ColoredProperty) color = ((ColoredProperty) property).getColor();
		return color;
	}
	
	public void setOwner(PurchasableProperty property, Player player) {
		//Removes the property from all the players owned properties
		for(Player p : getPlayers()) p.getProperties().remove(property);
		
		//Adds the property to the player's list of properties
		player.getProperties().add(property);
	}
	
	/**
	 * @return The owner of the property, or null if no owner
	 */
	public Player getOwner(PurchasableProperty property) {
		for(Player p : getPlayers()) if(p.getProperties().contains(property)) return p;
		return null;
	}
	
	/**
	 * @return A map of the number of properties a player has for each property colour
	 */
	public Map<PropertyColors, Integer> getPropertyNumbers(Player player) {
		return getPropertyNumbers(new ArrayList<>(player.getProperties()));
	}
	
	/**
	 * @return A map of the number of properties of each colour in the list
	 */
	public Map<PropertyColors, Integer> getPropertyNumbers(List<PurchasableProperty> properties) {
		Map<PropertyColors, Integer> map = new HashMap<>();
		for(Property prop : properties) { //Loop through each property in the list
			PropertyColors color = getColor(prop); //Get the colour of the property
			if(color == null) continue;
			if(map.containsKey(color)) map.put(color, map.get(color) + 1); //If the map contains the colour, add 1 to the value
			else map.put(color, 1); //Otherwise add the colour to the map with a value of 1
		}
		return map;
	}
	
	/**
	 * @param color The property colour
	 * @return All the properties which have that colour
	 */
	public List<PurchasableProperty> getProperties(PropertyColors color) {
		List<PurchasableProperty> list = new ArrayList<>();
		if(color == null) return list;
		//Loop through each space on the board
		for(int space : properties.keySet()) {
			Property prop = properties.get(space);
			PropertyColors c = getColor(prop);
			//Add the property to the list if its the same colour as the colour specified as a parameter
			if(c != null && c == color && prop instanceof PurchasableProperty) list.add((PurchasableProperty) prop);
		}
		return list;
	}
	
	/**
	 * @return A list of all the properties which that player owns and have that colour
	 */
	public List<PurchasableProperty> getPropertiesOwned(PropertyColors color, Player player) {
		List<PurchasableProperty> list = new ArrayList<>();
		if(color == null) return list;
		//Loop through each space on the board
		for(int space : properties.keySet()) {
			Property prop = properties.get(space);
			PropertyColors c = getColor(prop);
			//Adds the property to the list if the colour is the same as the one passed as a parameter, and that the player owns the property.
			if(c != null && c == color && prop instanceof PurchasableProperty && player.getProperties().contains(prop)) list.add((PurchasableProperty) prop);
		}
		return list;
	}
	
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	public void setCurrentPlayer(Player p) {
		currentPlayer = p;
	}

	public Map<Integer, Property> getProperties() {
		return properties;
	}

	public HumanPlayer getHumanPlayer() {
		return player;
	}

	public ComputerPlayer getComputerPlayer1() {
		return ai1;
	}

	public ComputerPlayer getComputerPlayer2() {
		return ai2;
	}

	public ComputerPlayer getComputerPlayer3() {
		return ai3;
	}
	
	/**
	 * @return An array of all the players who haven't resigned
	 */
	public Player[] getPlayers() {
		List<Player> left = new ArrayList<>();
		if(!ai1.hasResigned()) left.add(ai1);
		if(!ai2.hasResigned()) left.add(ai2);
		if(!ai3.hasResigned()) left.add(ai3);
		left.add(player);
		return left.toArray(new Player[0]);
	}

	/**
	 * @return The next player to take a turn. Ignores players who have resigned
	 */
	public Player getNextPlayer() {
		Player current = getCurrentPlayer();

		if(current == getHumanPlayer()) {
			if(!getComputerPlayer1().hasResigned()) return getComputerPlayer1();
			if(!getComputerPlayer2().hasResigned()) return getComputerPlayer2();
			if(!getComputerPlayer3().hasResigned()) return getComputerPlayer3();
			return getHumanPlayer();
		}
		else if(current == getComputerPlayer1()) {
			if(!getComputerPlayer2().hasResigned()) return getComputerPlayer2();
			if(!getComputerPlayer3().hasResigned()) return getComputerPlayer3();
			return getHumanPlayer();
		}
		else if(current == getComputerPlayer2()) {
			if(!getComputerPlayer3().hasResigned()) return getComputerPlayer3();
			return getHumanPlayer();
		}
		else return getHumanPlayer();
	}
	
	public PurchasableProperty getOverlay() {
		return overlay;
	}

	public void setOverlay(PurchasableProperty overlay) {
		this.overlay = overlay;
	}
	
	public Difficulty getDifficulty() {
		return difficulty;
	}
}
