package edu.monopoly.card;

public class Card {

	private boolean chance; //True = Chance Card, False = Community Chest Card
	private String text; //Text displayed on the card
	private CardType type;
	private int value; //Value of the card. This could be the money gained, or spaces to move.
	
	public Card(CardType type, String text, int value, boolean isChance) {
		this.type = type;
		this.text = text;
		this.value = value;
		this.chance = isChance;
	}
	
	public Card(CardType type, String text, boolean isChance) {
		this(type, text, -1, isChance);
	}

	public String getText() {
		return text;
	}

	public CardType getType() {
		return type;
	}

	public int getValue() {
		return value;
	}
	
	public boolean isChance() {
		return chance;
	}
	
	public boolean isCommunityChest() {
		return !isChance();
	}
}
