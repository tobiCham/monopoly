package edu.monopoly.card;

//List of all the types of card and their corresponding config name
public enum CardType {
	
	MOVE_TO_SPACE("moveToSpace"),
	GO_TO_JAIL("jail"),
	GAIN_MONEY("gainMoney"),
	LOSE_MONEY("loseMoney");
	
	private String configName;
	
	CardType(String configName) {
		this.configName = configName;
	}
	
	public String getConfigName() {
		return configName;
	}
	
	public static CardType getByName(String configName) {
		for(CardType type : values()) {
			if(type.getConfigName().equalsIgnoreCase(configName)) return type;
		}
		return null;
	}
}
