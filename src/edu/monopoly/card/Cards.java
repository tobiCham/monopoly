package edu.monopoly.card;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigList;
import com.typesafe.config.ConfigObject;
import com.typesafe.config.ConfigValue;

import edu.monopoly.Monopoly;
import edu.monopoly.util.StringUtils;

public class Cards {

	private static List<Card> chanceCards = new ArrayList<>();
	private static List<Card> communityChestCards = new ArrayList<>();
	
	private static int chanceIndex = 0, communityChestIndex = 0;
	
	/**
	 * Loads all the cards from the config file
	 */
	public static void load() {
		URL url = Monopoly.class.getResource("/chance.hocon");
		if(url == null) {
			System.err.println("Failed to load chance cards.");
			return;
		}
		Config config = ConfigFactory.parseURL(url); //Load in the config file and parse it using typesafe Config parser
		
		loadNode(chanceCards, config.getList("chance"), true);
		loadNode(communityChestCards, config.getList("communityChest"), false);
		
		//Randomises the ordering in the list of chances and community chests
		shuffleChance();
		shuffleCommunityChest();
	}
	
	private static void loadNode(List<Card> cards, ConfigList list, boolean chance) {
		//Goes through each config node in the list, and creates a card from the data
		for(ConfigValue value : list) {
			ConfigObject obj = (ConfigObject) value;
			String text = (String) obj.get("text").unwrapped(); //Gets text from the current node
			text = text.replace("%pound%", StringUtils.POUND_SIGN + ""); //Replaces %pound% with the pound sign to avoid the pound sign in the config
			
			String typeText = (String) obj.get("type").unwrapped(); //Gets the type as a string from the current node
			if(text == null || typeText == null) {  
				//If the text or typeText is missing, print out an error
				System.err.println("Must define text and type for node '" + obj + "'");
				continue;
			}
			CardType type = CardType.getByName(typeText);
			if(type == null) { 
				//If there is no card type with the specified type in the config
				System.err.println("'" + typeText + "' is not a valid card type.");
				continue;
			}
			if(!obj.containsKey("value")) {
				cards.add(new Card(type, text, chance));
				continue;
			}
			
			int intVal = (int) obj.get("value").unwrapped();
			cards.add(new Card(type, text, intVal, chance));
		}
	}
	
	/**
	 * Randomises the chance cards ordering
	 */
	public static void shuffleChance() {
		Collections.shuffle(chanceCards);
	}
	
	/**
	 * Randomises the community chest cards ordering
	 */
	public static void shuffleCommunityChest() {
		Collections.shuffle(communityChestCards);
	}
	
	/*
	 * Picks a random chance card from the list.
	 */
	public static Card randomChance() {
		Card card = chanceCards.get(chanceIndex);
		//Use the remainder so that the index never goes above the size of the list
		chanceIndex = (chanceIndex + 1) % chanceCards.size();
		return card;
	}
	
	/*
	 * Picks a random community chest card from the list.
	 */
	public static Card randomCommunityChest() {
		Card card = communityChestCards.get(communityChestIndex);
		// Use the remainder so that the index never goes above the size of the list
		communityChestIndex = (communityChestIndex + 1) % communityChestCards.size();
		return card;
	}
}
