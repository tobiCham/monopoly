package edu.monopoly.graphic;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which holds a list of characters for the TextBox
 */
public class CharacterSet {

	private List<Character> chars = new ArrayList<>();
	
	public static final CharacterSet LETTERS; //a-Z and some special letters, such as �
	public static final CharacterSet NUMBERS; //0-9
	public static final CharacterSet LETTERS_NUMBERS; //All values in letters and numbers
	public static final CharacterSet LETTERS_NUMBERS_SYMBOLS; //All values in letters_numbers, as well as symbols such as ? or @
	
	static {
		//Initialises the in built character sets.
		
		List<Character> chars = new ArrayList<>();
		for(char c = 'A'; c <= 'Z'; c++) chars.add(c);
		for(char c = 'a'; c <= 'z'; c++) chars.add(c);
		chars.add('�');
		chars.add('�');
		chars.add('�');
		chars.add('�');
		chars.add('�');
		LETTERS = loadSet(chars);
		
		List<Character> numbs = new ArrayList<>();
		for(char c = '0'; c <= '9'; c++) numbs.add(c);
		
		NUMBERS = loadSet(numbs);
		
		chars.addAll(numbs);
		LETTERS_NUMBERS = loadSet(chars);
		
		for(char c = '{'; c <= '~'; c++) chars.add(c);
		for(char c = '['; c <= '`'; c++) chars.add(c);
		for(char c = '!'; c <= '/'; c++) chars.add(c);
		for(char c = ':'; c <= '@'; c++) chars.add(c);
		chars.add('�');
		
		LETTERS_NUMBERS_SYMBOLS = loadSet(chars);
	}
	
	private CharacterSet(List<Character> allowedChars) {
		this.chars.addAll(allowedChars);
	}
	
	public List<Character> getChars() {
		return chars;
	}
	
	public static CharacterSet loadSet(List<Character> chars) {
		return new CharacterSet(chars);
	}
}
