package edu.monopoly.graphic;

/**
 * Class to hold information about Fade Text. 
 */
public class FadeText {

	private String text; //Text to show
	private float timeIn = 0, maxTime; //Time in seconds. timeIn / maxTime is the percentage completed.
	
	public FadeText(String text, float maxTime) {
		this.text = text;
		this.maxTime = maxTime;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public float getTimeIn() {
		return timeIn;
	}

	public void setTimeIn(float timeIn) {
		this.timeIn = timeIn;
	}

	public float getMaxTime() {
		return maxTime;
	}
}
