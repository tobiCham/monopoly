package edu.monopoly.graphic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import edu.monopoly.Monopoly;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.StringUtils;

public class OptionButton<T> {
	
	//Possible values of the button, type specified when the object is created
	//The type of the value is specified when the class is instantiated
	private T[] values; 
	private int selectedIndex; //Index of the selected value from the values variable above.
	private String text;
	private String hoverText; //Text shown when the button is hovered with the mouse
	private int x, y, width, height;
	private Color edgeColor, insideColor, textColor;
	private Runnable onClick; //Method to run when the button is clicked
	private Monopoly game;
	private float roundness = 0.2f;
	private boolean enabled = true;
	
	public OptionButton(Monopoly game, int x, int y, int width, int height, Color edgeColor, Color insideColor, Color textColor, String text, T[] values) {
		this.values = values;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.text = text;
		this.edgeColor = edgeColor;
		this.insideColor = insideColor;
		this.textColor = textColor;
		this.game = game;
	}
	
	public void render(Graphics g) {
		if (values == null) return; //Don't render anything if there are no values
		if (selectedIndex >= values.length) selectedIndex = 0; //Reset the selectedIndex to 0 if its above the size of the array
		
		int x = this.x;
		int y = this.y;
		int width = this.width;
		int height = this.height;
		boolean hover = false;
		
		//Make the button slightly larger when hovered over with the mouse
		if (isHovered()) {
			hover = true;
			if(enabled) {
				int expansion = (int) (width * 0.05);
				x -= expansion;
				width += expansion * 2;
			}
		}
		
		//Render the button
		Color origColor = g.getColor();
		
		g.setColor(edgeColor);
		g.fillRoundRect(x - 2, y - 2, width + 4, height + 4, (int) (roundness * width), (int) (roundness * width)); //Draw the border
		g.setColor(insideColor);
		g.fillRoundRect(x, y, width, height, (int) (roundness * width), (int) (roundness * width)); //Draw the background
		
		T value = null;
		if (selectedIndex < values.length) value = values[selectedIndex];
		String str = text;
		
		g.setColor(textColor);
		
		//Add on the value to the text to display if it isn't null
		if (value != null) str += ": " + getValueName(value);
		
		//Get size of the text
		Rectangle boundingBox = g.getFontMetrics(g.getFont()).getStringBounds(str, g).getBounds();
		
		//Draw the string in the centre of the button
		g.drawString(str, (int) (x + (width / 2) - boundingBox.getCenterX()), (int) (y + (height / 2) - boundingBox.getCenterY()));
		
		//Render the hover text if it exists
		if (hover && hoverText != null) {
			Font origFont = g.getFont();
			g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, (int) (g.getFont().getSize() / 1.5)));
			
			Rectangle newStringBox = g.getFontMetrics(g.getFont()).getStringBounds(hoverText, g).getBounds();
			g.drawString(hoverText, (int) (x + (width / 2) - newStringBox.getCenterX()), (int) (y + newStringBox.getCenterY()));
			g.setFont(origFont);
		}
		//Return to the colour set at the start of the render method
		g.setColor(origColor);
	}
	
	/**
	 * @return A string representation of the value specified.
	 */
	private String getValueName(T value) {
		if(value instanceof String) return value.toString();
		String txt = value.toString();
		return StringUtils.format(txt);
	}
	
	/**
	 * @return Whether the mouse if over the button
	 */
	public boolean isHovered() {
		Rectangle rect = new Rectangle(x - 2, y - 2, width + 4, height + 4);
		Rectangle mouse = new Rectangle(game.getDisplay().getMouse().getX() - 1, game.getDisplay().getMouse().getY() - 1, 2, 2);
		return rect.intersects(mouse);
	}
	
	/**
	 * Should be called on a mouse click event
	 * @return Whether or not the button was clicked
	 */
	public boolean mouseClick(int button, int x, int y) {
		if(!enabled) return false; //Do nothing if the button isn't enabled
		Rectangle buttonRect = new Rectangle(this.x, this.y, this.width, this.height);
		Rectangle mouseRect = new Rectangle(x - 1, y - 1, 2, 2);
		//If the mouse is inside the button, change the selected value and play the click sound
		if (buttonRect.intersects(mouseRect)) {
			//Release mouse button to prevent any code which only checks for mouse down to be run after the button is clicked
			game.getDisplay().getMouse().releaseAllButtons();
			selectedIndex++;
			if (selectedIndex >= values.length) selectedIndex = 0;
			Sounds.CLICK.play();
			if (onClick != null) onClick.run();
			return true;
		}
		return false;
	}
	
	/**
	 * @return The current selected value, or null if there is not one
	 */
	public T getValue() {
		if(values == null || values.length == 0) return null;
		if(selectedIndex >= values.length) selectedIndex = 0;
		return values[selectedIndex];
	}
	
	/**
	 * @return The index of the currently selected option
	 */
	public int getSelectedIndex() {
		return selectedIndex;
	}
	
	/**
	 * Sets the index of the currently selected option
	 */
	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	
	/**
	 * @param value The value to set to
	 * @return Whether the operation was successful.
	 */
	public boolean setValue(T value) {
		for (int i = 0; i < values.length; i++) {
			T t = values[i];
			if (t.equals(value)) {
				setSelectedIndex(i);
				return true;
			}
		}
		return false;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public Color getEdgeColor() {
		return edgeColor;
	}
	
	public void setEdgeColor(Color edgeColor) {
		this.edgeColor = edgeColor;
	}
	
	public Color getInsideColor() {
		return insideColor;
	}
	
	public void setInsideColor(Color insideColor) {
		this.insideColor = insideColor;
	}
	
	public Color getTextColor() {
		return textColor;
	}
	
	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}
	
	public Runnable getOnClick() {
		return onClick;
	}
	
	public void setOnClick(Runnable onClick) {
		this.onClick = onClick;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getHoverText() {
		return hoverText;
	}
	
	public void setHoverText(String hoverText) {
		this.hoverText = hoverText;
	}
	
	public T[] getValues() {
		return values;
	}
	
	public void setRoundnessRatio(float ratio) {
		if (ratio < 0) ratio = 0;
		if (ratio > 1) ratio = 1;
		this.roundness = ratio;
	}
	
	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
