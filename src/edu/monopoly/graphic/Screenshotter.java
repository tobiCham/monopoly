package edu.monopoly.graphic;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import javax.imageio.ImageIO;

import com.tobi.guibase.GUIBase;
import com.tobi.guibase.RenderSetting;

import edu.monopoly.Display;
import edu.monopoly.Monopoly;
import edu.monopoly.util.FileUtils;

public class Screenshotter {

	/**
	 * @return An image representation of the current screen
	 */
	public static BufferedImage screenshot() {
		Display display = Monopoly.getGameInstance().getDisplay();
		
		BufferedImage image = new BufferedImage(display.getInitialWidth(), display.getInitialHeight(), BufferedImage.TYPE_INT_ARGB); //Create an image of the screen size to save to
		Graphics2D g2d = image.createGraphics();
		GUIBase.applySettings(g2d, RenderSetting.ULTRA);
		display.render(g2d); //Render the game onto the image using Ultra settings so the image is good quality
		g2d.dispose();
		
		return image;
	}
	
	/**
	 * @return Whether the image was successfully saved
	 */
	public static boolean writeImage(File file, BufferedImage image) {
		try {
			ImageIO.write(image, "PNG", file);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Takes a screenshot of the game and saves it to the screenshots folder
	 */
	public static void takeScreenshot() {
		BufferedImage image = screenshot();
		
		File dir = new File(FileUtils.getGameDirectory(), "screenshots");
		if(!dir.exists()) dir.mkdirs(); //Create the screenshots folder if it doesn't already exist
		Calendar calendar = Calendar.getInstance();
	    int year = calendar.get(Calendar.YEAR);
	    int month = calendar.get(Calendar.MONTH);
	    int day = calendar.get(Calendar.DAY_OF_MONTH);
	    
	    int hour = calendar.get(Calendar.HOUR);
	    int minute = calendar.get(Calendar.MINUTE);
	    int second = calendar.get(Calendar.SECOND);
	    
	    //The file name is based on the time at which the screenshot was taken
	    String date = year + "-" + month + "-" + day + "_" + hour + "-" + minute + "-" + second;
	    File file = new File(dir, date + ".png");
	    
	    writeImage(file, image);
	}
}
