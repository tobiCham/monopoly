package edu.monopoly.graphic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.tobi.guibase.Keyboard;
import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.GraphicUtils;

public class TextBox {
	
	private int x, y, width, height;
	private Color textColor, boxColor;
	private String text = "", blankText = "";
	private int cursorPosition = 0;
	private CharacterSet characterSet; //Characters which can be entered
	private boolean multiLine = false; //Whether there can be multiple lines
	private int cursorCounter = 0; //Used for the blinking of the cursor
	private int blinkSpeed = 26; //Number of updates per blink
	private int characterLimit = -1; //Maximum number of characters
	private boolean selected = true; //Whether the text box is selected
	private Font font;
	
	public TextBox(int x, int y, int width, int height, Color textColor, Color boxColor, CharacterSet set) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.textColor = textColor;
		this.boxColor = boxColor;
		this.characterSet = set;
	}
	
	public void render(Graphics g) {
		
		int startY = (int) (y + (height / 2) + (g.getFont().getSize() / 3f));
		int startX = x + (width / 30);
		int gapY = 5;
		font = g.getFont();
		Color oldColor = g.getColor();
		
		Color boxColor = this.boxColor;
		if(!selected) {
			//If the box isn't selected, render it as slighly darker
			boxColor = new Color((int) (boxColor.getRed() * 0.9f), (int) (boxColor.getGreen() * 0.9f), (int) (boxColor.getBlue() * 0.9f));
		}
		
		GraphicUtils.drawButton(g, x, y, width, height, boxColor);
		g.setColor(textColor);
		
		if(!multiLine) {
			//If there aren't multiple lines, constantly remove a character from the end of the string while the string is longer than the textbox width
			while(g.getFontMetrics(g.getFont()).getStringBounds(text, g).getBounds().width > this.width * 0.95f) {
				setCursorPosition(getCursorPosition() - 1); 
				text = text.substring(0, text.length() - 1);
			}
		}
		
		String renderText = new String(text);
		
		if(text.isEmpty()) {
			//If there is no text, use a slightly lighter shade of the text colour, then set the text to draw as the blank text
			int newRed = (int) (textColor.getRed() + (0.4f * 255));
			int newGreen = (int) (textColor.getGreen() + (0.4f * 255));
			int newBlue = (int) (textColor.getBlue() + (0.4f * 255));
			if(newRed > 255) newRed = 255;
			if(newGreen > 255) newGreen = 255;
			if(newBlue > 255) newBlue = 255;
			g.setColor(new Color(newRed, newGreen, newBlue));
			renderText = blankText;
		}
		
		//Replace all tabs with spaces when rendering, then split into an array of all the lines, separated by \n (new line)
		String[] parts = renderText.replace("\t", "    ").split("\n");
		for(int i = 0; i < parts.length; i++) {
			String part = parts[i];
			//Draw each line in succession downwards
			g.drawString(part, startX, startY + (i * (g.getFont().getSize() + gapY)));
		}
		g.setColor(textColor);
		
		//Render the cursor if the box is selected
		if(cursorCounter > blinkSpeed / 2 && selected) {
			String sub = renderText.substring(0, cursorPosition);
			int newLines = countChars(sub, '\n');
			if(sub.contains("\n")) sub = sub.substring(sub.lastIndexOf('\n'), sub.length());
			sub = sub.replace("\t", "    ");
			Rectangle rect = g.getFontMetrics(g.getFont()).getStringBounds(sub, g).getBounds();
			
			g.fillRect(startX + rect.width - 2, startY - (int) (g.getFont().getSize() * 0.8f) + (newLines * (g.getFont().getSize() + gapY)), 2, g.getFont().getSize());
		}
		
		g.setColor(oldColor);
	}
	
	/**
	 * @return The number of characters of a type in a string
	 */
	private int countChars(String string, char c) {
		int total = 0;
		//Go through each character in the string, and add 1 to the total if the character is the same as the one specified as the parameter
		for(char ch : string.toCharArray()) {
			if(ch == c) total++;
		}
		return total;
	}
	
	public void update() {
		//Increment the cursor counter, or reset to 0 if larger than the blink speed
		if(cursorCounter > blinkSpeed) cursorCounter = 0;
		cursorCounter++;
	}
	
	//When the mouse is clicked
	public void mouseClick(int button) {
		if(button != Mouse.MOUSE_LEFT) return;
		Mouse mouse = Monopoly.getGameInstance().getDisplay().getMouse();
		
		Rectangle mouseBounds = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		Rectangle bounds = new Rectangle(x, y, width, height);
		//Set the booleans selected to whether the mouse is inside the textbox
		selected = mouseBounds.intersects(bounds);
	}
	
	//When a key is pressed
	public void keyPress(int key, char symbol) {
		//When Esc is pressed, de-select the textbox
		if(key == KeyEvent.VK_ESCAPE) {
			selected = false;
		}
		if(!selected) return;
		//If the enter key is pressed, enter a new line if multi line is enabled
		if(key == KeyEvent.VK_ENTER) {
			if(multiLine) insertCharacter('\n');
			return;
		}
		//If the enter key is pressed, enter a tab space if multi line is enabled
		if(key == KeyEvent.VK_TAB) {
			if(multiLine) insertCharacter('\t');
			return;
		}
		
		//If Ctrl-V is pressed, paste the contents of the clipboard into the textbox
		if(Keyboard.isKeyDown(KeyEvent.VK_CONTROL)) {
			if(key == KeyEvent.VK_V) {
				try {
					String text = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
					Keyboard.releaseKey(KeyEvent.VK_CONTROL);
					for(char c : text.toCharArray()) { 
						insertCharacter(c);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
				return;
			}
		}
		
		if(key == KeyEvent.VK_BACK_SPACE) {
			if(cursorPosition == 0) return;
			//Remove a character before the cursor
			String sub1 = text.substring(0, cursorPosition - 1);
			String sub2 = text.substring(cursorPosition, text.length());
			this.text = sub1 + sub2;
			setCursorPosition(getCursorPosition() - 1);
			playEffect();
			return;
		}
		if(key == KeyEvent.VK_DELETE) {
			if(cursorPosition >= text.length()) return;
			//Remove a character after the cursor
			String sub1 = text.substring(0, cursorPosition);
			String sub2 = text.substring(cursorPosition + 1, text.length());
			this.text = sub1 + sub2;
			playEffect();
			return;
		}
		if(key == KeyEvent.VK_LEFT) {
			//Move the cursor 1 character back
			setCursorPosition(getCursorPosition() - 1);
			return;
		}
		if(key == KeyEvent.VK_RIGHT) {
			//Move the cursor 1 character froward
			setCursorPosition(getCursorPosition() + 1);
			return;
		}
		if(multiLine) { //If the text box spans multiple lines
			if(key == KeyEvent.VK_UP) {
				//Move the cursor up one line
				String firstHalf = text.substring(0, cursorPosition);
				int last = firstHalf.lastIndexOf("\n");
				int secondLast = firstHalf.lastIndexOf("\n", last - 1);
				int in = getCursorPosition() - last;
				if(secondLast < 0) {
					secondLast = 0;
					in--;
				}
				int newCursor = secondLast + in;
				if(newCursor > last) newCursor = last;
				if(newCursor >= 0) setCursorPosition(newCursor);
				return;
			}
			if(key == KeyEvent.VK_DOWN) {
				//Move the cursor down one line
				String firstHalf = text.substring(0, cursorPosition);
				String sub = text.substring(cursorPosition, text.length());
				int first = sub.indexOf('\n');
				if(first >= 0) {
					int start = firstHalf.lastIndexOf("\n");
					int indent = (start < 0 ? getCursorPosition() : getCursorPosition() - start - 1);
					int next = sub.indexOf("\n", first + 1);
					if(next < 0) next = sub.length();
					int newIndent = first + 1 + indent;
					if(newIndent > next) newIndent = next;
					int absPos = newIndent + getCursorPosition();
					if(absPos > text.length()) absPos = text.length();
					setCursorPosition(absPos);
				}
				return;
			}
		}
		if(key == KeyEvent.VK_HOME) {
			//Set the cursor to the start of the text
			setCursorPosition(0);
			return;
		}
		if(key == KeyEvent.VK_END) {
			//Set the cursor to the end of the text
			setCursorPosition(text.length());
			return;
		}
		
		//Keys which should never be listed as characters
		List<Integer> blacklisted = Arrays.asList(KeyEvent.VK_SHIFT, KeyEvent.VK_CONTROL, KeyEvent.VK_PAGE_UP, KeyEvent.VK_PAGE_DOWN, KeyEvent.VK_INSERT,
				KeyEvent.VK_NUM_LOCK, KeyEvent.VK_SCROLL_LOCK, KeyEvent.VK_CAPS_LOCK, KeyEvent.VK_ALT, KeyEvent.VK_ALT_GRAPH);
			
		if(blacklisted.contains(key)) return;
		insertCharacter(symbol);
	}
	
	/**
	 * Play a random key type sound effect
	 */
	private void playEffect() {
		if(isSelected()) {
			Random rand = new Random();
			int clip = rand.nextInt(3);
			if(clip == 0) Sounds.KEY1.play();
			if(clip == 1) Sounds.KEY2.play();
			else Sounds.KEY3.play();
		}
	}
	
	private void insertCharacter(char c) {
		//Return if there is a character limit and adding a character to the text would be larger than it
		if(characterLimit >= 0 && text.length() + 1 > characterLimit) return;
		
		//Add 4 spaces instead of a tab
		if(c == '\t') {
			for(int i = 0; i < 4; i++) insertCharacter(' ');
			return;
		}
		if(characterSet != null) {
			//Return if the character set it not null and the character is not contained in the list
			if(!characterSet.getChars().contains(c) && !(c == ' ' || c == '\n')) return;
		} else {
			//Don't add the character if the font can't render it
			if(font != null && !font.canDisplay(c)) return;
		}
		//Play the typing sound effect
		playEffect();
		
		//Insert a character at the current cursor position
		String sub1 = text.substring(0, cursorPosition);
		String sub2 = text.substring(cursorPosition, text.length());
		this.text = sub1 + c + sub2;
		//Move the cursor 1 character forward
		setCursorPosition(getCursorPosition() + 1);
	}

	public int getCursorPosition() {
		return cursorPosition;
	}

	/**
	 * Set the cursor position. Validates to make sure that the new position isn't outside the text size.
	 */
	public void setCursorPosition(int newPos) {
		if(newPos < 0) newPos = 0;
		if(newPos > text.length()) newPos = text.length();
		this.cursorPosition = newPos;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	public Color getBoxColor() {
		return boxColor;
	}

	public void setBoxColor(Color boxColor) {
		this.boxColor = boxColor;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		setCursorPosition(0);
	}

	public String getBlankText() {
		return blankText;
	}

	public void setBlankText(String blankText) {
		this.blankText = blankText;
	}

	public int getCharacterLimit() {
		return characterLimit;
	}

	public void setCharacterLimit(int characterLimit) {
		this.characterLimit = characterLimit;
	}

	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
