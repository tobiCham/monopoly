package edu.monopoly.graphic;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.monopoly.Display;
import edu.monopoly.Monopoly;

public class Transition {
	
	//List of all the transitions currently running
	private static List<Transition> transitions = new ArrayList<>();
	
	private int fadeInTimer;
	private int fadeOutTimer;
	private int startIn, startOut;
	private boolean finished = false;
	
	public void startTransition() {
		transitions.add(this);
	}
	
	/**
	 * Starts a fade in transition
	 * @param time The time the transition will last, in updates
	 */
	public void fadeIn(int time) {
		fadeInTimer = time;
		startIn = time;
	}
	
	/**
	 * Starts a fade out transition
	 * @param time The time the transition will last, in updates
	 */
	public void fadeOut(int time) {
		fadeOutTimer = time;
		startOut = time;
	}
	
	public void renderTransition(Graphics g, Monopoly game) {
		Color oldColor = g.getColor();
		Display display = game.getDisplay();
		if (fadeInTimer > 0) {
			float percentage = (float) fadeInTimer / startIn;
			//Set the transparency to the percentage through the transition
			g.setColor(new Color(0, 0, 0, (int) (percentage * 255)));
			//Draw a rectangle over the whole screen
			g.fillRect(0, 0, display.getInitialWidth(), display.getInitialHeight());
			//Subtract 1 from the fade in timer
			fadeInTimer -= 1;
		}
		if (fadeOutTimer > 0) {
			float percentage = 1 - (float) fadeOutTimer / startOut;
			//Set the transparency to 1 - percentage through the transition
			g.setColor(new Color(0, 0, 0, (int) (percentage * 255)));
			//Draw a rectangle over the whole screen
			g.fillRect(0, 0, display.getInitialWidth(), display.getInitialHeight());
			//Subtract 1 from the fade out timer
			fadeOutTimer -= 1;
		}
		
		//If the fade timer and the fade out timer are both less than 1, the transition is finished
		if (fadeInTimer < 1 && fadeOutTimer < 1) {
			finished = true;
		}
		
		g.setColor(oldColor);
	}
	
	public static void updateAll(Graphics g, Monopoly game) {
		//Render all the transitions, and removes them from the list if they've finished
		Iterator<Transition> iterator = transitions.iterator();
		while (iterator.hasNext()) {
			Transition transition = iterator.next();
			transition.renderTransition(g, game);
			if (transition.finished) {
				iterator.remove();
			}
		}
	}
	
	public static List<Transition> getActiveTransitions() {
		return transitions;
	}
}
