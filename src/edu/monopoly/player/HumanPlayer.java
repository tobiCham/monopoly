package edu.monopoly.player;

import java.awt.Color;

import edu.monopoly.PlayerAnimation;
import edu.monopoly.board.Board;
import edu.monopoly.card.Card;
import edu.monopoly.card.CardType;
import edu.monopoly.property.Property;
import edu.monopoly.property.PurchasableProperty;
import edu.monopoly.saving.ConfigSection;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.propertyview.ScreenPropertyView;
import edu.monopoly.screen.question.ScreenQuestion;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.StringUtils;

public class HumanPlayer extends Player {
	
	private int moneyOwed;
	
	public HumanPlayer(PieceType piece, String name, Color color) {
		super(piece, name, color);
	}
	
	@Override
	public boolean onLand(ScreenPlay screen, Board board, Property property, int diceRoll) {
		//This method will always return false as the player's turn should never automatically end.
		if(property instanceof PurchasableProperty) {
			PurchasableProperty purchasable = (PurchasableProperty) property;
			Player owner = board.getOwner(purchasable);
			if(owner == null) { //If nobody owns the property the player just landed on
				if(getMoney() >= purchasable.getCost()) {
					//Enter into the property view screen with true as a parameter to indicate the player can purchase it
					ScreenPropertyView newScreen = new ScreenPropertyView(screen.getGame(), board, purchasable, true);
					newScreen.init();
					Screen.setSelectedScreen(newScreen);
					return false;
				} else {
					//If the player doesn't have enough money to buy, simply show them a message instead of showing the property
					Screen.showConfirmMessage("You don't have enough money\nto buy that property.\n\nSell or mortgage properties\nif you wish to buy", null);
				}
			} else if(owner != this) { //If another player owns the property
				int rentOwed = board.getRentOwed(diceRoll, this, purchasable); //Get the rent owed for landing
				if(rentOwed > getMoney()) {
					//If the player doesn't have enough money to pay the rent, add it on as debt
					Screen.showConfirmMessage("You don't have enougn money\nto pay for this.\n\nMortgage or sell to\npay off this debt.", null);
					setMoneyOwed(getMoneyOwed() + rentOwed);
				} else if(rentOwed != 0) {
					//The player has enough money to pay the rent. Enter into the question screen and ask the player how much money they
					//Have after paying the rent
					String question = "You landed on " + property.getName() + "\n";
					question += "Which is owned  by " + board.getOwner(purchasable).getName() + "\n";
					question += "You have �" + getMoney() + " and owe �" + rentOwed + " in rent.\n";
					question += "How much money do you have left?";
					
					ScreenQuestion newScreen = new ScreenQuestion(screen.getGame(), question, getMoney() - rentOwed, () -> setMoney(getMoney() - rentOwed));
					newScreen.init();
					Screen.setSelectedScreen(newScreen);
				}
			}
		}
		return false;
	}
	
	@Override
	public void onPass(ScreenPlay screen, Board board, Property property) { }
	
	@Override
	public void handleCard(Card card, ScreenPlay screen, Board board) {
		if(card.getType() == CardType.GAIN_MONEY) {
			//If the player receives money from the card, enter into a question screen asking the player how much money they have after
			//gaining the money specified on the card
			String question = "You currently have " + StringUtils.POUND_SIGN + getMoney() + "\nand gain " + StringUtils.POUND_SIGN + card.getValue();
			question += "\n\nHow much money do you now have?";
			
			ScreenQuestion newScreen = new ScreenQuestion(screen.getGame(), question, getMoney() + card.getValue(), () -> setMoney(getMoney() + card.getValue()));
			newScreen.init();
			Screen.setSelectedScreen(newScreen);
		} else if(card.getType() == CardType.LOSE_MONEY) {
			//If the player loses money from the card
			if(getMoney() >= card.getValue()) {
				//If the player has enough money to pay the amount on the card, enter into a question screen asking the player how much money they have after
				//losing the money specified on the card
				String question = "You currently have " + StringUtils.POUND_SIGN + getMoney() + "\nand owe " + StringUtils.POUND_SIGN + card.getValue();
				question += "\n\nHow much money do you have after paying?";
				
				ScreenQuestion newScreen = new ScreenQuestion(screen.getGame(), question, getMoney() - card.getValue(), () -> setMoney(getMoney() - card.getValue()));
				newScreen.init();
				Screen.setSelectedScreen(newScreen);
			} else {
				//Otherwise, the player doesn't have enough money. Send them a message telling them they have to mortgage or sell, then
				//Add on the value of the card to the player's debt
				Screen.showConfirmMessage("You don't have enougn money\nto pay for this.\n\nMortgage or sell to\npay off this debt.", null);
				setMoneyOwed(getMoneyOwed() + card.getValue());
			}
		} else if(card.getType() == CardType.MOVE_TO_SPACE) {
			int currentSpace = getBoardPosition();
			int diff = card.getValue() - currentSpace;
			//Calculate the number of spaces the player has to move to get to the space.
			//If the number of spaces is less than 0, i.e. the player has moved past the space, add 
			//40 to the difference (number of spaces on the board) so the player moves around the board and doesn't move backwards
			
			if(diff <= 0) diff += 40;
			
			//Move the player the number of spaces around the board
			PlayerAnimation animation = new PlayerAnimation(screen, this, diff);
			screen.getAnimations().add(animation);
		} else if(card.getType() == CardType.GO_TO_JAIL) {
			//If the card is go to jail, play the siren sound effect and set the player in jail
			Sounds.SIREN.play();
			setBoardPosition(10);
			setJailAttempts(0);
			setInJail(true);
		}
	}
	
	@Override
	public void save(ConfigSection section, Board board) {
		super.save(section, board);
		//Save the default settings, and also save how much money the player owes
		section.set("owe", getMoneyOwed());
	}
	
	@Override
	public void load(ConfigSection section, Board board) {
		super.load(section, board);
		//Loads the default settings, and also load how much money the player owes
		this.moneyOwed = section.getInteger("owe");
	}
	

	public int getMoneyOwed() {
		return moneyOwed;
	}

	public void setMoneyOwed(int moneyOwed) {
		this.moneyOwed = moneyOwed;
	}
}
