package edu.monopoly.player;

import java.awt.Image;

import edu.monopoly.util.ImageLoader;

public enum PieceType {

	//List of all the possible piece types which can be used.
	CAR, DOG, HAT, IRON, THIMBLE, WHEELBARROW;
	
	private Image icon;
	
	PieceType() {
		//Loads the icon for the piece as icons/icon_[name].png, e.g. icons/icon_hat.png
		icon = ImageLoader.loadImage("icons/icon_" + name().toLowerCase() + ".png");
	}
	
	public Image getIcon() {
		return icon;
	}
	
	public static void init() {}
	
}
