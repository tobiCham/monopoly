package edu.monopoly.player;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.reflect.TypeToken;

import edu.monopoly.board.Board;
import edu.monopoly.card.Card;
import edu.monopoly.property.ColoredProperty;
import edu.monopoly.property.Property;
import edu.monopoly.property.PropertyColors;
import edu.monopoly.property.PurchasableProperty;
import edu.monopoly.saving.ConfigSection;
import edu.monopoly.saving.SavingManager;
import edu.monopoly.screen.ScreenPlay;

public abstract class Player {
	
	private int money = 1500; //Balance
	private int boardPosition = 0; //Position on the board, 0 for Go, 39 for Shanghai
	private int jailAttempts = 0; //Number of attempts at getting out of jail. Maximum 3, 0 if not in jail
	private boolean inJail = false, resigned = false;
	private String name;
	private Color color;
	
	private Set<PurchasableProperty> properties = new HashSet<>(); //Properties purchased / owned
	private Map<ColoredProperty, Integer> buildings = new HashMap<>(); //Key is the property, value is the number of buildings built on that property
	private PieceType piece;
	
	public Player(PieceType piece, String name, Color color) {
		this.piece = piece;
		this.name = name;
		this.color = color;
	}
	
	/**
	 * Saves player data to a config section
	 */
	public void save(ConfigSection section, Board board) {
		//Put values into the config to save
		section.set("money", money);
		section.set("boardPosition", boardPosition);
		section.set("jailAttempts", jailAttempts);
		section.set("inJail", inJail);
		section.set("resigned", resigned);
		section.set("name", name);
		section.set("color", color.getRGB()); //RGB of the color is an int value.
		section.set("piece", piece); //Piece will be converted into its name
		
		List<Integer> propertyLocs = new ArrayList<>(); //List of property indexes which are owned. E.g. '1' corresponds to Madrid
		List<Integer> mortgagedProperties = new ArrayList<>(); //List of property indexes which are mortgaged. All entries in this list are also in propertyLocs
		Map<Integer, Integer> buildings = new HashMap<>(); //Map of property indexes to number built. E.g. 2 houses on Madrid would be 1:2
		
		//Loop through board positions. If the player owned the property, add the board index to one of the lists or map
		for(int pos : board.getProperties().keySet()) {
			Property prop = board.getProperties().get(pos);
			if(properties.contains(prop)) {
				PurchasableProperty purchasable = (PurchasableProperty) prop;
				propertyLocs.add(pos);
				if(purchasable.isMortgaged()) mortgagedProperties.add(pos);
				if(this.buildings.containsKey(prop)) buildings.put(pos, this.buildings.get(prop));
			}
		}
		
		section.set("properties", propertyLocs);
		section.set("mortgaged", mortgagedProperties);
		section.set("buildings", buildings);
	}
	
	/**
	 * Loads player data from a config section
	 */
	public void load(ConfigSection section, Board board) {
		this.properties.clear();
		this.buildings.clear();
		
		this.money = section.getInteger("money");
		this.boardPosition = section.getInteger("boardPosition");
		this.jailAttempts = section.getInteger("jailAttempts");
		this.inJail = section.getBoolean("inJail");
		this.resigned = section.getBoolean("resigned");
		this.name = section.getString("name");
		this.color = new Color(section.getInteger("color"));
		this.piece = section.getType("piece", PieceType.class);
		
		List<Integer> propertyLocs = section.getIntList("properties");
		List<Integer> mortgagedProperties = section.getIntList("mortgaged");
		Map<Integer, Integer> buildings = SavingManager.getGson().fromJson(SavingManager.getGson().toJson(section.get("buildings")), new TypeToken<Map<Integer, Integer>>() {}.getType());
		
		//Loop through all the board positions. 
		//If the current position in the loop is contained in propertyLocs, get the property at that board position and add it to the list of owned properties
		//If is contained in the mortgaged properties list, set it to mortgaged
		for(int pos : board.getProperties().keySet()) {
			Property prop = board.getProperties().get(pos);
			if(!(prop instanceof PurchasableProperty)) continue;
			PurchasableProperty purchasable = (PurchasableProperty) prop;
			if(propertyLocs.contains(pos)) {
				board.setOwner(purchasable, this);
			}
			if(mortgagedProperties.contains(pos)) purchasable.setMortgaged(true);
			
			if(buildings.containsKey(pos) && prop instanceof ColoredProperty) this.buildings.put((ColoredProperty) prop, buildings.get(pos));
		}
	}
	
	/** 
	 * Called when the player lands on a property
	 * @param property The property which has been landed on
	 * @param diceRoll The number rolled on the dice
	 * @return true to end the turn automatically, false if the turn should manually be ended
	 */
	public abstract boolean onLand(ScreenPlay screen, Board board, Property property, int diceRoll);
	
	/**
	 * Called when the player passes a property and does not land on it
	 * @param property The property the player is passing by
	 */
	public abstract void onPass(ScreenPlay screen, Board board, Property property);
	
	/**
	 * Handles the logic for the actions which can occur when the player lands on a chance or community chest.
	 * Will be called after the user has seen the card information
	 * @param card The card which the player has selected
	 */
	public abstract void handleCard(Card card, ScreenPlay screen, Board board);

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getBoardPosition() {
		return boardPosition;
	}

	public void setBoardPosition(int boardPosition) {
		this.boardPosition = boardPosition % 40;
	}

	public int getJailAttempts() {
		return jailAttempts;
	}

	public void setJailAttempts(int jailAttempts) {
		this.jailAttempts = jailAttempts;
	}

	public boolean isInJail() {
		return inJail;
	}

	public void setInJail(boolean inJail) {
		this.inJail = inJail;
	}

	public Set<PurchasableProperty> getProperties() {
		return properties;
	}
	
	public Color getColor() {
		return color;
	}

	public Set<PurchasableProperty> getMortgagedProperties() {
		//Loop through each property owned. If it is mortgaged, add it to the temporary list. 
		Set<PurchasableProperty> props = new HashSet<>();
		for(PurchasableProperty property : properties) {
			if(property.isMortgaged()) props.add(property);
		}
		return props;
	}

	public Set<ColoredProperty> getBuildings() {
		return buildings.keySet();
	}
	
	public boolean hasBuildings(ColoredProperty property) {
		return buildings.containsKey(property) && getBuildings(property) > 0;
	}
	
	/**
	 * @return True if the player has any buildings on any of the properties of the colour specified
	 */
	public boolean hasBuildings(PropertyColors color) {
		for(ColoredProperty prop : buildings.keySet()) {
			if(prop.getColor() == color && hasBuildings(prop)) return true;
		}
		return false;
	}
	
	public int getBuildings(ColoredProperty property) {
		if(buildings.containsKey(property)) return buildings.get(property);
		return 0;
	}
	
	public void setBuildings(ColoredProperty property, int number) {
		if(number < 0) number = 0;
		if(number > 5) number = 5;
		if(number == 0) buildings.remove(property);
		else buildings.put(property, number);
	}

	public PieceType getPiece() {
		return piece;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean hasResigned() {
		return resigned;
	}

	public void setResigned(boolean resigned) {
		this.resigned = resigned;
	}
}
