package edu.monopoly.property;

/**
 * Each instance represents a property which has a coloured strip visually, and can have
 * houses or hotels built on.
 */
public class ColoredProperty extends PurchasableProperty {

	private final PropertyColors color; //Colour of the property. Enum contains the colour of the property
	private final int rent, house1, house2, house3, house4, hotel;
	
	public ColoredProperty(String name, PropertyColors color, int cost, int rent, int house1, int house2, int house3, int house4, int hotel) {
		super(name, cost);
		this.color = color;
		this.rent = rent;
		this.house1 = house1;
		this.house2 = house2;
		this.house3 = house3;
		this.house4 = house4;
		this.hotel = hotel;
	}
	
	public PropertyColors getColor() {
		return color;
	}

	@Override
	public int getRent(int owned) {
		//Brown or Dark blue has only 2 properties, so owning 2 is a monopoly, and return double the rent
		//Any other colour has 3 properties, so owning 3 is a monopoly and return double the rent
		//Other than owning a monopoly, the normal rent should be returned, unless none are owned, in which case no rent,
		//Any other value returns -1, e.g. if -2 is specified
		if(color == PropertyColors.BROWN || color == PropertyColors.DARK_BLUE) {
			if(owned == 1) return rent;
			if(owned == 2) return rent * 2;
		} else {
			if(owned == 1 || owned == 2) return rent;
			if(owned == 3) return rent * 2;
		}
		if(owned == 0) return 0;
		return -1;
	}
	
	/**
	 * @return The rent owed for landing in the property with a specified number of houses built. If 0 is specified, it is treated as normal rent
	 */
	public int getBuildingRent(int houses) {
		if(houses == 0) return getRent();
		if(houses == 1) return house1;
		if(houses == 2) return house2;
		if(houses == 3) return house3;
		if(houses == 4) return house4;
		if(houses == 5) return hotel;
		return -1;
	}
	
	/**
	 * @return The cost to build 1 building on the property. For the first row, �50, the second is �100, third is �150 and
	 * 		   fourth row is �200
	 */
	public int getBuildingCost() {
		if(color == PropertyColors.BROWN || color == PropertyColors.LIGHT_BLUE) return 50;
		if(color == PropertyColors.PURPLE || color == PropertyColors.ORANGE) return 100;
		if(color == PropertyColors.YELLOW || color == PropertyColors.RED) return 150;
		return 200;
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
