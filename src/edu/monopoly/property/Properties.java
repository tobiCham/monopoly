package edu.monopoly.property;

import java.util.ArrayList;
import java.util.List;

import edu.monopoly.screen.Screens;

public class Properties {
	
	private static List<Property> properties = new ArrayList<>(); //List of all the properties
	private static float startProgress = 0f; //Used to store the loading progress when property loading begins
	
	public static void init() {
		startProgress = Screens.LOADING.getLoading(); 
		
		//Add all the properties
		addProperty(new TileGo());
		
		addProperty(new ColoredProperty("Madrid", PropertyColors.BROWN, 60, 2, 10, 30, 90, 160, 250));
		addProperty(new TileCommunityChest());
		addProperty(new ColoredProperty("Berlin", PropertyColors.BROWN, 80, 4, 20, 60, 180, 320, 450));
		addProperty(new TileIncomeTax());
		addProperty(new TransportProperty("Rail Transport"));
		addProperty(new ColoredProperty("Cape Town", PropertyColors.LIGHT_BLUE, 100, 6, 30, 90, 270, 400, 550));
		addProperty(new TileChance());
		addProperty(new ColoredProperty("Los Angeles", PropertyColors.LIGHT_BLUE, 100, 6, 30, 90, 270, 400, 550));
		addProperty(new ColoredProperty("Johannesburg", PropertyColors.LIGHT_BLUE, 120, 8, 40, 100, 300, 450, 600));
		
		addProperty(new TileJail());
		
		addProperty(new ColoredProperty("Singapore", PropertyColors.PURPLE, 140, 10, 50, 150, 450, 625, 750));
		addProperty(new UtilityProperty("Water Works"));
		addProperty(new ColoredProperty("Santiago", PropertyColors.PURPLE, 140, 10, 50, 150, 450, 625, 700));
		addProperty(new ColoredProperty("Rio De Janeiro", PropertyColors.PURPLE, 160, 12, 60, 180, 500, 700, 900));
		addProperty(new TransportProperty("Air Transport"));
		addProperty(new ColoredProperty("New York City", PropertyColors.ORANGE, 180, 14, 70, 200, 550, 750, 950));
		addProperty(new TileCommunityChest());
		addProperty(new ColoredProperty("London", PropertyColors.ORANGE, 180, 14, 70, 200, 550, 750, 950));
		addProperty(new ColoredProperty("Mexico City", PropertyColors.ORANGE, 200, 16, 80, 220, 600, 800, 1000));
		
		addProperty(new TileFreeParking());
		
		addProperty(new ColoredProperty("Cairo", PropertyColors.RED, 220, 18, 90, 250, 700, 875, 1050));
		addProperty(new TileChance());
		addProperty(new ColoredProperty("Seoul", PropertyColors.RED, 220, 18, 90, 250, 700, 875, 1050));
		addProperty(new ColoredProperty("Jakarta", PropertyColors.RED, 240, 20, 100, 300, 750, 925, 1100));
		addProperty(new TransportProperty("Ocean Transport"));
		addProperty(new ColoredProperty("Sao Paulo", PropertyColors.YELLOW, 260, 22, 110, 330, 800, 975, 1150));
		addProperty(new ColoredProperty("Moscow", PropertyColors.YELLOW, 260, 22, 110, 330, 800, 975, 1150));
		addProperty(new UtilityProperty("Power Plant"));
		addProperty(new ColoredProperty("Mumbai", PropertyColors.YELLOW, 280, 24, 120, 360, 850, 1025, 1200));
		
		addProperty(new TileGoToJail());
		
		addProperty(new ColoredProperty("Tokyo", PropertyColors.GREEN, 300, 26, 130, 390, 900, 1100, 1275));
		addProperty(new ColoredProperty("Istanbul", PropertyColors.GREEN, 300, 26, 130, 390, 900, 1100, 1275));
		addProperty(new TileCommunityChest());
		addProperty(new ColoredProperty("Delhi", PropertyColors.GREEN, 320, 28, 150, 450, 1000, 1200, 1400));
		addProperty(new TransportProperty("Space Transport"));
		addProperty(new TileChance());
		addProperty(new ColoredProperty("Beijing", PropertyColors.DARK_BLUE, 350, 35, 175, 500, 1100, 1300, 1500));
		addProperty(new TileLuxuryTax());
		addProperty(new ColoredProperty("Shanghai", PropertyColors.DARK_BLUE, 400, 50, 200, 600, 1400, 1700, 2000));
	}
	
	private static void addProperty(Property property) {
		if(properties.contains(property)) return; //Prevent duplicates
		properties.add(property);
		//Change the loading progress. Once all properties have loaded, the bar will be at 100% completed.
		Screens.LOADING.setLoading(startProgress + ((properties.size() / 40f) * (1 - startProgress)));
	}
	
	/**
	 * Resets all properties. Don't call this method mid-game because it'll probably break something
	 */
	public static void reset() {
		for(Property prop : getProperties()) prop.reset();
	}
	
	/**
	 * @return A list of all the properties in the game. Due to each property potentially having to load in an image, 
	 * which takes a little while to load, property instances are NOT copied. When this list to added to the board, and then, for example, 
	 * a property is mortgaged, this will affect the value in the list. Call the {@link #reset()} method in order to reset 
	 */
	public static List<Property> getProperties() {
		return properties;
	}
}
