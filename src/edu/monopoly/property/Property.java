package edu.monopoly.property;

import edu.monopoly.board.Board;
import edu.monopoly.player.Player;
import edu.monopoly.screen.ScreenPlay;

//Abstract class, so cannot be instantiated, must be extended
public abstract class Property {

	private final String name; //Display name of the property
	
	public Property(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void reset() {}
	public void onLand(Player player, ScreenPlay screen, Board board) {} //Called when a player lands on the property
	public void onPass(Player player, ScreenPlay screen, Board board) {} //Called when a player passes the property but not landing
}
