package edu.monopoly.property;

import java.awt.Color;

/**
 * Contains a list of all the property colours in the game. Note that there are also Transport and Utility colours, 
 * despite those spaces not being a ColoredProperty. This property mostly refers to the colour shown at the top
 * of the card information in game.
 */
public enum PropertyColors {
	
	BROWN(149, 84, 56), 
	LIGHT_BLUE(169, 225, 250),
	PURPLE(216, 57, 150), 
	ORANGE(247, 148, 29), 
	RED(237, 27, 36), 
	YELLOW(254, 242, 0),
	GREEN(31, 178, 90), 
	DARK_BLUE(0, 114, 187),
	
	TRANSPORT(120, 120, 120), 
	UTILITY(20, 20, 20);
	
	PropertyColors(Color rgbColor) {
		this.color = rgbColor;
	}
	
	PropertyColors(int red, int green, int blue) {
		this(new Color(red, green, blue));
	}
	
	private Color color;
	
	public Color getColor() {
		return color;
	}
}
