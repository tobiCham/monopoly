package edu.monopoly.property;

import java.awt.Image;

import edu.monopoly.util.ImageLoader;

/**
 * Each instance represents a property on the board which can be purchased. This is all coloured properties, as well as utilities
 * and transport spaces.
 */
public abstract class PurchasableProperty extends Property {

	private final int cost, mortgageValue;
	private final Image image;
	private boolean mortgaged = false;
	
	public PurchasableProperty(String name, int cost) {
		super(name);
		this.cost = cost;
		this.mortgageValue = cost / 2;
		//Loads in the image as property/[name].png, e.g. property/london.png
		image = ImageLoader.loadImage("property/" + name.toLowerCase().replace(" ", "_") + ".png");
	}

	@Override
	public void reset() {
		super.reset();
		mortgaged = false;
	}

	/**
	 * @return The initial cost of the property to buy
	 */
	public int getCost() {
		return cost;
	}
	
	public int getMortgageValue() {
		return mortgageValue;
	}

	public boolean isMortgaged() {
		return mortgaged;
	}

	public void setMortgaged(boolean mortgaged) {
		this.mortgaged = mortgaged;
	}
	
	/**
	 * @return The icon image for the property. This is usually a picture of the City
	 */
	public Image getImage() {
		return image;
	}
	
	/**
	 * @return The value of the property, taking into account whether the property is mortgaged
	 */
	public int getValue() {
		//If mortgaged, return the mortgage value, otherwise, return the cost of the property
		return mortgaged ? mortgageValue : cost;
	}
	
	/**
	 * @return The rent owed for one of these properties owned, without considering whether the property is mortgaged
	 * 		   or has buildings built on.
	 */
	public int getRent() {
		return getRent(1);
	}
	
	/**
	 * @return The raw rent owed for the property, without considering whether the property is mortgaged
	 * 		   or has buildings built on. 
	 */
	public abstract int getRent(int owned);
}
