package edu.monopoly.property;

import edu.monopoly.Monopoly;
import edu.monopoly.board.Board;
import edu.monopoly.card.Card;
import edu.monopoly.card.Cards;
import edu.monopoly.player.Player;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenCardInfo;
import edu.monopoly.screen.ScreenPlay;

public class TileChance extends Property {

	public TileChance() {
		super("Chance");
	}
	
	@Override
	public void onLand(Player player, ScreenPlay screen, Board board) {
		Card chance = Cards.randomChance(); //Select a random chance card
		
		//Show the Card info screen. When the ok button is clicked, call the handleCard method for the player who landed on the property
		ScreenCardInfo info = new ScreenCardInfo(Monopoly.getGameInstance(), chance, () -> player.handleCard(chance, screen, board));
		info.init();
		Screen.setSelectedScreen(info);
	}
}
