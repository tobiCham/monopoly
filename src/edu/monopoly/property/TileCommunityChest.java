package edu.monopoly.property;

import edu.monopoly.Monopoly;
import edu.monopoly.board.Board;
import edu.monopoly.card.Card;
import edu.monopoly.card.Cards;
import edu.monopoly.player.Player;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenCardInfo;
import edu.monopoly.screen.ScreenPlay;

public class TileCommunityChest extends Property {

	public TileCommunityChest() {
		super("Community Chest");
	}
	
	@Override
	public void onLand(Player player, ScreenPlay screen, Board board) {
		Card chest = Cards.randomCommunityChest(); //Select a random community chest card
		
		//Show the Card info screen. When the ok button is clicked, call the handleCard method for the player who landed on the property
		ScreenCardInfo info = new ScreenCardInfo(Monopoly.getGameInstance(), chest, () -> player.handleCard(chest, screen, board));
		info.init();
		Screen.setSelectedScreen(info);
	}
}
