package edu.monopoly.property;

import edu.monopoly.Monopoly;
import edu.monopoly.Question;
import edu.monopoly.QuestionManager;
import edu.monopoly.board.Board;
import edu.monopoly.player.HumanPlayer;
import edu.monopoly.player.Player;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.question.ScreenQuestion;

public class TileFreeParking extends Property {

	public TileFreeParking() {
		super("Free Parking");
	}
	
	@Override
	public void onLand(Player player, ScreenPlay play, Board board) {
		if(!(player instanceof HumanPlayer)) return; //Only human players can answer the question
		
		Question question = QuestionManager.generateRandomQuestion(board.getDifficulty()); //Generate a random question with the current difficulty
		String text = "You landed on Free Parking.\nAnswer the following question:\n\n" + question.getQuestion();
		
		//Show the question screen
		ScreenQuestion screen = new ScreenQuestion(Monopoly.getGameInstance(), text, question.getAnswer(), null);
		screen.init();
		Screen.setSelectedScreen(screen);
	}
}
