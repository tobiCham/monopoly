package edu.monopoly.property;

import edu.monopoly.MoneyValues;
import edu.monopoly.Monopoly;
import edu.monopoly.Question;
import edu.monopoly.QuestionManager;
import edu.monopoly.board.Board;
import edu.monopoly.player.ComputerPlayer;
import edu.monopoly.player.Player;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.question.ScreenQuestion;

public class TileGo extends Property {

	public TileGo() {
		super("Go");
	}
	
	@Override
	public void onLand(final Player player, ScreenPlay screen, Board board) {
		//If the player is a computer player, add the money gained from passing go to their account, and add a message to the game
		if(player instanceof ComputerPlayer) {
			screen.addInfoText(player.getName() + " has gained �" + MoneyValues.PASS_GO + " for passing Go!");
			player.setMoney(player.getMoney() + MoneyValues.PASS_GO);
		} else {
			//Otherwise, show a question for the player. 
			Question question = QuestionManager.generateRandomQuestion(board.getDifficulty());
			String text = "You landed on Go.\nAnswer the question for �200:\n\n" + question.getQuestion();
			
			//If the player receives the question correct, they gain the money for passing go
			//Otherwise, the player only receives half that money
			Runnable succeed = () -> player.setMoney(player.getMoney() + MoneyValues.PASS_GO);
			Runnable fail = () -> player.setMoney(player.getMoney() + MoneyValues.PASS_GO / 2);
			
			ScreenQuestion newScreen = new ScreenQuestion(Monopoly.getGameInstance(), text, question.getAnswer(), 3, succeed, fail);
			newScreen.setSucceedMessage("Correct!\nYou gain " + MoneyValues.PASS_GO);
			newScreen.init();
			Screen.setSelectedScreen(newScreen);
		}
	}
	
	@Override
	public void onPass(Player player, ScreenPlay screen, Board board) {
		onLand(player, screen, board);
	}
}
