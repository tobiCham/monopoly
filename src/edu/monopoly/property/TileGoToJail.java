package edu.monopoly.property;

import edu.monopoly.board.Board;
import edu.monopoly.player.HumanPlayer;
import edu.monopoly.player.Player;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.sound.Sounds;

public class TileGoToJail extends Property {

	public TileGoToJail() {
		super("Go to Jail");
	}
	
	@Override
	public void onLand(final Player player, ScreenPlay screen, Board board) {
		if(player instanceof HumanPlayer) {
			//Show a message telling the player they have been sent to Jail, then set them in Jail
			Screen.showConfirmMessage("You landed on go to Jail!", () -> {
				player.setInJail(true);
				player.setJailAttempts(0);
				player.setBoardPosition(10);
			});
		} else {
			//Add a message to the game saying the player has been sent to Jail, then set them in Jail
			screen.addInfoText(player.getName() + " has been sent to Jail!");
			player.setInJail(true);
			player.setJailAttempts(0);
			player.setBoardPosition(10);
		}
		Sounds.SIREN.play();
	}
}
