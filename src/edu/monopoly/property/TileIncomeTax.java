package edu.monopoly.property;

import static edu.monopoly.MoneyValues.*;
import edu.monopoly.board.Board;
import edu.monopoly.player.ComputerPlayer;
import edu.monopoly.player.HumanPlayer;
import edu.monopoly.player.Player;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.question.ScreenQuestion;

public class TileIncomeTax extends Property {

	public TileIncomeTax() {
		super("Income Tax");
	}
	
	@Override
	public void onLand(final Player player, ScreenPlay screen, Board board) {
		//INCOME_TAX here a static import from MoneyValues.INCOME_TAX
		
		if(player instanceof ComputerPlayer) {
			//If the player is a computer player, they lose �200 (default value for Income tax)
			((ComputerPlayer) player).payMoney(screen, board, player.getName() + " landed on Income Tax and paid �" + INCOME_TAX, INCOME_TAX);
		} else {
			if(player.getMoney() >= INCOME_TAX) { //The player has enough money to pay
				String question = "You landed on Income Tax. You currently have\n";
				question += "�" + player.getMoney() + " and pay �" + INCOME_TAX + ".\n\nHow much money do you now have?";

				//Show the player the question asking how much money they have left.
				ScreenQuestion newScreen = new ScreenQuestion(screen.getGame(), question, player.getMoney() - INCOME_TAX, () -> player.setMoney(player.getMoney() - INCOME_TAX));
				newScreen.init();
				Screen.setSelectedScreen(newScreen);
			} else {
				//The player doesn't have enough money, so send them a message and add the Income tax value to the current money owed.
				Screen.showConfirmMessage("You don't have enougn money\nto pay for this.\n\nMortgage or sell to\npay off this debt.", null);
				HumanPlayer human = (HumanPlayer) player;
				human.setMoneyOwed(human.getMoneyOwed() + INCOME_TAX);
			}
		}
	}
}
