package edu.monopoly.property;

public class TransportProperty extends PurchasableProperty {

	public TransportProperty(String name) {
		super(name, 200);
	}

	@Override
	public int getRent(int owned) {
		//Values for the rent owed.
		if(owned == 1) return 20;
		if(owned == 2) return 50;
		if(owned == 3) return 100;
		if(owned == 4) return 200;
		return 0;
	}
}
