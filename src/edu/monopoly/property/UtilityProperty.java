package edu.monopoly.property;

public class UtilityProperty extends PurchasableProperty {

	public UtilityProperty(String name) {
		super(name, 150);
	}

	@Override
	public int getRent(int owned) {
		//Return 0, this is handled elsewhere 
		return 0;
	}

}
