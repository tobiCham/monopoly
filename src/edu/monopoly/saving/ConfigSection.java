package edu.monopoly.saving;

import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//Extend HashMap to easily store data. This is allows easy serialisation using GSON. See the SavingManager class about this.
public class ConfigSection extends HashMap<String, Object> {

	public ConfigSection set(String key, Object value) {
		//If someone actually did this, it would cause a stack-overflow from the recursive loop.
		//Tracing through the process used, its pretty clear this is that case:
		//  The only value in here is the map, so get the value of this:
		//  Its a HashMap - and so we loop through all the values in the map. 
		//  There's only one value in this map, so get the value of this
		//  Its the same HashMap, so we loop through all its values...
		if(value == this) throw new IllegalAccessError("Can't set the config to the config");
		put(key, value);
		return this;
	}

	/**
	 * @return A config section with the specified key.
	 */
	public ConfigSection getSection(String key) {
		Object value = get(key);
		if(value == null) return null;
		return getType(ConfigSection.class, value);
	}
	
	public List<ConfigSection> getSectionList(String key) {
		Object value = get(key);
		if(value == null) return null;
		Gson gson = SavingManager.getGson();
		//Due to complexities in deserialising the JSON used to save the file, first the value has to be converted to JSON and then 
		//retrieved back as a List of config sections. If not done this way, the value retrieved would be of type List<Map>, which is not what is wanted
		return gson.fromJson(gson.toJson(value), new TypeToken<List<ConfigSection>>() {}.getType());
	}
	
	public List<Integer> getIntList(String key) {
		Object value = get(key);
		if(value == null) return null;
		Gson gson = SavingManager.getGson();
		//Similar situation as with the method to get a list of ConfigSections. Read the comments in that method for more information on the process
		//Of this method
		return gson.fromJson(gson.toJson(value), new TypeToken<List<Integer>>() {}.getType());
	}
	
	public <T> T getType(String key, Class<T> clas) {
		Object value = get(key);
		if(value == null) return null;
		Gson gson = SavingManager.getGson();
		//Here, the value of object, if it is a class and not a primitive data type, will be a Map of all the fields in that class
		//This means that the value needs to be first be converted to json, and then back again, but this time specifying what type 
		//The JSON is of.
		return gson.fromJson(gson.toJson(value), clas);
	}
	
	private <T> T getType(Class<T> clas, Object value) {
		//Converts the config object to the specified type. Read the commnets in the getType method which has a Class as a parameter for more information
		//on why this is done
		Gson gson = SavingManager.getGson();
		return gson.fromJson(gson.toJson(value), clas);
	}

	//Methods to get a specific primitive data type from the config section
	
	public int getInteger(String key) {
		Object obj = get(key);
		if(obj == null) return 0;
		if(obj instanceof Integer) return (int) obj;
		return (int) getDouble(key);
	}

	public double getDouble(String key) {
		Object obj = get(key);
		if(obj == null) return 0;
		if(obj instanceof Double) return (double) obj;
		return Double.parseDouble(obj.toString());
	}

	public float getFloat(String key) {
		Object obj = get(key);
		if(obj == null) return 0;
		if(obj instanceof Float) return (float) obj;
		return (float) getDouble(key);
	}

	public byte getByte(String key) {
		Object obj = get(key);
		if(obj == null) return 0;
		if(obj instanceof Byte) return (byte) obj;
		return (byte) getDouble(key);
	}

	public short getShort(String key) {
		Object obj = get(key);
		if(obj == null) return 0;
		if(obj instanceof Short) return (short) obj;
		return (short) getDouble(key);
	}

	public long getLong(String key) {
		Object obj = get(key);
		if(obj == null) return 0;
		if(obj instanceof Long) return (long) obj;
		return (long) getDouble(key);
	}

	public boolean getBoolean(String key) {
		Object obj = get(key);
		if(obj == null) return false;
		if(obj instanceof Boolean) return (boolean) obj;
		return obj != null && Boolean.parseBoolean(obj.toString());
	}

	public String getString(String key) {
		Object obj = get(key);
		return (obj == null) ? null : obj.toString();
	}

	/**
	 * @param key The key to check if the config section contains
	 * @return Whether or not the config section contains the key specified
	 */
	public boolean contains(String key) {
		return containsKey(key);
	}
}
