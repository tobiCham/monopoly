package edu.monopoly.saving;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.tobi.guibase.RenderSetting;

public class GameProperties {
	
	private static PropertiesFile properties;
	
	public static void load(File propertiesFile) {
		properties = new PropertiesFile(propertiesFile);
		
		Map<String, Object> initialProperties = new HashMap<>();
		initialProperties.put("sound", true);
		initialProperties.put("music", true);
		initialProperties.put("quality", RenderSetting.GOOD.toString());
		
		if(propertiesFile.exists()) { //If the properties file exists on the users computer, attempt to load the properties
			try {
				properties.load();
			} catch (IOException e) {
				e.printStackTrace(); //Failing to load the properties, ignore it and re-write the default properties.
			}
		}
		
		boolean missingKey = false;
		//Loop through all the keys in the initial properties.
		//If a key is missing from the properties file, put the default property and set the missingKey variable to true to indicate this
		for(String key : initialProperties.keySet()) {
			if(!properties.containsKey(key)) {
				properties.put(key, initialProperties.get(key));
				missingKey = true;
			}
		}
		if(missingKey) { //If any key was missing from the properties, write the properties to disc.
			try {
				properties.save();
			} catch (IOException e) { 
				e.printStackTrace(); //Failing to write the properties, ignore it. Settings will be reset next time the game runs however
			}
		}
	}
	
	public static PropertiesFile getProperties() {
		return properties;
	}
}
