package edu.monopoly.saving;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFile {

	private Properties properties; //Underlying properties used to store the data
	private File file; //File to save or load properties to or from
	
	public PropertiesFile(File file) {
		this.file = file;
		properties = new Properties();
	}
	
	/**
	 * Loads in settings from the file specified in the constructor
	 */
	public void load() throws IOException {
		FileInputStream input = null;
		try {
			input = new FileInputStream(file); 
			properties.load(input); //Load the properties in from the file
			input.close();
		} catch(IOException e) {
			if(input != null) input.close();
			throw e; //Throw the exception again after making sure the input stream is closed
		}
	}
	
	/**
	 * Writes all the stores properties to the file specified in the constructor
	 */
	public void save() throws IOException {
		//If the folder the file is in doesn't exist, create it
		File parentFile = file.getParentFile();
		if(parentFile != null && !parentFile.exists()) parentFile.mkdirs();
		
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(file);
			properties.store(out, null); //Write the properties to the file
			out.close();
		} catch(IOException e) {
			if(out != null) out.close();
			throw e; //Throw the exception again after making sure the output stream is closed
		}
	}
	
	//Methods to get values from the config
	public Object get(String key) {
		return properties.get(key);
	}
	
	public int getInt(String key) {
		Object value = get(key);
		if(value == null) return 0;
		return Integer.parseInt(value + "");
	}
	
	public float getFloat(String key) {
		Object value = get(key);
		if(value == null) return 0;
		return Float.parseFloat(value + "");
	}
	
	public double getDouble(String key) {
		Object value = get(key);
		if(value == null) return 0;
		return Double.parseDouble(value + "");
	}
	
	public boolean getBoolean(String key) {
		Object value = get(key);
		return value != null && Boolean.parseBoolean(value + "");
	}
	
	public String getString(String key) {
		return properties.getProperty(key);
	}
	
	/**
	 * @param Stores a value in the properties. Nothing will be saved to the file unless the {@link #save()} method is called
	 */
	public void put(String key, Object value) {
		if(key == null || value == null) return;
		properties.put(key, value.toString());
	}
	
	/**
	 * Whether the properties contains a specific key
	 */
	public boolean containsKey(String key) {
		return properties.containsKey(key);
	}
	
	public Properties getProperties() {
		return properties;
	}
	
	public File getFile() {
		return file;
	}
}
