package edu.monopoly.saving;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.google.gson.Gson;

import edu.monopoly.screen.ScreenPlay;

public class SavingManager {

	private static Gson gson = new Gson();

	public static Gson getGson() {
		return gson;
	}
	
	/**
	 * Saves all data from the current game into a binary file on the user's computer
	 */
	public static void save(ScreenPlay game, File saveFile) throws Exception {
		//Create the folder the file is in if it doesn't exist
		if (!saveFile.getParentFile().exists()) saveFile.getParentFile().mkdirs();
		ConfigSection section = new ConfigSection();
		game.save(section); //Stores all the data from the current game into the config section
		
		//Serialises the game data to a JSON string. This is handled by Google's GSON library. A typical save string may look something like the following:
		/*
		{
		    "boardXOffset": -1445,
		    "boardYOffset": -3253,
		    "scale": 2.5,
		    "rolled": false,
		    "board": {
		        "difficulty": "EASY",
		        "currentPlayer": 0
		    },
		    "player": {
		        "inJail": false,
		        "money": 624,
		        "color": -27136,
		        "piece": "HAT",
		        "buildings": {},
		        "jailAttempts": 0,
		        "name": "Test Player",
		        "boardPosition": 24,
		        "owe": 0,
		        "properties": [3, 8, 24, 29, 31, 32],
		        "resigned": false,
		        "mortgaged": []
		    },
		    "player1": {
		        "inJail": false,
		        "money": 574,
		        "color": -1238236,
		        "piece": "IRON",
		        "buildings": {},
		        "jailAttempts": 0,
		        "name": "Computer 1",
		        "boardPosition": 11,
		        "properties": [5, 11, 13, 16, 28, 37],
		        "resigned": false,
		        "mortgaged": []
		    },
		    (other two player sections would be here, but is not shown to save space)
		}
		 */
		String json = gson.toJson(section); 
		byte[] bytes = json.getBytes(StandardCharsets.UTF_8); //Convert the string into a byte array using UTF-8 encoding
		
		try (GZIPOutputStream output = new GZIPOutputStream(new FileOutputStream(saveFile))) {
			//Write the byte array to the file using GZIP to compress the file so that it takes up less room on the user's computer
			output.write(bytes, 0, bytes.length);
		} catch (Exception e) {
			throw e; //Re-throw the exception. Using this type try-catch block automatically closes the output stream
		}
	}
	
	/**
	 * Loads the current game with the save data from the specified file
	 */
	public static void load(ScreenPlay game, File file) throws Exception {
		ConfigSection section = loadSection(file); //Load the config section from the file.
		game.load(section); //Load the game with the save data.
	}
	
	/**
	 * Loads a config section from the specified file.
	 */
	public static ConfigSection loadSection(File file) throws Exception {
		//If the file does not exist to load, throw an exception
		if (!file.exists()) throw new FileNotFoundException();
		
		//Due to not knowing how large the file is, this allows a single byte array to be retrieved after reading the file
		//As bytes are read, they are added to this buffer. It only stores the data in memory
		ByteArrayOutputStream byteOutput = null; 
		
		try(GZIPInputStream input = new GZIPInputStream(new FileInputStream(file))) {
			byteOutput = new ByteArrayOutputStream();
			
			//Read blocks of 64 bytes in from the file. Files should be maximum 512 bytes.
			while(true) {
				byte[] bytes = new byte[64];
				int len = input.read(bytes);
				
				if(len < 0) break; //Break out of the loop if there is nothing left to be read
				byteOutput.write(bytes, 0, len); //Otherwise, write the bytes just read from the file into the ByteArrayOutputStream.
			}
			byteOutput.close();
			
			byte[] bytes = byteOutput.toByteArray(); //Retrieve a single byte array. This is all the data of the file.
			
			//Convert the data in the file into a single string using UTF-8 encoding. The format of the string is JSON string, but see the save method
			//for more information on how this looks
			String str = new String(bytes, StandardCharsets.UTF_8);
			
			ConfigSection section = gson.fromJson(str, ConfigSection.class); //Serialise the data back into a ConfigSection using gson
			return section;
		} catch(Exception e) {
			//If there are any errors, close the bytearrayoutputstream, then re-throw the exception
			//This can happen if the file is corrupted
			if(byteOutput != null) byteOutput.close();
			throw e; 
		}
	}
}
