package edu.monopoly.screen;

import edu.monopoly.property.PurchasableProperty;

public class PropertyInfo {

	private PurchasableProperty property;
	private boolean left; //True if the property is shown on the left hand side, false if on the right hand side
	
	public PropertyInfo(PurchasableProperty property, boolean left) {
		this.property = property;
		this.left = left;
	}
	
	public PurchasableProperty getProperty() {
		return property;
	}
	
	public boolean isLeft() {
		return left;
	}
}
