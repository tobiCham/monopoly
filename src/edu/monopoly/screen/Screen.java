package edu.monopoly.screen;

import java.awt.Cursor;
import java.awt.Graphics;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.Transition;

public abstract class Screen {
	private Monopoly game;
	
	private static Screen selected = null;
	
	public Screen(Monopoly game) {
		this.game = game;
	}
	
	public abstract void init();
	public abstract void render(Graphics g);
	public abstract void update(double delta);
	
	//Event methods which can be overridden in a subclass
	public void screenEntered(Screen lastScreen) {}
	public void screenExited(Screen nextScreen) {}
	public void keyPressed(int key, char ch) {}
	public void keyReleased(int key, char ch) {}
	public void mouseClicked(int mouseButton) {}
	public void mouseReleased(int mouseButton) {}
	public void mouseScrolled(int scroll) {}
	
	public static Screen getSelectedScreen() {
		return selected;
	}
	
	public static void setSelectedScreen(Screen screen) {
		Transition.getActiveTransitions().clear(); //Clear the active transitions
		Monopoly.getGameInstance().getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); //Set the cursor to default
		Screen old = selected;
		Monopoly.getGameInstance().getDisplay().getMouse().releaseAllButtons(); //Release the mouse
		if (selected != null) {
			selected.screenExited(screen); //Call the screen exited method on the current screen
		}
		
		selected = screen;
		
		if (selected != null) {
			selected.screenEntered(old); //Call the screen entered method on the new screen
		}
	}
	
	public final Monopoly getGame() {
		return this.game;
	}

	/**
	 * Shows a message to the player
	 * @param onFinish Runnable callback to be called when the user clicks Ok
	 */
	public static void showConfirmMessage(String message, Runnable onFinish) {
		ScreenInfo info = new ScreenInfo(Monopoly.getGameInstance(), message, onFinish);
		info.init();
		setSelectedScreen(info);
	}
}
