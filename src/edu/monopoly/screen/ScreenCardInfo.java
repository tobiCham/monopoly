package edu.monopoly.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.card.Card;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.util.GraphicUtils;

public class ScreenCardInfo extends Screen {

	private Card card; //Card to display info about
	private Runnable runnable; //Method to run when the Ok button is clicked
	private Screen previousScreen; //Previous screen to return to
	private OptionButton<Object> buttonOk; //Ok button which the user clicks when they have read the card
	
	public ScreenCardInfo(Monopoly game, Card card, Runnable onFinish) {
		super(game);
		this.card = card;
		this.runnable = onFinish;
	}
	
	@Override
	public void init() {
		int width = 400;
		Color c = new Color(180, 180, 180);
		buttonOk = new  OptionButton<>(getGame(), (1920 / 2) - (width / 2), 700, width, 75, c, c, Color.black, "Ok", new Object[] {});
		buttonOk.setOnClick(() -> {
			//When the button is clicked, return to the previous screen and run the runnable if it isn't null
			Screen.setSelectedScreen(previousScreen);
			if(runnable != null) runnable.run();
		});
	}

	@Override
	public void render(Graphics g) {
		if(previousScreen != null) previousScreen.render(g); //Render the previous screen as a background
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 28)); //Set the font to size 28
		
		//Draw a black semi-transparent rectangle over the whole screen to darken the background
		g.setColor(new Color(0, 0, 0, 120));
		g.fillRect(0, 0, 1920, 1080);
		
		int width = 550;
		int height = 550;
		
		//Draw a white box with black border to display the card information in
		g.setColor(Color.black);
		g.fillRect((1920 /2) - (width / 2) - 8, (1080 / 2) - (height / 2) - 8, width + 16, height + 16);
		
		g.setColor(Color.white);
		g.fillRect((1920 /2) - (width / 2), (1080 / 2) - (height / 2), width, height);
		
		g.setColor(Color.black);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 36)); //Set the font to size 36
		
		//Render the the card text in the textbox, using the method which will move onto new lines automatically.
		GraphicUtils.renderText(g, card.getText(), getGame().getDisplay().getInitialWidth() / 2, 330, 30, (int) (g.getFont().getSize() * 1.2f));
		
		buttonOk.render(g);
	}

	@Override
	public void update(double delta) {
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		buttonOk.mouseClick(mouseButton, mouse.getX(), mouse.getY()); //Forward the mouse click to the button
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		previousScreen = lastScreen;
	}

}
