package edu.monopoly.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.File;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.CharacterSet;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.graphic.TextBox;
import edu.monopoly.util.ButtonUtils;
import edu.monopoly.util.FileUtils;
import edu.monopoly.util.GraphicUtils;

public class ScreenFileNameInput extends Screen {

	private Screen previousScreen;
	private TextBox box; //Text box to enter file name into
	private OptionButton<Object> buttonOk;
	private ScreenPlay playScreen;
	private Runnable runnable; //Method to run when the button is clicked
	
	private static final String TEXT = "Enter save name below";
	
	public ScreenFileNameInput(Monopoly game, ScreenPlay playScreen, Runnable onChanged) {
		super(game);
		this.playScreen = playScreen;
		this.runnable = onChanged;
	}
	
	@Override
	public void init() {
		Color c = new Color(180, 180, 180);
		
		box = new TextBox(660, 505, 600, 75, Color.black, c, CharacterSet.LETTERS_NUMBERS);
		box.setCharacterLimit(25);
		
		buttonOk = new  OptionButton<>(getGame(), 710, 700, 500, 75, c, c, Color.black, "Ok", new Object[] {});
		buttonOk.setOnClick(() -> {
			//When the button is clicked, set the save name to the text in the box (removing whitespace), return to the previous screen
			playScreen.setSaveName(box.getText().trim());
			Screen.setSelectedScreen(previousScreen);
			if(runnable != null) runnable.run();
		});
		
		reset();
		check();
	}
	
	/**
	 * Resets the text box, removing text and setting background text
	 */
	private void reset() {
		box.setText("");
		box.setCursorPosition(0);
		box.setBlankText("Enter Save name");
		box.setCharacterLimit(30);
		box.setSelected(true);
	}

	@Override
	public void render(Graphics g) {
		if(previousScreen != null) previousScreen.render(g); //Render the previous screen as a background
		
		//Temporary variables used to calculate the bounds of the box
		int width = 650;
		int height = 550;
		
		g.setColor(new Color(0, 0, 0, 120));
		g.fillRect(0, 0, 1920, 1080);
		
		//Render the outline for the box
		g.setColor(Color.black);
		g.fillRect((1920 /2) - (width / 2) - 8, (1080 / 2) - (height / 2) - 8, width + 16, height + 16);
		
		//Render the box
		g.setColor(Color.white);
		g.fillRect((1920 /2) - (width / 2), (1080 / 2) - (height / 2), width, height);
		
		//Set the colour and font size, then draw the text in the centre of the screen
		g.setColor(Color.black);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 36));
		GraphicUtils.drawCenteredString(g, TEXT, 345);
		
		//Render the textbox
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 50));
		box.render(g);
		
		//Render the button
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 42));
		buttonOk.render(g);
	}

	@Override
	public void update(double delta) {
		box.update(); //Update the text box
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		box.keyPress(key, ch);
		check(); //Check the input when a key is pressed
	}
	
	/**
	 * Enables or disables the button, checks to make sure that a file with that name can be saved and the user has entered a file name
	 */
	private void check() {
		//Loop through all the save file is in the directory. If any file has the same name as the one entered, the existing variable will be set to true
		File[] files = new File(FileUtils.getGameDirectory(), "saves").listFiles();
		boolean existing = false;
		if(files != null) {
			for(File f : files) {
				String name = FileUtils.getName(f);
				//Check if the names are the same ignoring the capitalisation. Note though, that Linux does differentiate between capitalisation for file names
				//So ignoring capitalisation is for windows and not linux
				if(name.equalsIgnoreCase(box.getText().trim())) {
					existing = true;
				}
			}
		}
		
		String reason = existing ? "File already exists" : "Enter a file name"; //Only used if the button is disabled
		
		//Enabled if the file doesn't exist and there is text in the box
		boolean enabled = !existing && box.getText() != null && !box.getText().trim().isEmpty();
		ButtonUtils.setEnabled(buttonOk, new Color(180, 180, 180), new Color(80, 80, 80), reason, enabled);
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		box.mouseClick(mouseButton);
		buttonOk.mouseClick(mouseButton, mouse.getX(), mouse.getY());
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		if(!(lastScreen instanceof ScreenInfo)) previousScreen = lastScreen;
	}
}
