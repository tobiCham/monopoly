package edu.monopoly.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.util.GraphicUtils;

public class ScreenInfo extends Screen {

	private String message; //Message to display to the user
	private Runnable runnable; //Method to run when the user clicks Ok
	private Screen previousScreen; //The previous screen to return to
	private OptionButton<Object> buttonOk; //Ok button which the user can click
	
	public ScreenInfo(Monopoly game, String message, Runnable onFinish) {
		super(game);
		this.message = message;
		this.runnable = onFinish;
	}
	
	@Override
	public void init() {
		Color c = new Color(180, 180, 180); //Gray colour for the button
		buttonOk = new OptionButton<>(getGame(), 760, 700, 400, 75, c, c, Color.black, "Ok", new Object[] {});
		buttonOk.setOnClick(() -> {
			//When clicked, return to the previous screen and run the runnable if it isn't null
			Screen.setSelectedScreen(previousScreen);
			if(runnable != null) runnable.run();
		});
	}

	@Override
	public void render(Graphics g) {
		if(previousScreen != null) previousScreen.render(g); //Render the previous screen as a background
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 30));
		
		int width = 550;
		int height = 550;
		
		g.setColor(new Color(0, 0, 0, 120));
		g.fillRect(0, 0, 1920, 1080);
		
		//Draw a black border around the box 
		g.setColor(Color.black);
		g.fillRect((1920 /2) - (width / 2) - 8, (1080 / 2) - (height / 2) - 8, width + 16, height + 16);
		
		//Draw the white box
		g.setColor(Color.white);
		g.fillRect((1920 /2) - (width / 2), (1080 / 2) - (height / 2), width, height);
		
		g.setColor(Color.black);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 36));
		
		//Split the message into an array splitting at new line
		int lineDiff = g.getFont().getSize();
		String[] parts = message.split("\n");
		for(int i = 0; i < parts.length; i++) {
			String part = parts[i];
			//Draw each line in the centre of the screen a line below the previous
			GraphicUtils.drawCenteredString(g, part, (int) (330 + (i * lineDiff * 1.2f)));
		}
		
		buttonOk.render(g);
	}

	@Override
	public void update(double delta) {
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		buttonOk.mouseClick(mouseButton, mouse.getX(), mouse.getY());
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		previousScreen = lastScreen;
	}

}
