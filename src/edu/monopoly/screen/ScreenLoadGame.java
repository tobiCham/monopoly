package edu.monopoly.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tobi.guibase.Mouse;

import edu.monopoly.Difficulty;
import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.graphic.Transition;
import edu.monopoly.player.PieceType;
import edu.monopoly.saving.ConfigSection;
import edu.monopoly.saving.SavingManager;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.ButtonUtils;
import edu.monopoly.util.FileUtils;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.StringUtils;

public class ScreenLoadGame extends Screen {
	
	private List<File> files; //List of all the save files in the save folder
	private File selected = null; //Currently file which is selected and info shown for
	private int selectedIndex = -1; //Index in the list the selected file is.
	private List<String> information = new ArrayList<>(); //List of information to display for the selected file.
	
	private boolean corrupt = true; //Whether the current file is corrupted. Should only be accessed when the selected file is not null 
	
	private int scrollBar = 0; //Y position of the scroll bar
	private int startMousePos = -1, startPos = -1; //Used for dragging of the scroll bar
	private int scrollBarLength = 150; //Height of the scroll bar in pixels
	private OptionButton<?> buttonLoad, buttonDelete, buttonBack;
	

	public ScreenLoadGame(Monopoly game) {
		super(game);
	}

	@Override
	public void init() {
		files = new ArrayList<>(); //Set the files to an empty list
		
		buttonLoad = new OptionButton<>(getGame(), 1300, 720, 490, 75, Color.red, Color.red, Color.white, "Load Game", new Object[] {});
		buttonLoad.setOnClick(this::loadGame); //Call the loadGame method when the button is clicked
		
		buttonBack = new OptionButton<>(getGame(), 380, 950, 550, 75, Color.red, Color.red, Color.white, "Back to Main Menu", new Object[] {});
		buttonBack.setOnClick(() -> Screen.setSelectedScreen(Screens.TITLE)); //Return to the title screen when the button is clicked
		
		buttonDelete = new OptionButton<>(getGame(), 1300, 820, 490, 75, Color.red, Color.red, Color.white, "Delete", new Object[] {});
		buttonDelete.setOnClick(() -> {
			//Check with the user before deleting the save file.
			ScreenYesNo screen = new ScreenYesNo(getGame(), "Are you sure you want to delete\n'" + FileUtils.getName(selected) + "'?\n\nYou can't undo this!", () -> {
				selected.delete(); //Delete the file
				reloadFiles(); //Reloads the files in the list, as it will have changed after deleting the file
			}, null);
			screen.init();
			Screen.setSelectedScreen(screen);
		});
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, 1920, 1080); //Draw a black background
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 32));
		g.setColor(Color.white); //Set the font size to 32 and set the color to white
		
		g.fillRoundRect(130, 200, 1050, 700, 50, 50); //Draw the white background for the saves box
		
		Map<Integer, Rectangle> buttons = getVisibleButtons(); 
		for(int i : buttons.keySet()) { //Loop through all the button indexes
			File file = files.get(i);
			Rectangle bounds = buttons.get(i); //Get the corresponding button bounds
			if(selectedIndex == i) g.setColor(new Color(120, 120, 120)); //If this save file is the current index, use a different colour to render
			else {
				Mouse mouse = getGame().getDisplay().getMouse();
				Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2); //Bounding box for the mouse cursor
				if(mouseRect.intersects(bounds)) g.setColor(new Color(100, 100, 100)); //If the mouse is hovered over the save file button use a lighter colour
				else g.setColor(new Color(80, 80, 80)); //Otherwise use a darker colour
			}
			g.fillRoundRect(bounds.x, bounds.y, bounds.width, bounds.height, 20, 20); //Draw the save file button with the color set above
			
			String name = FileUtils.getName(file); //Get the file name
			boolean hadToTrim = false; //Used to check later whether the string had to be trimmed
			while(g.getFontMetrics(g.getFont()).getStringBounds(name, g).getBounds().width > 950) { //Constantly loop while the width of the string is more than 950 pixels
				name = name.substring(0, name.length() - 1); //Has the effect of removing the last character from the string
				hadToTrim = true;
			}
			//if the string had to be trimmed, remove another two characters and add a '...' which just indicates to the user it isn't the full file name
			if(hadToTrim) name = name.substring(0, name.length() - 2) + "..."; 
			
			g.setColor(Color.white);
			g.drawString(name, bounds.x + 10, bounds.y + g.getFontMetrics(g.getFont()).getAscent() + 15); //Finally draw the save file name
		}
		
		if(hasScrollBar()) {
			g.setColor(Color.black);
			g.fillRoundRect(1100, this.scrollBar + 220, 50, scrollBarLength, 20, 20); //Draw the scroll bar
		}
		
		//Draw a black box over the top and bottom of the save file list so the buttons are not visible outside the box
		g.setColor(Color.black);
		g.fillRect(130, 130, 1050, 70);
		g.fillRect(130, 900, 1050, 160);
		
		//Draw text showing information at the top of the screen
		g.setColor(Color.white);
		if(files.isEmpty()) g.drawString("You have no current saved games. Return to the title screen to create a new game!", 145, 80);
		else g.drawString("Load one of the save files below, or return to the main menu!", 145, 80);
		
		GraphicUtils.drawCenteredString(g, "Saves", 190, 655);
		
		if(selected != null) { //If a file is selected
			if(selected != null) GraphicUtils.drawCenteredString(g, "Info", 190, 1540);
			g.fillRoundRect(1290, 200, 500, 500, 50, 50); //Draw the information box background
			g.setColor(Color.black);
			for(int i = 0; i < information.size(); i++) { //Loop through each piece of information to display about the save file
				String info = information.get(i);
				if(info == null || info.trim().isEmpty()) continue;
				g.drawString(info, 1310, 250 + (i * 50)); //Draw each piece of information a line below the previous inside the information box
			}
		}
		
		//Render the buttons
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 40));
		if(selected != null) {
			buttonLoad.render(g);
			buttonDelete.render(g);
		}
		buttonBack.render(g);
	}

	@Override
	public void update(double delta) {
		if(hasScrollBar() && startPos >= 0 && startMousePos >= 0) { //If the scroll bar is currently being dragged
			Mouse mouse = getGame().getDisplay().getMouse();
			setScrollBar((mouse.getY() - startMousePos) + startPos);  //Move the scroll bar from the mouse position. 
		}
		
		//Enable the load button if there is a file selected and it isn't corrupted
		ButtonUtils.setEnabled(buttonLoad, Color.red, Color.gray, !corrupt && selected != null);
	}
	
	private void setScrollBar(int scrollBar) {
		//Limit the scroll bar position to a a minimum of 0 and a maximum of 662 + the scroll bar length
		if(scrollBar + scrollBarLength > 662) scrollBar = 662 - scrollBarLength; //Prevents it going too far down
		if(scrollBar < 0) scrollBar = 0; //Prevents it going to far up
		this.scrollBar = scrollBar;
	}
	
	private void loadGame() {
		Screen.setSelectedScreen(Screens.LOADING); //Enter into the loading screen for the benefit of the user
		
		new Thread(() -> {
			ScreenPlay screen = new ScreenPlay(getGame(), "Player", PieceType.DOG, Difficulty.MEDIUM); //Initialise the player screen
			screen.init();
			try {
				SavingManager.load(screen, selected); //Attempt to load the selected save file into the game
				screen.setSaveName(FileUtils.getName(selected)); //Set the save name of the game to the file name
				
				Transition transition = new Transition(); //Create a fade in animation. Should last 1 second
				transition.fadeIn(getGame().getDisplay().getRenderFps());
				transition.startTransition();
				
				Screen.setSelectedScreen(screen);
			} catch (Exception e) {
				e.printStackTrace();
				//If there is an error loading, return to this screen and send the user a message telling them the game failed to load the save file
				Screen.setSelectedScreen(this);
				Screen.showConfirmMessage("Failed to load save file", null);
			}
		}).start();
	}
	
	/**
	 * @param index Sets the selected index to selected - if it already selected it is deselected
	 */
	private void setSelected(int index) {
		if(index == selectedIndex) { //Index is already selected
			selected = null; //Deselect the button
			selectedIndex = -1;
			return;
		}
		//Otherwise, select the file and load data about it
		information.clear();
		
		selectedIndex = index;
		selected = files.get(selectedIndex);
		
		ConfigSection playerSection; //Will store data about the player
		try {
			ConfigSection section = SavingManager.loadSection(selected); //Try loading the save file
			playerSection = section.getSection("player"); //Set the player section to the section from the save file
			if(playerSection == null) throw new NullPointerException("No player section found"); //Throw an exception if missing player data
			corrupt = false; //The file isn't corrupted, able to load data
		} catch(Exception e) {
			//Error loading save file, presume it is corrupted.
			System.err.println(selected.getName() + " is invalid: " + e.getMessage());
			information.add("Invalid Save File"); //Tells the user it is an invalid save file
			corrupt = true;
			return;
		}
		 
		//Add game data information to the information list to display to the user
		information.add("Name: " + playerSection.getString("name"));
		information.add("Piece: " + StringUtils.format(playerSection.getType("piece", PieceType.class).toString()));
		information.add("Money: " + StringUtils.POUND_SIGN + playerSection.getInteger("money"));
		information.add("Properties: " + playerSection.getIntList("properties").size());
		
		Calendar calendar = Calendar.getInstance();
		// Day/Month/Year hour:minute
		SimpleDateFormat format = new SimpleDateFormat("d/MM/y k:m");
		
		try {
			BasicFileAttributes attr = Files.readAttributes(selected.toPath(), BasicFileAttributes.class);
			//Get the last time the file was modified in Milliseconds since January 1st 1979. Add on the number of milliseconds offset the user's computer is
			//from UTC so even if they are in a different timezone the time is still shown correctly.
			long modified = attr.lastModifiedTime().toMillis() + calendar.get(Calendar.ZONE_OFFSET);
			
			information.add(""); //Add a blank line as a space
			information.add("Last Saved: " + format.format(new Date(modified))); //Add the last time saved, formatting the time using the data format
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		
		if(hasScrollBar()) {
			Rectangle scrollRect = new Rectangle(1100, this.scrollBar + 220, 50, scrollBarLength);
			//If there is a scroll bar and the user clicks it, sets the startPos and startMousePos for scrolling
			if(mouseRect.intersects(scrollRect)) {
				startPos = scrollBar;
				startMousePos = mouse.getY();
			}
		}
		
		Map<Integer, Rectangle> buttons = getVisibleButtons();
		for(int index : buttons.keySet()) { //Loop through each index for the visible buttons
			Rectangle bounds = buttons.get(index);
			
			//If the button bounds intersects the mouse bounding box, play the click sound and set the button to selected
			if(bounds.intersects(mouseRect)) {
				setSelected(index);
				Sounds.CLICK.play();
				break;
			}
		}
		
		buttonBack.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		
		if(selected != null) { //If there is a selected file, call the click method on the buttons
			if(!corrupt) buttonLoad.mouseClick(mouseButton, mouse.getX(), mouse.getY());
			buttonDelete.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		}
	}
	
	@Override
	public void mouseReleased(int mouseButton) {
		//Reset the start positions for the scroll bar when the mouse is released
		startPos = -1; 
		startMousePos = -1;
	}
	
	@Override
	public void mouseScrolled(int scroll) {
		if(hasScrollBar()) { //If the scroll bar exists, move the scroll bar down
			setScrollBar(scrollBar + (scroll * 15));
		}
	}
	
	/**
	 * @return A map of the button ID to its bounds
	 */ 
	private Map<Integer, Rectangle> getVisibleButtons() {
		Map<Integer, Rectangle> rects = new LinkedHashMap<>(); //Temporary map to return
		
		int totalHeight = (85 * files.size()) - 680; //Calculate the total height of all the save files
		int offSet = (int) ((scrollBar / - (660f - scrollBarLength)) * totalHeight); //Offset to render the buttons at, from 0 to -totalHeight
		
		//Start index to render the buttons from - performance enhancement so only buttons which are visible are being rendered
		int start = (int) ((65 - offSet) / 85f) - 1;
		if(start < 0) start = 0; //Prevent the start going negative
		
		//Loop from the start to start + 10. Rendering maximum 10 save files, as the user can see a maximum of 10 buttons (some of the buttons may only
		//be partially visible)
		for(int i = start; i < files.size() && i < start + 10; i++) {
			int y = 220 + offSet + (i * 85);
			//Continue if not visible
			if(y < 220 - 90) continue;
			if(y > 220 + 700 + 90) continue;
			
			rects.put(i, new Rectangle(160, y, hasScrollBar() ? 920 : 970, 70));
		}
		return rects;
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		//In case coming from a screen where the aspect ratio is different (such as the title screen), resize the display render area
		getGame().getDisplay().setBackgroundColor(Color.black);
		getGame().getDisplay().resize(1920, 1080);
		
		//Don't reload files if the previous screen was a confirmation screen, otherwise the files would be refreshed when
		//the user cancels deleting a file
		if(!(lastScreen instanceof ScreenYesNo)) reloadFiles();
	}
	
	/**
	 * Resets the scroll position and reloads the files listed
	 */
	private void reloadFiles() {
		//Reset variables
		selected = null;
		selectedIndex = -1;
		scrollBar = 0;
		scrollBarLength = 150;
		files.clear();
		
		File savesFolder = new File(FileUtils.getGameDirectory(), "saves"); //Folder where saves are saved to
		if (savesFolder.exists()) {
			File[] fs = savesFolder.listFiles(); //Get all the files in the directory
			if (fs == null) return;
			
			for(File f : fs) {
				if (f.isFile()) files.add(f);  //Loop through all the files, and add the file if it isn't a directory
			}
			
			files.sort((f1, f2) -> { //Sort the files based on date last modified, with most recent files appearing first
				try {
					BasicFileAttributes attr = Files.readAttributes(f1.toPath(), BasicFileAttributes.class);
					BasicFileAttributes attr2 = Files.readAttributes(f2.toPath(), BasicFileAttributes.class);
					//Get the last time each of the files was modified and compare
					long modified1 = attr.lastModifiedTime().toMillis();
					long modified2 = attr2.lastModifiedTime().toMillis();
					return Long.compare(modified1, modified2) * -1;
				} catch (IOException e) {
					e.printStackTrace();
				}
				return f1.getName().compareTo(f2.getName()); //If failing to get the modified time, compare the names instead
			});
		} else savesFolder.mkdirs(); //If the save folder didn't exist, create it
		if(!hasScrollBar()) scrollBarLength = 150; //Reset the scroll bar length if there aren't enough files for a scroll bar
		else {
			scrollBarLength = (int) (600f / (files.size() / 8f)); //Calculates the new scroll bar length from the number of files
			if(scrollBarLength < 50) scrollBarLength = 50; //Cap the scroll bar length to a minimum of 50
		}
	}

	/**
	 * @return Whether there is a scroll bar - more than 8 files and there is
	 */
	private boolean hasScrollBar() {
		return files.size() > 8;
	}
}
