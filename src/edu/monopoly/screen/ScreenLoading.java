package edu.monopoly.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import edu.monopoly.Display;
import edu.monopoly.Monopoly;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.ImageLoader;

public class ScreenLoading extends Screen {

	private float loading = 0; //Percentage loaded, from 0 to 1
	private String loadingMessage = "Loading Game"; //Message to display to the user
	private static Image loadingImage;
	
	public ScreenLoading(Monopoly game) {
		super(game);
	}

	@Override
	public void init() {
		loadingImage = ImageLoader.loadImage("man_loading.png");
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.black);
		Display display = getGame().getDisplay();
		
		int loadingBarIndent = 100;
		g.setColor(Color.red);
		g.fillRect(loadingBarIndent, 100, display.getInitialWidth() - (loadingBarIndent * 2), 50); //Draw the red box the whole width of the bar
		g.setColor(Color.green);
		g.fillRect(loadingBarIndent, 100, (int) ((display.getInitialWidth() - (loadingBarIndent * 2)) * loading), 50); //Draw the green progress bar the percentage loaded of the way along
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 45));
		g.setColor(Color.white);
		GraphicUtils.drawCenteredString(g, loadingMessage + "...", 210); //Draw the loading message in the centre of the screen
		
		int width = 400;
		g.drawImage(loadingImage, (display.getInitialWidth() / 2) - (width / 2), 300, width, (int) (width * 1.5), null); //Draw the monopoly man image
	}

	@Override
	public void update(double delta) {
	}

	@Override
	public void screenEntered(Screen lastScreen) {
		getGame().getDisplay().resize(1024, 1024); //Set the correct size for this screen
	}

	public float getLoading() {
		return loading;
	}

	/**
	 * @param loading The percentage loaded, a value between 0 and 1
	 */
	public void setLoading(float loading) {
		if(loading < 0) loading = 0;
		if(loading > 1) loading = 1;
		this.loading = loading;
	}

	public String getLoadingMessage() {
		return loadingMessage;
	}

	public void setLoadingMessage(String loadingMessage) {
		this.loadingMessage = loadingMessage;
	}
	
	public static Image getLoadingImage() {
		return loadingImage;
	}
}
