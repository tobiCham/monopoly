package edu.monopoly.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.tobi.guibase.Mouse;

import edu.monopoly.Difficulty;
import edu.monopoly.Display;
import edu.monopoly.MoneyValues;
import edu.monopoly.Monopoly;
import edu.monopoly.PlayerAnimation;
import edu.monopoly.Question;
import edu.monopoly.QuestionManager;
import edu.monopoly.board.Board;
import edu.monopoly.card.Cards;
import edu.monopoly.graphic.FadeText;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.graphic.Screenshotter;
import edu.monopoly.player.ComputerPlayer;
import edu.monopoly.player.HumanPlayer;
import edu.monopoly.player.PieceType;
import edu.monopoly.player.Player;
import edu.monopoly.property.Property;
import edu.monopoly.property.PropertyColors;
import edu.monopoly.property.PurchasableProperty;
import edu.monopoly.saving.ConfigSection;
import edu.monopoly.screen.dice.ScreenDiceRoll;
import edu.monopoly.screen.options.ScreenGameOptions;
import edu.monopoly.screen.propertyview.ScreenPropertyView;
import edu.monopoly.screen.question.ScreenQuestion;
import edu.monopoly.screen.trade.ScreenTrade;
import edu.monopoly.screen.win.ScreenWin;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.ButtonUtils;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.StringUtils;

public class ScreenPlay extends Screen {

	private float scale = 1; //Value to scale the board by. A value of 1.1 would be a 10% increase
	private int boardXOffset = 0, boardYOffset = 0; //Amounts to offset the board in the X and Y direction. Applied as a sideways transformation
	private Board board;
	private Point movePoint, mousePoint; //Temporary variables used to handle dragging of the board
	
	private String playerName; //Temporary variable to hold the player's name until init() is called
	private PieceType piece; //Temporary variable to hold the player's piece until init() is called
	private Difficulty difficulty; //Temporary variable to hold the difficulty until init() is called
	
	private List<PlayerAnimation> animations = new ArrayList<>();  //List of all the piece animations currently occurring
	private List<FadeText> fadeText = new ArrayList<>(); //List of all the text shown to the user which fades
	
	//Buttons to allow the player to roll or end turn. These buttons may change text or action, such as 
	//Changing to paying back debt or resigning
	private OptionButton<Object> buttonRoll, buttonEndTurn; 
	private OptionButton<Object> tradep1, tradep2, tradep3; //Buttons to allow the player to trade with the AI players
	
	private boolean rolled = false; //Used to track whether the player has rolled. Allows the tracking of whether to enable the end turn button
	private Map<Runnable, Float> delayedTasks = new HashMap<>(); //Tasks to run later
	private String saveName; //Save file name, used so that the player doesn't have to re-enter the save name
	
	private Map<Player, Point> playerInfoRenderPoints = new HashMap<>(); //Stores the locations for where the information boxes should be rendered.
	
	public ScreenPlay(Monopoly game, String playerName, PieceType piece, Difficulty difficulty) {
		super(game);
		this.playerName = playerName;
		this.difficulty = difficulty;
		this.piece = piece;
	}

	@Override
	public void init() {
		board = new Board(this, playerName, piece, difficulty); //Create a new board instance
		
		//Add the player render points
		playerInfoRenderPoints.put(board.getHumanPlayer(), new Point(40, 70));
		playerInfoRenderPoints.put(board.getComputerPlayer1(), new Point(40, 620));
		playerInfoRenderPoints.put(board.getComputerPlayer2(), new Point(1540, 70));
		playerInfoRenderPoints.put(board.getComputerPlayer3(), new Point(1540, 620));
		
		//Initialise the buttons
		Color c = new Color(180, 180, 180);
		buttonRoll = new OptionButton<>(getGame(), 50, 435, 120, 50, c, c, Color.black, "Roll!", new Object[] {});
		buttonRoll.setOnClick(this::rollClicked); //Call the rollClicked method when the button is clicked
		
		buttonEndTurn = new OptionButton<>(getGame(), 185, 435, 180, 50, c, c, Color.black, "End Turn", new Object[] {});
		buttonEndTurn.setOnClick(this::endTurnClicked); //Call the endTurnClicked method when the button is clicked
		
		tradep1 = new OptionButton<>(getGame(), 85, 980, 250, 50, c, c, Color.black, "Trade", new Object[] {});
		tradep1.setOnClick(() -> tradeClicked(board.getComputerPlayer1())); //When button is clicked, trade with the 1st Computer player
		
		tradep2 = new OptionButton<>(getGame(), 1585, 430, 250, 50, c, c, Color.black, "Trade", new Object[] {});
		tradep2.setOnClick(() -> tradeClicked(board.getComputerPlayer2())); ////When button is clicked, trade with the 2nd Computer player
		
		tradep3 = new OptionButton<>(getGame(), 1585, 980, 250, 50, c, c, Color.black, "Trade", new Object[] {});
		tradep3.setOnClick(() -> tradeClicked(board.getComputerPlayer3())); //When button is clicked, trade with the 3rd Computer player
		Screens.LOADING.setLoading(0.95f);
	}
	
	private void rollClicked() {
		if(rolled) return; //Shouldn't be able to roll twice
		if(!(board.getCurrentPlayer() instanceof HumanPlayer)) return; //If the current player isn't the human player, return
		
		HumanPlayer humanPlayer = (HumanPlayer) board.getCurrentPlayer();
		if(humanPlayer.isInJail()) {
			if(humanPlayer.getJailAttempts() >= 3) {
				//The player has used all attempts up, so is forced to pay to get out of Jail
				if(humanPlayer.getMoney() >= MoneyValues.PAY_GET_OUT_OF_JAIL) {
					//User has enough money to pay to get out of jail, ask them a question asking how much money they will have after paying.
					String question  = "You currently have " + StringUtils.POUND_SIGN + humanPlayer.getMoney() + " and you pay\n";
					question += StringUtils.POUND_SIGN + "" + MoneyValues.PAY_GET_OUT_OF_JAIL + " to get out of Jail.\n\n";
					question += "How much money do you have left?";
					ScreenQuestion screen = new ScreenQuestion(getGame(), question, humanPlayer.getMoney() - MoneyValues.PAY_GET_OUT_OF_JAIL, () -> {
						//When the player gets the correct answer, remove the value of getting out of Jail from their account and remove them from jail
						humanPlayer.setMoney(humanPlayer.getMoney() - MoneyValues.PAY_GET_OUT_OF_JAIL);
						humanPlayer.setInJail(false);
						humanPlayer.setJailAttempts(0);
						rolled = true; //Should not be able to roll after getting out of jail
					});
					screen.init();
					Screen.setSelectedScreen(screen);
				} else {
					//Not enough money. Add debt to their account
					Screen.showConfirmMessage("You don't have enougn money\nto pay for this.\n\nMortgage or sell to\npay off this debt.", null);
					humanPlayer.setMoneyOwed(humanPlayer.getMoneyOwed() + MoneyValues.PAY_GET_OUT_OF_JAIL);
				}
			} else {
				//Hasn't used up all attempts to get out of jail.
				Question question = QuestionManager.generateRandomQuestion(board.getDifficulty()); //Generate a random question based on the difficulty set
				
				//Create the question screen, with a maximum of 2 attempts
				ScreenQuestion screen = new ScreenQuestion(getGame(), question.getQuestion(), question.getAnswer(), 2, () -> {
					//Getting the answer correct, set out of jail.
					humanPlayer.setInJail(false);
					humanPlayer.setJailAttempts(0);
					rolled = true; //Should not be able to roll after getting out of jail
				}, () -> {
					//Failing the question, add an extra attempt
					humanPlayer.setJailAttempts(humanPlayer.getJailAttempts() + 1);
					rolled = true; //Should not be able to roll after getting out of jail
				});
				screen.init();
				Screen.setSelectedScreen(screen);
			}
			return;
		} 
		//Not in jail, enter into the dice roll screen
		ScreenDiceRoll screen = new ScreenDiceRoll(getGame(), (numbers, total) -> {
			rolled = true;
			if(numbers.size() == 6 && numbers.get(4) == numbers.get(5)) { //Rolled 3 doubles, sent to Jail
				board.getHumanPlayer().setBoardPosition(10);
				board.getHumanPlayer().setInJail(true);
				board.getHumanPlayer().setJailAttempts(0);
			} else { //Animate the player around the board the total number of spaces rolled
				PlayerAnimation animation = new PlayerAnimation(this, board.getHumanPlayer(), total);
				animations.add(animation);
			}
		});
		screen.init();
		Screen.setSelectedScreen(screen);
	}
	
	private void endTurnClicked() {
		if(board.getCurrentPlayer() != board.getHumanPlayer()) return; //Can't end the turn if it isn't currently the player's turn
		HumanPlayer human = board.getHumanPlayer();
		if(human.getMoneyOwed() > 0) {
			//When the player owes money, the end turn button is converted into a Pay Back / Resign button
			if(human.getMoney() < human.getMoneyOwed()) {
				//Doesn't have enough money to pay back, so the button allows the player to resign.
				String message = "Are you sure you want\nto resign?\nYou will lose the game\nif you do this!\n\nOnly click yes if you have no\nother way of paying back\nthe money!";
				ScreenYesNo screen = new ScreenYesNo(getGame(), message, () -> {
					//If the user clicks yes, go to the Win screen. (Although it is the "win" screen, it is also the lose screen) and set the player to resigned
					human.setResigned(true);
					ScreenWin win = new ScreenWin(getGame(), this);
					win.init();
					Screen.setSelectedScreen(win);
				}, null);
				screen.init();
				Screen.setSelectedScreen(screen);
			} else {
				//Has enough money to pay back. Asks the user to enter how much money they have after paying
				String question = "You currently have " + StringUtils.POUND_SIGN + human.getMoney();
				question += " and you owe " + StringUtils.POUND_SIGN + human.getMoneyOwed();
				question += "\n\nHow much money do you have";
				question += "after paying?";
				
				ScreenQuestion screen = new ScreenQuestion(getGame(), question, human.getMoney() - human.getMoneyOwed(), () -> {
					//Remove the money owed from the player's balance, then set the amount of money they owe to 0
					human.setMoney(human.getMoney() - human.getMoneyOwed());
					human.setMoneyOwed(0);
					human.setJailAttempts(0); //In case the player owed money from trying to pay off Jail
					human.setInJail(false);
				});
				screen.init();
				Screen.setSelectedScreen(screen);
			}
		} else endTurn(); //Otherwise, the button should just end their turn, so call that method
	}
	
	private void tradeClicked(ComputerPlayer player) {
		if(board.getCurrentPlayer() != board.getHumanPlayer()) return; //Cannot trade when it is not the player's turn
		
		//Create the trade screen and enter into it
		ScreenTrade trade = new ScreenTrade(getGame(), getBoard(), this, player); 
		trade.init();
		Screen.setSelectedScreen(trade);
	}

	/**
	 * Ends the turn for the current player. Can be either the human player or a computer player
	 */
	public void endTurn() {
		for(Player p : board.getPlayers()) {
			//Loop through each player on the board. If its a computer player, clear their trade list. (This is the list of trades they have decided on)
			if(p instanceof ComputerPlayer) ((ComputerPlayer) p).clearTradeList();
		}
		//Run on a delayed task the following:
		runDelayedTask(() -> {
			Player current = board.getCurrentPlayer();
			Player next = board.getNextPlayer(); //The player who's turn it is next
			if(next instanceof HumanPlayer) {
				rolled = false; //If the next player is the human player, allow them to roll
			}
			board.setCurrentPlayer(next);
			current = board.getCurrentPlayer();
			
			addInfoText("It is now " + current.getName() + "'s turn!");
			
			if(current instanceof ComputerPlayer) {
				//If the new player is a computer player, after 2 seconds, run a delayed task after 2 seconds which:
				ComputerPlayer computerPlayer = (ComputerPlayer) current;
				runDelayedTask(() -> {
					//Starts the computer player's turn. If the method returns true, then their turn should automatically end. Do this on a 2 second
					//delay so the game doesn't go too quickly
					boolean endTurn = computerPlayer.startTurn(this, getBoard());
					if(endTurn) {
						runDelayedTask(() -> {
							addInfoText(computerPlayer.getName() + " has ended their turn.");
							endTurn();
						}, 2f);
					}
				}, 2f);
			}
		}, board.getCurrentPlayer() == board.getHumanPlayer() ? 0 : 3); //Run the task after immediately if its a human player, or 3 seconds if not
	}
	
	@Override
	public void render(Graphics g) {
		Display display = getGame().getDisplay();
		g.setColor(Color.black);
		g.fillRect(0, 0, display.getInitialWidth(), display.getInitialHeight()); //Draw a black background on the size of the window

		float aspectRatio = 1920f / 1080f;
		float newScale = scale / aspectRatio; //Scale the scale due to the board being rendered at 1920x1920 but needs to fit in 1080x1080 area
		float xOffset = (boardXOffset / aspectRatio) + 420; //Again, scale the x-transformation to fit. Add 420 pixels to centre
		float yOffset = boardYOffset  / aspectRatio; //Again, scale the y-transformation to fit
		
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.translate(xOffset, yOffset); //Translate where the board is being rendered to.
		g2d.scale(newScale, newScale); //Scale where the board is being rendered to fit the area
		board.render(g); //Render the board to the area
		g2d.scale(1f/newScale, 1f/newScale); //Undo the scale
		g2d.translate(-xOffset, -yOffset); //Undo the transformation
		
		//Fill black on the sides of the board. This prevents the board form being rendered over 
		g.setColor(Color.black); 
		g.fillRect(0, 0, 420, 1080);
		g.fillRect(1500, 0, 420, 1080);
		
		//Render each of the informations for each of the players. The Property variable assigned is the property which is hovered over.
		PurchasableProperty hover1 = renderPlayerInfo(g, board.getHumanPlayer(), playerInfoRenderPoints.get(board.getHumanPlayer())); 
		PurchasableProperty hover2 = renderPlayerInfo(g, board.getComputerPlayer1(), playerInfoRenderPoints.get(board.getComputerPlayer1()));
		PurchasableProperty hover3 = renderPlayerInfo(g, board.getComputerPlayer2(), playerInfoRenderPoints.get(board.getComputerPlayer2()));
		PurchasableProperty hover4 = renderPlayerInfo(g, board.getComputerPlayer3(), playerInfoRenderPoints.get(board.getComputerPlayer3()));
		
		//If any of the hovered properties aren't null, set the board overlay to it. (This outlines the property on the board to allow the user to 
		//see where the property is)
		if(hover1 != null) board.setOverlay(hover1);
		else if(hover2 != null) board.setOverlay(hover2);
		else if(hover3 != null) board.setOverlay(hover3);
		else if(hover4 != null) board.setOverlay(hover4);
		else board.setOverlay(null);
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 36));
		int maxWidth = 0;
		for(FadeText text : fadeText) {
			//Loop through all the fade texts to find the largest width to be displayed
			int width = g.getFontMetrics(g.getFont()).getStringBounds(text.getText(), g).getBounds().width;
			if(width > maxWidth) maxWidth = width;
		}
		if(maxWidth > 0) {
			//There is text to render, so draw a semi-transparent white box covering where the text will be rendered
			g.setColor(new Color(255, 255, 255, 160));
			g.fillRect((1920 / 2) - (maxWidth / 2) - 2, 164, maxWidth + 4, fadeText.size() * 45);
		}
		
		for(int i = 0; i < fadeText.size(); i++) {
			FadeText text = fadeText.get(i);
			
			//Calculates the transparency which the text should be. Below 75% through the time of the text, fully black
			//Above that, fade to fully transparent.
			float percentage = text.getTimeIn() / text.getMaxTime();
			float transparency = 1;
			float in = 0.75f;
			if(percentage >= in) {
				transparency = 1 - ((text.getTimeIn() - (text.getMaxTime() * in)) / (text.getMaxTime() - (text.getMaxTime() * in)));
			}
			if(transparency > 1) transparency = 1;
			g.setColor(new Color(0, 0, 0, transparency));
			GraphicUtils.drawCenteredString(g, text.getText(), 200 + (i * 45)); //Draw the text in the middle of the screen below the previous text
		}
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 31));
		
		//Render the buttons
		buttonRoll.render(g);
		buttonEndTurn.render(g);
		if(!board.getComputerPlayer1().hasResigned()) tradep1.render(g);
		if(!board.getComputerPlayer2().hasResigned()) tradep2.render(g);
		if(!board.getComputerPlayer3().hasResigned()) tradep3.render(g);
	}
	
	/**
	 * Renders the player information box for a player at the specified point on the screen
	 */
	private PurchasableProperty renderPlayerInfo(Graphics g, Player player, Point p) {
		int x = p.x;
		int y = p.y;
		
		//Render the outline for the player information box using the player's colour
		g.setColor(player.getColor());
		int indent = 8;
		g.fillRect(x - indent, y - indent, 340 + (indent * 2), 430 + (indent * 2));
		
		//Render the player's name at the top of the box
		g.setColor(Color.white);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 40));
		int width = g.getFontMetrics(g.getFont()).getStringBounds(player.getName(), g).getBounds().width;
		g.drawString(player.getName(), x + 2, y - (g.getFont().getSize() / 3));
		
		g.fillRect(x, y, 340, 430); //Render the background for the information box
		
		//Render the player's piece on the top right of the box
		int size = 75;
		GraphicUtils.renderMaintainAspect(g, player.getPiece().getIcon(), x + (340 - size), y - size, size, size);
		
		//Draw a red outline under their name if it is currently this player's turn
		if(board.getCurrentPlayer() == player) {
			g.setColor(Color.red);
			g.fillRect(x - 4, y - indent, width + 10, indent);
		}
		
		//If the player has resigned, draw a red line through their name
		if(player.hasResigned()) {
			g.setColor(Color.red);
			g.fillRect(x + 2, y - 30, width + 4, 10);
		}
		
		g.setColor(Color.black);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 30));
		
		//Render the amount of money the player has
		g.drawString("Cash:  " + StringUtils.POUND_SIGN + (player.getMoney() < 0 ? 0 : player.getMoney()), x + 10, y + 35);
		int newOffset = 0; //Used to move all information downwards in case extra information is needed
		if(player instanceof HumanPlayer && ((HumanPlayer) player).getMoneyOwed() > 0) {
			//Money is owed, so render this then add 35 to the offset.
			newOffset += 35;
			g.drawString("Owed: " + StringUtils.POUND_SIGN + ((HumanPlayer) player).getMoneyOwed(), x + 10, y + 70);
		}
		
		if(!player.hasResigned()) g.drawString("Properties:", x + 10, y + 70 + newOffset); //Draw the "Properties:" text
		
		return renderPropertyList(g, new ArrayList<>(player.getProperties()), x, y + newOffset);
	}
	
	/**
	 * @param properties The properties to render
	 * Renders the properties specified as tiles relative to the coordinates specified. It will not draw a background box
	 * @return The property which is being hovered over with the mouse, or null if none.
	 */
	public PurchasableProperty renderPropertyList(Graphics g, List<PurchasableProperty> properties, int x, int y) {
		Mouse mouse = getGame().getDisplay().getMouse();
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		
		//Get a list of where each of the property boxes should be rendered
		Map<Rectangle, PropertyInfo> propInfo = getPropertyInfoLocations(properties, x, y);
		
		PropertyInfo hovered = null;
		Rectangle hoveredRect = null;
		for(Rectangle rect : propInfo.keySet()) { //Loop through each rectangle (bounding box) in the list
			PropertyInfo prop = propInfo.get(rect);
			g.setColor(board.getColor(prop.getProperty()).getColor());
			GraphicUtils.fillRectangle(g, rect); //Fill in a rectangle using the property's colour
			if(prop.getProperty().isMortgaged()) { //If the property is mortgaged, draw a semi-transparent box over the top to darken
				g.setColor(new Color(0, 0, 0, 125));
				GraphicUtils.fillRectangle(g, rect);
			}
			if(mouseRect.intersects(rect)) { //If the mouse is over the bounding box, set the hovered variables
				hovered = prop;
				hoveredRect = rect;
			}
		}
		if(hovered != null) { //If there is a property being hovered over
			Color c = Color.white;
			if(board.getColor(hovered.getProperty()) == PropertyColors.YELLOW) c = Color.black;
			//Use white for the text colour unless the property colour is yellow (helps readability).
			
			//If its on the left hand side, draw the a text box with the property name to the right
			if(hovered.isLeft()) GraphicUtils.drawTextBox(g, hovered.getProperty().getName(), hoveredRect.x + 15, hoveredRect.y - 50, c, board.getColor(hovered.getProperty()).getColor());
			else {
				//Otherwise, draw the text box with the property name in it to the left. Need to know the width of the string to offset -width in the x direction
				int width = g.getFontMetrics(g.getFont()).getStringBounds(hovered.getProperty().getName(), g).getBounds().width;
 				GraphicUtils.drawTextBox(g, hovered.getProperty().getName(), hoveredRect.x + 15 - width, hoveredRect.y - 50, c, board.getColor(hovered.getProperty()).getColor());
			}
		}
		
		return hovered == null ? null : hovered.getProperty();
	}
	
	/**
	 * @return A map of Rectangle (which is the bounding box for each property information tile) to a PropertyInfo instance, which
	 * contains basic information about the property which is to be rendered.
	 */
	public Map<Rectangle, PropertyInfo> getPropertyInfoLocations(List<PurchasableProperty> properties, int x, int y) {
		Map<PropertyColors, Integer> colors = board.getPropertyNumbers(properties); //Gets the number of properties of each colour in the list
		Map<Rectangle, PropertyInfo> map = new HashMap<>(); //Temporary Map, which will be returned at the end of the method
		
		//Convert the keys in the map to a List. This is because a Map has no set order.
		//Then, sort the list so that the colours are in the order they appear on the board
		List<PropertyColors> listColors = new ArrayList<>(colors.keySet()); 
		listColors.sort((c1, c2) -> c1.compareTo(c2));
		
		final int tileSize = 30; //Temporary constant. Used to calculate where each location should be
		for(int k = 0; k < listColors.size(); k++) { //Loop through each colour in the sorted list
			PropertyColors color = listColors.get(k);
			int numb = colors.get(color);
			int column = k % 2;
			int row = k / 2;
			
			for(int j = 0; j < numb; j++) { //Loop from 0 to the number of properties of that colour
				Rectangle bounds;
				//Set the bounds depending on the column. 
				//Indent either 10 or 180 on the x, each tile is separated by 35 (tileSize + 5).
				//Indent 85 on the y, each row is separated by 40 (tileSize + 10) on the y axis. Each row has two colors.
				if(column == 0) bounds = new Rectangle(x + 10 + (j * (tileSize + 5)), y + 85 + (row * (tileSize + 10)), tileSize, tileSize); //On the left hand side
				else bounds = new Rectangle(x + 180 + (j * (tileSize + 5)), y + 85 + (row * (tileSize + 10)), tileSize, tileSize); //On the right hand side
				map.put(bounds, new PropertyInfo(getColoredProperty(color, j, properties), column == 0)); //getColoredProperty method to get the corresponding property for the index
			}
		}
		return map;
	}
	
	/**
	 * Opens the property view screen for the specified property
	 */
	public void showPropertyInfo(PurchasableProperty property) { 
		ScreenPropertyView screen;
		//If its currently the player's turn, the player is currently on the property, and nobody owns the property:
		if(board.getCurrentPlayer() == board.getHumanPlayer() && board.getProperties().get(board.getHumanPlayer().getBoardPosition()) == property && board.getOwner(property) == null) {
			//Set the screen to property view, but passing true so that it allows the player to buy the property (when they land on the property)
			screen = new ScreenPropertyView(getGame(), board, property, true);
		}
		else screen = new ScreenPropertyView(getGame(), board, property, false); //Set the screen to property view, but only displays information, doesn't allow to buy
		screen.init();
		Screen.setSelectedScreen(screen); //Enter into the screen
	}
	
	/**
	 * @param index Index to start at
	 * @return The property at the specified index and colour which the player owns. For example, if the player owns all 3 light blues, 
	 * specifying light blue as a color, and 1 for the index, will return the second light blue they own, in this case Los Angeles
	 */
	private PurchasableProperty getColoredProperty(PropertyColors color, int index, List<PurchasableProperty> properties) {
		int counter = 0;
		//Loop through each space on the board. Guarantees correct ordering
		for(int space : board.getProperties().keySet()) {
			Property prop = board.getProperties().get(space);
			if(properties.contains(prop)) { //If the player owns the property
				if(board.getColor(prop) == color) { //If the property's colour is the colour specified as a parameter
					if(counter == index) return (PurchasableProperty) prop; //If the counter equals the index specified as a parameter, return the property
					counter++; //Otherwise, increment the counter by 1
				}
			}
		}
		return null; //No property found with the search terms, return null
	}

	@Override
	public void update(double delta) {
		Mouse mouse = getGame().getDisplay().getMouse();
		
		if(movePoint != null && mousePoint != null) {
			//Used for moving the board. Sets the board offsets to the difference between the mouse positions and the position where the mouse was clicked
			int xDiff = (int) ((mouse.getX() - mousePoint.x) * 1920f / 1080f);
			int yDiff = (int) ((mouse.getY() - mousePoint.y) * 1920f / 1080f);
			boardXOffset = movePoint.x + xDiff;
			boardYOffset = movePoint.y + yDiff;
		}
		
		//Loop through each animation. Use an iterator to be able to remove elements in the loop and not encounter a ConcurrentModificationException
		Iterator<PlayerAnimation> it = animations.iterator();
		while(it.hasNext()) {
			PlayerAnimation animation = it.next();
			animation.update(delta);
			if(animation.hasFinished()) {
				it.remove(); //Remove the animation from the list
				//When the animation has finished, call the playerLand method, as they will have landed on the space the animation finishes at.
				playerLand(animation.getPlayer(), board.getProperties().get(animation.getBoardPosition()), animation.getSpacesToMove());
			}
		}
		float time = (float) (delta / 1000000000f); //Time in seconds, delta is in nanoseconds
		
		//Loop through each FadeText. Again, using an iterator to be able to remove elements in the loop and not encounter a ConcurrentModificationException
		Iterator<FadeText> fadeTextIt = fadeText.iterator(); 
		while(fadeTextIt.hasNext()) {
			FadeText text = fadeTextIt.next();
			text.setTimeIn(text.getTimeIn() + time); //Add on the time of the last update to the current time elapsed in the fade text
			if(text.getTimeIn() > text.getMaxTime()) {
				//If the time elapsed is over the maximum time, the text has finished, and remove it from the list
				text.setTimeIn(text.getMaxTime());
				fadeTextIt.remove();
			}
		}
		
		boolean rollEnabled, endTurnEnabled;
		
		if(board.getCurrentPlayer() == board.getHumanPlayer()) {
			rollEnabled = !rolled; //Can click the roll button is the player hasn't rolled yet
			endTurnEnabled = rolled && animations.isEmpty(); //Can end their turn if they've rolled and there is not currently any animations
			HumanPlayer human = board.getHumanPlayer();
			buttonEndTurn.setText("End Turn");
			buttonRoll.setText("Roll");
			if(human.getMoneyOwed() > 0) { //Owes money
				rollEnabled = false; //Not able to roll
				endTurnEnabled = true; //The "end turn button" is enabled, but will change text
				if(human.getMoney() >= human.getMoneyOwed()) buttonEndTurn.setText("Pay back"); //Has enough money, so the button will allow the player to pay back
				else buttonEndTurn.setText("Give up"); //Not enough money, button will allow the player to give up
			} else if(board.getCurrentPlayer().isInJail()) {
				if(board.getCurrentPlayer().getJailAttempts() >= 3) buttonRoll.setText("Pay"); //Roll button changes to force the user to pay back if used up jail attempts
				else buttonRoll.setText("Answer"); //Roll button changes to allow the user to answer a question while in jail
			} 
		} else {
			//Not currently player's turn, disable the buttons
			rollEnabled = false;
			endTurnEnabled = false;
		}
		
		Color enabledColor = new Color(180, 180, 180);
		Color disabledColor = new Color(80, 80, 80);
		ButtonUtils.setEnabled(buttonRoll, enabledColor, disabledColor, rollEnabled); //Enable the button is the rollEnabled variable is true
		ButtonUtils.setEnabled(buttonEndTurn, enabledColor, disabledColor, endTurnEnabled); //Enable the button is the endTurnEnabled variable is true
		
		if(board.getCurrentPlayer() != board.getHumanPlayer()) { //Disable all the trade buttons if it isn't the player's turn
			ButtonUtils.setEnabled(tradep1, enabledColor, disabledColor, false);
			ButtonUtils.setEnabled(tradep2, enabledColor, disabledColor, false);
			ButtonUtils.setEnabled(tradep3, enabledColor, disabledColor, false);
		} else { //Enable all the trade buttons if it is the player's turn
			ButtonUtils.setEnabled(tradep1, enabledColor, disabledColor, true);
			ButtonUtils.setEnabled(tradep2, enabledColor, disabledColor, true);
			ButtonUtils.setEnabled(tradep3, enabledColor, disabledColor, true);
		}
		
		//Loop through all the Runnables in the delayedTask keys.
		Iterator<Runnable> runIt = delayedTasks.keySet().iterator();
		float update = (float) (delta / 1000000000f);
		while(runIt.hasNext()) {
			Runnable task = runIt.next();
			float dTime = delayedTasks.get(task);
			dTime -= update;
			//Subtract the previous update time from the time left before the task executes
			if(dTime <= 0) { //If the time left is left than or equal to 0, the task should execute
				//Run the task and then remove it from the map
				runIt.remove();
				task.run();
				continue;
			}
			//Re-enter the task into the map, updating its time
			delayedTasks.put(task, dTime);
		}
	}
	
	/**
	 * Called when a player lands on a space.
	 * @param diceRoll The number of spaces the player rolled on the dice to get here
	 * @param property The property the player has landed on
	 */
	private void playerLand(Player player, Property property, int diceRoll) {
		property.onLand(player, this, board);
		if(player instanceof HumanPlayer) {
			//If its the human player, call the onLand method for the player. Ignore the return - the turn should never end automatically
			player.onLand(this, getBoard(), property, diceRoll);
		} else {
			//Delay the following code by 1 second:
			runDelayedTask(() -> {
				boolean end = player.onLand(this, getBoard(), property, diceRoll);
				//Call the onLand method for the player. In this case, a return of true means the turn should automatically
				if(end) {
					//Ending the turn automatically, but running on a delay of 2 seconds to slow down the pace of the game a bit.
					runDelayedTask(() -> {
						addInfoText(player.getName() + " has ended their turn.");
						endTurn();
					}, 2f);
				}
			}, 1f);
		}
	}
	
	/**
	 * When a player passes a property but does not land on it
	 */
	public void playerPass(Player player) {
		Property property = board.getProperties().get(player.getBoardPosition());
		property.onPass(player, this, getBoard());
		player.onPass(this, getBoard(), property);
	}
	
	@Override
	public void mouseScrolled(int scroll) {
		float beforeScale = scale; //Stores the scale before changing it
		scale += (scroll * -0.15f); //Add on the amount scrolled on the mouse *-0.15
		
		boolean scrolled = true;
		if(scale < 1) {
			scale = 1; //Caps the scroll at 1 (default size)
			if(beforeScale == 1) scrolled = false; //If it was 1 before, so no change is made, no scrolling has occurred
		}
		if(scale > 2.5f) {
			scale = 2.5f; //Caps the scroll at 2.5x zoom
			if(beforeScale == 2.5f) scrolled = false; //If it was 2.5 before, so no change is made, no scrolling has occurred
		}
		if(!scrolled) return; //Do not reset transformation if no scrolling has taken place
		
		//Always zoom in to the centre of the board, so calculate where to set the translation to.
		int movementX = (int) ((1080 * scale) - (-boardXOffset + 1080));
		int movementY = (int) ((1080 * scale) - (-boardYOffset + 1080));
		boardXOffset -= movementX;
		boardYOffset -= movementY;
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		
		//If the mouse is inside the board render area
		if(mouse.getX() >= 420 && mouse.getX() <= 1920 - 420 && mouse.getY() >= 0 && mouse.getY() <= 1080) {
			Point point = transformToBoard(mouse.getX(), mouse.getY()); //Get the board coordinates from the mouse coordinates
			
			Rectangle mouseRect = new Rectangle(point.x - 1, point.y - 1, 2, 2);
			Rectangle boardPos = new Rectangle(0, 0, 1920, 1920);
			
			if(mouseRect.intersects(boardPos)) { //If the board is clicked
				movePoint = new Point(boardXOffset, boardYOffset); //Store the current board x and y offsets
				mousePoint = new Point(mouse.getX(), mouse.getY()); //Store the current mouse position
				return;
			}
		}
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		for(Player p : board.getPlayers()) { //Loop through each of the players and get the position of the information box
			int x = playerInfoRenderPoints.get(p).x; 
			int y = playerInfoRenderPoints.get(p).y;
			
			//Adds the offset in case of owing money, see the renderPlayerInfo method for info
			if(p instanceof HumanPlayer && ((HumanPlayer) p).getMoneyOwed() > 0) y += 35; 
			
			Map<Rectangle, PropertyInfo> propInfo = getPropertyInfoLocations(new ArrayList<>(p.getProperties()), x, y);
			
			//Loop through all the property tiles shown on the player info box, and check if one of them was clicked
			for(Rectangle rect : propInfo.keySet()) {
				PropertyInfo prop = propInfo.get(rect);
				if(mouseRect.intersects(rect)) { //This property has been clicked, show information for it
					showPropertyInfo(prop.getProperty());
				}
			}
		}
		//Call the button clicked methods for each button
		buttonRoll.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		buttonEndTurn.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		
		if(board.getCurrentPlayer() == board.getHumanPlayer()) {
			if(!board.getComputerPlayer1().hasResigned()) tradep1.mouseClick(mouseButton, mouse.getX(), mouse.getY());
			if(!board.getComputerPlayer2().hasResigned()) tradep2.mouseClick(mouseButton, mouse.getX(), mouse.getY());
			if(!board.getComputerPlayer3().hasResigned()) tradep3.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		}
	}
	
	@Override
	public void mouseReleased(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		
		if(movePoint != null && mousePoint != null) { //If the board is currently being dragged
			int xDiff = Math.abs((int) ((mouse.getX() - mousePoint.x) * scale));
			int yDiff = Math.abs((int) ((mouse.getY() - mousePoint.y) * scale));
			
			movePoint = null; //Set the variables to null so the board is not being dragged any more
			mousePoint = null;
			
			if(xDiff <= 2 && yDiff <= 2) { //If the mouse has been moved less than 2 pixels in any direction
				Point p = transformToBoard(mouse.getX(), mouse.getY());
				board.mouseClick(mouseButton, p.x, p.y); //Call the click on the board.
			}
		}
	}
	
	/**
	 * Writes game data to the config
	 * @param config The config to write data to
	 */
	public void save(ConfigSection config) {
		//Save the board scale, offset in x and y, and whether the player has rolled
		config.set("scale", scale);
		config.set("boardXOffset", boardXOffset);
		config.set("boardYOffset", boardYOffset);
		config.set("rolled", rolled);
		
		if(!animations.isEmpty()) {
			//If there is currently a piece animation occurring
			List<ConfigSection> animationSections = new ArrayList<>(); //Each ConfigSection stores information for an animation
			for(PlayerAnimation animation : animations) {
				ConfigSection animationSection = new ConfigSection();
				animation.save(animationSection); //Write the animation to the config section
				animationSections.add(animationSection); //Add the config to the list
			}
			config.set("animations", animationSections); //Add the list to the main config to save
		}
		
		//Create a config section for each player on the board, and add it to the main config section
		ConfigSection playerSection = new ConfigSection();
		board.getHumanPlayer().save(playerSection, getBoard());
		
		ConfigSection playerSection1 = new ConfigSection();
		board.getComputerPlayer1().save(playerSection1, getBoard());
		
		ConfigSection playerSection2 = new ConfigSection();
		board.getComputerPlayer2().save(playerSection2, getBoard());
		
		ConfigSection playerSection3 = new ConfigSection();
		board.getComputerPlayer3().save(playerSection3, getBoard());
		
		config.set("player", playerSection);
		config.set("player1", playerSection1);
		config.set("player2", playerSection2);
		config.set("player3", playerSection3);
		
		//Create a config section for the board and write board data.
		ConfigSection boardSection = new ConfigSection();
		board.save(boardSection);
		config.set("board", boardSection);
	}
	
	/**
	 * Loads game data from the config for the current game.
	 */
	public void load(ConfigSection config) {
		animations.clear();
		
		//Load in scale, board offsets and whether the player has rolled from the config
		scale = config.getFloat("scale");
		boardXOffset = config.getInteger("boardXOffset");
		boardYOffset = config.getInteger("boardYOffset");
		rolled = config.getBoolean("rolled");
		
		if(config.contains("animations")) { //There were animations set in the config
			List<ConfigSection> animationSections = config.getSectionList("animations");
			for(ConfigSection animationSection : animationSections) { //Loop through each ConfigSection in the list
				PlayerAnimation animation = new PlayerAnimation(this, null, 10);
				animation.load(animationSection); //Load the animation from the config section
				animations.add(animation); //Add the animation to the list
			}
		}
		//Load player data from the config for each player
		board.getHumanPlayer().load(config.getSection("player"), getBoard());
		board.getComputerPlayer1().load(config.getSection("player1"), getBoard());
		board.getComputerPlayer2().load(config.getSection("player2"), getBoard());
		board.getComputerPlayer3().load(config.getSection("player3"), getBoard());
		
		board.load(config.getSection("board")); //Load board data from the config
		
		//Re-shuffle chance and community chests, as not storing the ordering of these
		Cards.shuffleChance(); 
		Cards.shuffleCommunityChest();
	}
	
	/**
	 * Converts a Screen X + Y coordinate into a board coordinate. Takes into account the board offset and 
	 */
	private Point transformToBoard(float x, float y) {
		x -= 420; //Subtract 420 from the x, as the board starts to be rendered at 420px on the x
		x = (x * (1920f / 1080)) - boardXOffset; //Scale the x as the board is 1920x1920 rendered in a 1080x1080 space, then subtract the board offset
		y = (y * (1920f / 1080)) - boardYOffset; //Scale the y for the same reason, then subtract the board offset
		
		//Scale the x and y based on the board scaling
		x = x / scale;
		y = y / scale;
		
		return new Point((int) x, (int) y); //return the coordinates
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		if(key == KeyEvent.VK_R) { //'R' key resets scaling of the board
			scale = 1f;
			boardXOffset = 0;
			boardYOffset = 0;
		}

		if(key == KeyEvent.VK_ESCAPE) {
			Screens.GAME_OPTIONS.init(); //Reload the game options screen
			Screen.setSelectedScreen(Screens.GAME_OPTIONS); //Enter into the game options screen
		}
		if(key == KeyEvent.VK_F2) { //Takes a screenshot
			Screenshotter.takeScreenshot();
		}
 	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		getGame().setCurrentGame(this); //This is now the current game
		getGame().getDisplay().setBackgroundColor(new Color(10, 10, 10)); //Set the background colour to a dark gray
		getGame().getDisplay().resize(1920, 1080); //Ensure the correct size
		if(!Sounds.BACKGROUND_MUSIC.isPlaying()) { //Start the music if its not playing
			Sounds.BACKGROUND_MUSIC.play();
		}
	}
	
	@Override
	public void screenExited(Screen nextScreen) {
		if(nextScreen instanceof ScreenGameOptions) {
			//If the next screen is the options screen, pause the music
			Sounds.BACKGROUND_MUSIC.pause();
		}
	}
	
	public void addInfoText(String text, float time) {
		fadeText.add(new FadeText(text, time)); //Add the info text and print it out on the console
		System.out.println(text);
	}
	
	/**
	 * Adds the fade text to the screen with a time of 7 seconds
	 */
	public void addInfoText(String text) {
		addInfoText(text, 7);
	}
	
	public Board getBoard() {
		return board;
	}
	
	public List<PlayerAnimation> getAnimations() {
		return animations;
	}
	
	public void runDelayedTask(Runnable runnable, float time) {
		delayedTasks.put(runnable, time);
	}
	
	public void setSaveName(String name) {
		this.saveName = name;
	}
	
	public String getSaveName() {
		return saveName;
	}

	public List<FadeText> getFadeText() {
		return fadeText;
	}
}
