package edu.monopoly.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.util.GraphicUtils;

public class ScreenYesNo extends Screen {

	private String message; //Message to display to the user
	private Runnable confirm, cancel; //Methods called when either confirm or cancel is clicked (yes or no)
	private Screen previousScreen; //Screen to go back to after a button is clicked
	private OptionButton<Object> buttonYes, buttonNo; //Buttons
	
	public ScreenYesNo(Monopoly game, String message, Runnable confirm, Runnable cancel) {
		super(game);
		this.message = message;
		this.confirm = confirm;
		this.cancel = cancel;
	}
	
	@Override
	public void init() {
		Color c = new Color(180, 180, 180);
		
		//Button to confirm
		buttonYes = new OptionButton<>(getGame(), 975, 700, 280, 75, c, c, Color.black, "Yes", new Object[] {});
		buttonYes.setOnClick(() -> {
			//When clicked, set the screen to the previous screen and run the confirm method
			Screen.setSelectedScreen(previousScreen);
			if(confirm != null) confirm.run();
		});
		
		//Button to cancel
		buttonNo = new OptionButton<>(getGame(), 665, 700, 280, 75, c, c, Color.black, "No", new Object[] {});
		buttonNo.setOnClick(() -> {
			//When clicked, set the screen to the previous screen and run the cancel method
			Screen.setSelectedScreen(previousScreen);
			if(cancel != null) cancel.run();
		});
	}

	@Override
	public void render(Graphics g) {
		//Draw the previous screen as a background
		if(previousScreen != null) previousScreen.render(g);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 30));
		
		int width = 650;
		int height = 550;
		
		//Draw a semi-transparent black rectangle over the background (darkens it)
		g.setColor(new Color(0, 0, 0, 120));
		g.fillRect(0, 0, 1920, 1080);
		
		//Draw the outline for the box
		g.setColor(Color.black);
		g.fillRect((1920 /2) - (width / 2) - 8, (1080 / 2) - (height / 2) - 8, width + 16, height + 16);
		
		//Draw the background for the box
		g.setColor(Color.white);
		g.fillRect((1920 /2) - (width / 2), (1080 / 2) - (height / 2), width, height);
		
		g.setColor(Color.black);

		//Convert the text into a string array, split at blank lines
		//Render each line in the center of the screen going down the screen
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 36));
		int lineDiff = g.getFont().getSize();
		String[] parts = message.split("\n");
		for(int i = 0; i < parts.length; i++) {
			String part = parts[i];
			GraphicUtils.drawCenteredString(g, part, (int) (330 + (i * lineDiff * 1.2f)));
		}
		
		//Render the buttons
		buttonYes.render(g);
		buttonNo.render(g);
	}

	@Override
	public void update(double delta) {
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		//Forward the mouse event onto the buttons
		buttonYes.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		buttonNo.mouseClick(mouseButton, mouse.getX(), mouse.getY());
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		previousScreen = lastScreen;
	}

}
