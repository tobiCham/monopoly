package edu.monopoly.screen;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import edu.monopoly.Monopoly;
import edu.monopoly.screen.entername.ScreenEnterName;
import edu.monopoly.screen.options.ScreenGameOptions;
import edu.monopoly.screen.selectdifficulty.ScreenSelectDifficulty;
import edu.monopoly.screen.title.ScreenTitle;

public class Screens {

	private static List<Screen> screens = new ArrayList<>();
	
	public static ScreenLoading LOADING = null;
	public static final ScreenTitle TITLE = loadScreen(ScreenTitle.class);
	public static final ScreenSelectDifficulty SELECT_DIFFICULTY = loadScreen(ScreenSelectDifficulty.class);
	public static final ScreenEnterName ENTER_NAME = loadScreen(ScreenEnterName.class);
	public static final ScreenGameOptions GAME_OPTIONS = loadScreen(ScreenGameOptions.class);
	public static final ScreenLoadGame LOAD_GAME = loadScreen(ScreenLoadGame.class);
	
	//Generic type. Simply returns the screen instance after adding it to the list
	//Use generics otherwise would only return Screen
	private static <T extends Screen> T loadScreen(T screen) {
		screens.add(screen);
		return screen;
	}
	
	//Tries to load a screen by using reflection to call its constructor. This method will only work
	//If there is a constructor in the screen class which has 1 argument of Monopoly
	private static <T extends Screen> T loadScreen(Class<T> screen) {
		try {
			Constructor<T> constructor = screen.getDeclaredConstructor(Monopoly.class);
			return loadScreen(constructor.newInstance(Monopoly.getGameInstance()));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//Empty method, however due to how Java class loading works, calling it will cause the static fields
	//at the top of the class to be initialised
	public static void init() {}
	
	/**
	 * @return A list of all the Screens defined in the class
	 */
	public static List<Screen> getScreens() {
		return screens;
	}
}