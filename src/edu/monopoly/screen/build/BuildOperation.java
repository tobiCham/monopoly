package edu.monopoly.screen.build;

public enum BuildOperation {
	NOTHING, MODIFY, BUILD, DEMOLISH
}