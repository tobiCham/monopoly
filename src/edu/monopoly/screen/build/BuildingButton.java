package edu.monopoly.screen.build;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.util.GraphicUtils;

public class BuildingButton {

	private int x, y, width, height; //Coordinates of the button
	private Image hovered, unselected, clicked, current;
	private boolean isHovered;
	
	public BuildingButton(int x, int y, int width, int height, Image hovered, Image unselected, Image clicked) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.hovered = hovered;
		this.unselected = unselected;
		this.clicked = clicked;
		current = unselected;
	}
	
	public void render(Graphics g) {
		//If the button isn't currently selected, use either the hovered image if it is currently being
		//hovered, or the unselected image if it isn't
		if(!isSelected()) {
			current = isHovered ? hovered : unselected;
		}
		//Render the image for the button
		GraphicUtils.renderMaintainAspect(g, current, x, y, width, height);
	}
	
	public void setSelected(boolean selected) {
		if(selected) current = clicked;
		else current = unselected;
	}
	
	public boolean isSelected() {
		return current == clicked;
	}
	
	/**
	 * @return Whether the button is currently being hovered over with the mouse
	 */
	public boolean isHovered() {
		Mouse mouse = Monopoly.getGameInstance().getDisplay().getMouse();
		Rectangle bounds = new Rectangle(this.x, this.y, this.width, this.height);
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		return bounds.intersects(mouseRect);
	}
	
	/**
	 * @return 1 if the button is currently selected, 0 if its not selected, or -1 if the mouse didn't even click the button
	 */
	public int mouseClick(int button) {
		boolean clicked = isHovered();
		if(clicked) {
			if(current == this.clicked) return 1;
			else return 0;
		}
		return -1;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Image getHoveredImage() {
		return hovered;
	}

	public void setHoveredImage(Image hovered) {
		this.hovered = hovered;
	}

	public Image getUnselectedImage() {
		return unselected;
	}

	public void setUnselectedImage(Image unselected) {
		this.unselected = unselected;
	}

	public Image getClickedImage() {
		return clicked;
	}

	public void setClickedImage(Image clicked) {
		this.clicked = clicked;
	}

	public void setHovered(boolean isHovered) {
		this.isHovered = isHovered;
	}
}
