package edu.monopoly.screen.build;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.board.Board;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.player.Player;
import edu.monopoly.property.ColoredProperty;
import edu.monopoly.property.PropertyColors;
import edu.monopoly.property.PurchasableProperty;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.propertyview.ScreenPropertyView;
import edu.monopoly.screen.question.ScreenQuestion;
import edu.monopoly.util.ButtonUtils;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.ImageLoader;
import edu.monopoly.util.StringUtils;

public class ScreenBuild extends Screen {

	private Screen previousScreen; //Screen to return to after building or cancelling
	private Board board;
	private PropertyColors color; //Colour of the property to allow the player to build on
	private PurchasableProperty property; //Currently selected property to show information about
	
	private Point movePoint, mousePoint; //Used to allow the property information box to be moved
	private int selectedRow = 0; //Index of the row currently selected.
	private OptionButton<Object> buttonBuild, buttonCancel;
	
	//Offset coordinates for where the render information about the property variable
	//Is static so it remains in the same place when the screen changes
	private static int xOffset = 1030, yOffset = 240; 
	
	//Images used as button icons. Static so they are only loaded once
	private static Image apartment = ImageLoader.loadImage("apartment.png");
	private static Image apartment_hovered = ImageLoader.loadImage("apartment_hovered.png");
	private static Image apartment_unselected = ImageLoader.loadImage("apartment_unselected.png");

	private static Image hotel = ImageLoader.loadImage("hotel.png");
	private static Image hotel_hovered = ImageLoader.loadImage("hotel_hovered.png");
	private static Image hotel_unselected = ImageLoader.loadImage("hotel_unselected.png");
	
	//Maps the row index to a List of the buttons on that row. The key will only be from 0 to 2
	private Map<Integer, List<BuildingButton>> buttons = new HashMap<>();
	
	public ScreenBuild(Monopoly game, PropertyColors color, Board board) {
		super(game);
		this.board = board;
		this.color = color;
	}

	@Override
	public void init() {
		buttons.clear();
		
		//Loops from 0 to the number of properties of the colour specified.
		for(int i = 0; i < getProperties().size(); i++) {
			List<BuildingButton> buttons = new ArrayList<>(); //List of buttons to add to the buttons map
			for(int j = 0; j < 5; j++) { //Loops from 0 to 5 (maximum number of buildings which can be built)
				if(j == 4) {
					//If j is 4, then its a hotel. Use the hotel images and add a new button to the list
					buttons.add(new BuildingButton(375 + (j * 90), 220 + (i * 100), 75, 75, hotel_hovered, hotel_unselected, hotel));
				} else {
					//Otherwise its an apartment, so use the apartment images and add a new button to the list
					buttons.add(new BuildingButton(375 + (j * 90), 220 + (i * 100), 75, 75, apartment_hovered, apartment_unselected, apartment));
				}
			}
			//Add the list of buttons to the buttons map
			this.buttons.put(i, buttons);
		}
		
		//Sets the property to property at the index of the selected row on the board for the colour specified
		property = getProperties().get(selectedRow);
		
		//Create the Build and Cancel buttons
		buttonBuild = new OptionButton<>(getGame(), 145, 730, 550, 75, Color.red, Color.red, Color.white, "Build", new Object[] {});
		buttonBuild.setOnClick(this::buildClicked); //When build is clicked, call the buildClicked method
		
		buttonCancel = new OptionButton<>(getGame(), 145, 875, 550, 75, Color.red, Color.red, Color.white, "Cancel", new Object[] {});
		buttonCancel.setOnClick(() -> Screen.setSelectedScreen(previousScreen)); //When cancel is clicked return to the previous screen
		
		//Loads the number of buildings currently built on the properties
		loadCurrentBuildings();
	}
	
	/**
	 * Called when the build button is clicked.
	 */
	private void buildClicked() {
		BuildOperation operation = getOperation(); //Gets the operation which will occur
		if(operation == BuildOperation.NOTHING) return; //Return if nothing will be built
		
		int money = getCost(); //Calculates the cost of the operation
		
		//The text of the question to display. Will be appended to in the code below to create a full question
		//Will change depending on the operation that is occurring and whether the user gains or loses money from it
		//Will always ask the user how much money they have after the operation
		String text = "You are ";
		
		//Converts the operation name to a verb ("Build" -> "building")
		String operationName = operation.toString().toLowerCase() + "ing";
		text += operationName;
		
		int modifying = getModifying(); //Gets the number of buildings which are being modified
		String format = modifying == 1 ? "building" : "buildings";
		
		text += " " + modifying + " " + format + "\n";
		
		text += "You currently have " + StringUtils.POUND_SIGN + board.getHumanPlayer().getMoney() + " and the total ";
		if(money > 0) text += "cost is " + StringUtils.POUND_SIGN + money;
		else text += "amount you gain is " + StringUtils.POUND_SIGN + Math.abs(money);
		text += "\n\nHow much money do you have after " + operationName + "?";
		
		//Create the question screen. The question is the question text which was created above. The answer is their current money
		//minus the cost of the operation. If the money is negative (therefore the player will gain money from demolishing), 
		//then the two minuses cancel to plus resulting in the player gaining money
		ScreenQuestion screen = new ScreenQuestion(getGame(), text, board.getHumanPlayer().getMoney() - money, () -> {
			//When the question is answered correctly, update the player's money to add on the cost of the operation
			Player human = board.getHumanPlayer();
			human.setMoney(human.getMoney() - money);
			
			//Gets the buildings which are on the screen and updates the buildings owned by the player
			Map<Integer, Integer> buildingNumbers = getBuildingNumbers();
			List<PurchasableProperty> props = getProperties();
			for(int in : buildingNumbers.keySet()) {
				int buildings = buildingNumbers.get(in);
				human.setBuildings((ColoredProperty) props.get(in), buildings);
			}
			//Returns to the previous screen
			Screen.setSelectedScreen(previousScreen);
		});
		screen.init(); //Show the question to the user
		Screen.setSelectedScreen(screen);
	}
	
	/**
	 * Loads the buildings which the player currently owns into the screen. Any changes the user was making will be discarded
	 */
	private void loadCurrentBuildings() {
		List<PurchasableProperty> properties = getProperties();
		
		//Iterates through each property of the colour currently being shown
		for(int i = 0; i < properties.size(); i++) {
			PurchasableProperty prop = properties.get(i);
			
			//Gets the number of buildings the player owns for the property (may be 0)
			int numb = board.getHumanPlayer().getBuildings((ColoredProperty) prop);
			
			//Sets all the buttons for the corresponding property to unselected, then selects 
			//the however many buildings were on that property
			for(int j = 0; j < 5; j++) buttons.get(i).get(j).setSelected(false);
			for(int j = 0; j < numb; j++) buttons.get(i).get(j).setSelected(true);
		}
		//Validate the input so that the user can't click build as no changes are being made
		validateBuilds();
	}

	@Override
	public void render(Graphics g) {
		//Renders the board on the right hand side of the window. Scales so that it fits in the GUI properly
		float newScale = 1080f / 1920f;
		float boardXOffset = (1080f / 1920f) + 840;
		float boardYOffset = newScale;
		
		//Translate and scale to fit in the designated area
		((Graphics2D) g).translate(boardXOffset, boardYOffset);
		((Graphics2D) g).scale(newScale, newScale);
		board.render(g); //Render the board
		//Undo the transformation and scaling
		((Graphics2D) g).scale(1f/newScale, 1f/newScale);
		((Graphics2D) g).translate(-boardXOffset, -boardYOffset);
		
		g.setColor(Color.black);
		g.fillRect(0, 0, 840, 1080);
		
		int startX = xOffset;
		int startY = yOffset;
		
		//Fills in a black background on the left hand side of the screen
		g.setColor(Color.black);
		g.fillRect(0, 0, 840, 200);
		g.fillRect(0, 1080 - 200, 840, 200);
		
		//Renders the screen title
		g.setColor(Color.white);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 50));
		GraphicUtils.drawCenteredString(g, "Build Or Demolish Houses", 170, 420);
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 40));
		
		List<PurchasableProperty> props = getProperties();
		for(int i = 0; i < props.size(); i++) { //Loops through all the properties of the current colour
			PurchasableProperty p = props.get(i);
			
			//Bounds of the rectangle strip to render. The rectangle will contain the property name and the building buttons
			Rectangle rowRect = new Rectangle(20, 220 + (i * 100), 800, 75);
			
			//If the current row is the selected row:
			if(i == selectedRow) { 
				//Get the RGB colour of the property colour
				Color c = color.getColor();
				
				//Convert the Rgb colour to HSB, and increase both the saturation and brightness so that the box is lighter
				//then use that instead
				float[] hsb = new float[3];
				Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
				hsb[1] = 1;
				hsb[2] *= 1.2f;
				if(hsb[2] > 1) hsb[2] = 1;
				g.setColor(new Color(Color.HSBtoRGB(hsb[0], hsb[1], hsb[2])));
			} else {
				//Otherwise, it is not the selected row.
				Mouse mouse = getGame().getDisplay().getMouse();
				Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
				
				//If the mouse is hovered over the row:
				if(mouseRect.intersects(rowRect)) {
					Color c = color.getColor();
					
					//Lighten the RGB colour to use, but not quite as much as if it were selected.
					float[] hsb = new float[3];
					Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
					hsb[1] = 0.95f;
					hsb[2] *= 1.2f;
					if(hsb[2] > 1) hsb[2] = 1;
					g.setColor(new Color(Color.HSBtoRGB(hsb[0], hsb[1], hsb[2])));
				} else g.setColor(color.getColor()); //Otherwise, use the normal colour
			}
			
			//Render the rectangle, then draw the name of the property
			GraphicUtils.fillRectangle(g, rowRect);
			g.setColor(Color.black);
			g.drawString(p.getName(), 30, 275 + (i * 100));
		}
		
		//Render all the BuildingButtons on the screen
		for(int i : buttons.keySet()) {
			for(BuildingButton button : buttons.get(i)) {
				button.render(g);
			}
		}
		
		//Render the build and cancel buttons
		buttonBuild.render(g);
		buttonCancel.render(g);

		//Render the player's current money
		g.setColor(Color.white);
		g.drawString("Money: " + StringUtils.POUND_SIGN + board.getHumanPlayer().getMoney(), 30, 580);
		
		int money = getCost(); //The total cost of building/demolishing
		
		//Either show the cost of the buildings, or the money gained from demolishing
		if(money >= 0) {
			g.drawString("Building Cost: " + StringUtils.POUND_SIGN + money, 30, 650);
		} else {
			g.drawString("Money Gained: " + StringUtils.POUND_SIGN + Math.abs(money), 30, 650);
		}
		
		//Draw a black border around the property card, then draw the property card itself
		g.setColor(Color.black);
		g.fillRect(startX - 6, startY - 6, 712, 612);
		ScreenPropertyView.renderPropertyCard(g, startX, startY, property, board);
	}

	@Override
	public void update(double delta) {
		Mouse mouse = getGame().getDisplay().getMouse();
		
		//Controls the dragging of the property information card. Similar systems in the trade screen, see ScreenTrade
		//for more information on this
		if(movePoint != null && mousePoint != null) {
			int xDiff = (mouse.getX() - mousePoint.x);
			int yDiff = (mouse.getY() - mousePoint.y);
			xOffset = movePoint.x + xDiff;
			yOffset = movePoint.y + yDiff;
			
			if(xOffset < 846) xOffset = 846;
			if(xOffset + 706 > 1920) xOffset = 1920 - 706;
			
			if(yOffset < 6) yOffset = 6;
			if(yOffset + 606 > 1080) yOffset = 1080 - 606;
		}
		
		//Loops through all the buttons on the screen
		for(int i : buttons.keySet()) {
			List<BuildingButton> butts = buttons.get(i);
			for(int j = 0; j < butts.size(); j++) {
				BuildingButton button = butts.get(j);
				boolean hovered = button.isHovered();
				
				//If this button is hovered, set all the buttons up to and including this button on the property row to hovered
				if(hovered) {
					for(int k = 0; k < butts.size(); k++) buttons.get(i).get(k).setHovered(false);
					for(int k = 0; k <= j; k++) butts.get(k).setHovered(true);
					break;
				} else for(BuildingButton b : butts) b.setHovered(false); //Otherwise, set all the buttons to not hovered
			}
		}
	}
	
	/**
	 * Validates the changes the user is attempting to make in order to ensure it is valid. Will disable the button
	 * if the user isn't making any changes or doesn't have enough money for example
	 */
	private void validateBuilds() {
		List<PurchasableProperty> props = getProperties();
		property = props.get(selectedRow);
		
		Color disableColor = new Color(140, 140, 140); //The colour the button should turn when it is disabled
		
		//Resets the button back to enable. Will be disable later if needed
		ButtonUtils.setEnabled(buttonBuild, Color.red, disableColor, null, true); 
		buttonBuild.setText("Build");
		
		Map<Integer, Integer> buildingNumbers = getBuildingNumbers();
		
		int maxDiff = 0;
		//Compares the number of buildings on each of the properties to every other. If the difference in numbers is
		//more than the current maxDiff, update it
		for(int i : buildingNumbers.keySet()) {
			for(int j : buildingNumbers.keySet()) {
				int numb1 = buildingNumbers.get(i);
				int numb2 = buildingNumbers.get(j);
				if(Math.abs(numb2 - numb1) > maxDiff) maxDiff = Math.abs(numb2 - numb1);
			}
		}
		
		//If the maximum difference is more than 1, it means that there was a difference in numbers of buildings
		//between two properties of more than 1 which results in the button being disabled
		if(maxDiff > 1) {
			ButtonUtils.setEnabled(buttonBuild, Color.red, disableColor, "Must have a similar number of houses!", false);
			return;
		}
		
		BuildOperation operation = getOperation(); //Gets the build operation
		
		if(operation == BuildOperation.NOTHING) { //If nothing is built or demolished, disable the button as nothing is being changed
			ButtonUtils.setEnabled(buttonBuild, Color.red, disableColor, "Nothing to Build!", false);
			return;
		}
		//Set the text of the button to the formatted version of the operation name with a capitalised first letter
		buttonBuild.setText(StringUtils.format(operation.toString()));
		
		//If the player is building or modifying (not demolishing), then the operation will cost a certain amount of money
		if(operation == BuildOperation.BUILD || operation == BuildOperation.MODIFY) {
			int money = getCost(); //Get the cost of the operation
			
			//If the player doesn't have enough money to build, disable the button
			if(money > board.getHumanPlayer().getMoney()) {
				ButtonUtils.setEnabled(buttonBuild, Color.red, disableColor, "Not enough money!", false);
				return;
			}
		}
	}
	
	/**
	 * @return A map mapping the index of the row to the number of buildings on that row
	 */
	private Map<Integer, Integer> getBuildingNumbers() {
		Map<Integer, Integer> buildingNumbers = new HashMap<>(); //Map to return
		for(int i : buttons.keySet()) { //Loop through all the rows of properties (buttons map)
			
			buildingNumbers.put(i, 0); //Initially set the number of buildings on the row to 0
			
			//Loops through all the buttons on the current row. If the button at that index is selected,
			//update the buildingNumbers map putting the number of buildings to j + 1 (to remove the zero index)
			for(int j = 0; j < buttons.get(i).size(); j++) {
				if(buttons.get(i).get(j).isSelected()) buildingNumbers.put(i, j + 1);
			}
		}
		return buildingNumbers;
	}
	
	/**
	 * Gets the numbers of properties which the player currently has built, regardless of the changes the player
	 * is making on this screen
	 * @return A map mapping the index of the row to the number of buildings on that row
	 */
	private Map<Integer, Integer> getCurrentNumbers() {
		Map<Integer, Integer> buildingNumbers = new HashMap<>(); //Map to return
		
		List<PurchasableProperty> props = getProperties();
		//Iterate from 0 to the number of properties of the current colour (ensures it is the correct row)
		for(int i = 0; i < props.size(); i++) {
			PurchasableProperty prop = props.get(i);
			
			//Get the number of buildings which are on the property and add it to the map
			int buildings = 0;
			if(prop instanceof ColoredProperty) buildings = board.getHumanPlayer().getBuildings((ColoredProperty) prop);
			buildingNumbers.put(i, buildings);
		}
		
		return buildingNumbers;
	}
	
	/**
	 * @return The total cost for the current operation. 0 will mean no changes, a negative number means the player
	 * gains money, usually from demolishing. A positive number means the player will lose money, usually from
	 */
	private int getCost() {
		int total = 0;
		List<PurchasableProperty> props = getProperties();
		
		//The cost per building. Can get the 0th element property because all properties of the colour will have the same building cost
		int buildingCost = ((ColoredProperty) props.get(0)).getBuildingCost();
		
		//Gets the current buildings and to-build building numbers
		Map<Integer, Integer> currentNumbers = getCurrentNumbers();
		Map<Integer, Integer> buildingNumbers = getBuildingNumbers();
		
		//Loops through all the properties/rows of the current colour
		for(int i = 0; i < props.size(); i++) {
			int current = currentNumbers.get(i); //Numbers of buildings currently built
			int building = buildingNumbers.get(i); //Number of buildings to build
			
			int difference = Math.abs(current - building);
			
			//If fewer buildings are being built than there currently are, buildings are being demolished. Remove the cost of the
			//building * 0.9 from the building cost (money gained from demolishing)
			if(building < current) {
				total = total - (int) (difference * (buildingCost * 0.9f));
			} else if(building > current) {
				//If the number of buildings is more than the current, buildings are being built. Add the cost to build the required
				//number of buildings to the total building cost
				total += (buildingCost * difference);
			}
		}
		
		return total;
	}
	
	private BuildOperation getOperation() {
		boolean anyBuild = false; //Whether any buildings are being built
		boolean anyRemove = false; //Whether any buildings are being demolished
		
		List<PurchasableProperty> props = getProperties();
		
		//Gets the current buildings and to-build building numbers
		Map<Integer, Integer> currentNumbers = getCurrentNumbers();
		Map<Integer, Integer> buildingNumbers = getBuildingNumbers();
		
		//Loops through all the properties/rows of the current colour
		for(int i = 0; i < props.size(); i++) {
			int current = currentNumbers.get(i); //Numbers of buildings currently built
			int building = buildingNumbers.get(i); //Number of buildings to build
			
			//If there are fewer buildings than there currently are on the space, then buildings are being demolished
			if(building < current) anyRemove = true;
			
			//If there are more buildings than there currently are on the space, then buildings are being demolished
			if(building > current) anyBuild = true;
		}
		if(!anyBuild && !anyRemove) return BuildOperation.NOTHING; //Nothing to build and nothing to remove -> nothing has changed
		if(anyBuild && anyRemove) return BuildOperation.MODIFY; //Both buildings being demolished and built -> buildings are being modified
		if(anyRemove) return BuildOperation.DEMOLISH; //Just buildings being removed -> buildings are being demolished
		return BuildOperation.BUILD; //Otherwise (just buildings being built) -> buildings are being built
	}
	
	/**
	 * @return The number of buildings which are being changed (whether thats 
	 */
	private int getModifying() {
		int total = 0;
		List<PurchasableProperty> props = getProperties();
		
		//Gets the current buildings and to-build building numbers
		Map<Integer, Integer> currentNumbers = getCurrentNumbers();
		Map<Integer, Integer> buildingNumbers = getBuildingNumbers();
		
		//Loops through all the properties/rows of the current colour
		for(int i = 0; i < props.size(); i++) {
			int current = currentNumbers.get(i); //Numbers of buildings currently built
			int building = buildingNumbers.get(i); //Number of buildings to build
			
			//If the current number is different to the number to build, add to the total the difference in the values
			if(current != building) total += Math.abs(current - building);
		}
		return total;
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		Rectangle rect = new Rectangle(xOffset, yOffset, 700, 600);
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		
		//If the user clicks inside the property view card, set the movePoints to allow dragging
		if(rect.intersects(mouseRect)) {
			movePoint = new Point(xOffset, yOffset);
			mousePoint = new Point(mouse.getX(), mouse.getY());
		} 
		
		//Loop through all of the building buttons
		for(int i : buttons.keySet()) {
			List<BuildingButton> butts = buttons.get(i);
			for(int j = 0; j < butts.size(); j++) {
				BuildingButton button = butts.get(j);
				
				int result = button.mouseClick(mouseButton);
				//If the button wasn't clicked continue
				if(!(result == 1 || result == 0)) continue;
				
				int number = 0; //Will be the number of buildings to be selected
				
				//Calculates the number of buildings built on this row
				int furthest = 0;
				for(int k = 0; k < butts.size(); k++) {
					if(butts.get(k).isSelected()) furthest=k;
				}
				
				//if the furthest building is the building clicked:
				if(furthest == j) {
					//If currently clicked, set the number of buildings to be enabled to the current building index to disable the current button
					if(result == 1) number = j;
					//Otherwise, set the number of buildings to be enabled to the current building index + 1 to enable the current button
					else number = j + 1;
				} 
				//Otherwise, set the number of buildings to be enabled to the current building index + 1 (to remove 0 indexing)
				else number = j + 1;
				
				//Disable all the buttons, then enable only the number in the number variable
				for(int k = 0; k < butts.size(); k++) buttons.get(i).get(k).setSelected(false);
				for(int k = 0; k < number; k++) buttons.get(i).get(k).setSelected(true);
				
				//Validate the input to disable the build button is necessary
				validateBuilds();
				
				return;
			}
		}
		
		//Loops through all the rows / properties. If the user clicks on a row, set that row to the currently enabled row
		List<PurchasableProperty> props = getProperties();
		for(int i = 0; i < props.size(); i++) {
			Rectangle rowRect = new Rectangle(20, 220 + (i * 100), 800, 75);
			if(rowRect.intersects(mouseRect)) {
				selectedRow = i;
				property = getProperties().get(selectedRow);
			}
		}
		
		//Forward the mouse click to the build and cancel buttons
		buttonBuild.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		buttonCancel.mouseClick(mouseButton, mouse.getX(), mouse.getY());
	}
	
	/**
	 * @return A list of all the properties on the board of the current colour
	 */
	private List<PurchasableProperty> getProperties() {
		//Get the list of properties from the board.
		List<PurchasableProperty> props = board.getProperties(color);
		
		//Reverse the list (first item becomes the last) so that the properties are listed with the most expensive 
		//property at the top. Otherwise, the most expensive property would be at the bottom
		Collections.reverse(props);
		return props;
	}
	
	@Override
	public void mouseReleased(int mouseButton) {
		//Reset the move points to stop dragging
		movePoint = null;
		mousePoint = null;
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		//Set the previousScreen to the last screen. (Will be the screen returned to)
		if(!(lastScreen instanceof ScreenQuestion)) this.previousScreen = lastScreen;
	}
}
