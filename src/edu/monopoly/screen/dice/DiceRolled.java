package edu.monopoly.screen.dice;

import java.util.List;

public interface DiceRolled {

	//Used as a callback when the dice is rolled on the screen
	void diceRolled(List<Integer> numbers, int total);
}
