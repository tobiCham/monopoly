package edu.monopoly.screen.dice;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenInfo;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.GraphicUtils;

public class ScreenDiceRoll extends Screen {

	private static final Random rand = new Random(); //Generates the random numbers
	
	private Screen previousScreen; //The screen to return to after rolling the dice
	
	//Variables used to keep track of the spinning of the numbers animation
	private float time;
	private float maxTime = 0.01f;
	private int numb1, numb2;
	private boolean finished = false;
	
	private int total = 0; //Total number of spaces rolled
	private List<Integer> doubles = new ArrayList<>(); //List of all the numbers which have been rolled which are doubles
	private List<Integer> numbers = new ArrayList<>(); //List of all the numbers which have been rolled
	private DiceRolled rolled; //Callback instance to call after finishing running
	private OptionButton<Object> buttonOk;
	
	public ScreenDiceRoll(Monopoly game, DiceRolled rolled) {
		super(game);
		this.rolled = rolled;
	}

	@Override
	public void init() {
		int width = 400;
		Color c = new Color(180, 180, 180); //Colour to use for the button
		
		buttonOk = new  OptionButton<>(getGame(), (1920 / 2) - (width / 2), 700, width, 75, c, c, Color.black, "Ok", new Object[] {});
		buttonOk.setOnClick(this::click); //Call the click method when the button is clicked
	}
	
	/**
	 * Called when the button is clicked
	 */
	private void click() {
		//If the button text is "ok", the dice has finished being rolled
		if(buttonOk.getText().toLowerCase().contains("ok")) {
			Screen.setSelectedScreen(previousScreen);
			if(rolled != null) rolled.diceRolled(numbers, total);
		} else {
			//Otherwise, the it allows the player to roll again. Reset the finished and maxTime variables to spin
			//the dice and again
			finished = false;
			maxTime = 0.01f;
		}
	}

	@Override
	public void render(Graphics g) {
		//Render the previous screen as a background
		if(previousScreen != null) previousScreen.render(g);
		
		int width = 550;
		int height = 550;

		//Draws a partially transparent black background over the screen to darken the previous screen
		g.setColor(new Color(0, 0, 0, 120));
		g.fillRect(0, 0, 1920, 1080);
		
		//Draw a black border for the window
		g.setColor(Color.black);
		g.fillRect((1920 /2) - (width / 2) - 8, (1080 / 2) - (height / 2) - 8, width + 16, height + 16);
		
		//Draw the white background for the window
		g.setColor(Color.white);
		g.fillRect((1920 /2) - (width / 2), (1080 / 2) - (height / 2), width, height);
		
		//Draw the grey backgrounds for the two numbers currently shown
		g.setColor(Color.gray);
		g.fillRect(735, 400, 200, 200);
		g.fillRect(985, 400, 200, 200);
		
		//Use font size 100 and black and draw the current numbers on the dice inside the two grey boxes
		g.setColor(Color.black);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 100));
		GraphicUtils.drawCenteredString(g, numb1 + "", 530, 835);
		GraphicUtils.drawCenteredString(g, numb2 + "", 530, 1085);
		
		//Use font size 40
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 40));
		//Draw a message on the screen either saying that the dice is being rolled or showing the total number rolled
		String msg = !finished ? "Rolling Dice..." : "You rolled a " + (numbers.get(numbers.size() - 1) + numbers.get(numbers.size() - 2));
		GraphicUtils.drawCenteredString(g, msg, 360);
		
		//Render the Ok button if rendering has finished
		if(finished) buttonOk.render(g);
	}

	@Override
	public void update(double delta) {
		time += (delta / 1000000000f);
		//Add to the elapsed time the time to complete the last update
		
		//If the time elapsed is more than the time per number change
		if(time >= maxTime && !finished) {
			//Reset the time and increase the time per number change (so the rate of change slows)
			time = 0;
			maxTime *= 1.15f;
			
			//Generate two new random numbers
			numb1 = rand.nextInt(6) + 1;
			numb2 = rand.nextInt(6) + 1;
			
			//If the time per number change is greater than 0.2 seconds, finish the current roll
			if(maxTime >= 0.2) {
				finished = true;
				rollFinished();
			}
		}
	}
	
	/**
	 * Called when the "spinning" of the dice finishes and lands on two numbers
	 */
	private void rollFinished() {
		total += numb1 + numb2; //Add the total of the two numbers to the current total
		
		//Add the two numbers landed on to the list of numbers which have been rolled
		numbers.add(numb1);
		numbers.add(numb2);
		
		//If the numbers are the same, then a double has been rolled
		if(numb1 == numb2) {
			
			//Add the number of the double rolled to the list of doubles
			doubles.add(numb1);
			
			//If 3 doubles have been rolled, the player should be sent to Jail. Although this is handled elsewhere,
			//show a message telling the player they will be sent to Jail and play the siren sound effect.
			if(doubles.size() >= 3) {
				Screen.showConfirmMessage("You rolled 3 doubles!\nGo to Jail!", () -> {
					Sounds.SIREN.play();
					Screen.setSelectedScreen(previousScreen);
					
					//Run the dice called callback if it isn't null
					if(rolled != null) rolled.diceRolled(numbers, total);
				});
			} else buttonOk.setText("Roll Again"); //Otherwise, allow the player to roll again
		} else buttonOk.setText("Ok"); //Otherwise, set the button text to Ok to allow the player to finish rolling
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		//Forward the mouse click event the button if the roll has finished
		if(finished) buttonOk.mouseClick(mouseButton, mouse.getX(), mouse.getY());
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		//Set the previous screen (screen) to return to to the last screen. Ignore if its the Info screen
		if(!(lastScreen instanceof ScreenInfo)) this.previousScreen = lastScreen;
	}
}
