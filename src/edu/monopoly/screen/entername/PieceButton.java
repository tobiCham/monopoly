package edu.monopoly.screen.entername;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.player.PieceType;
import edu.monopoly.util.GraphicUtils;

public class PieceButton {

	private PieceType type; //Piece to display
	private int x, y, width, height; //Coordinates to render at
	private boolean selected = false; //Whether the button is selected, changes how it is rendered
	
	public PieceButton(PieceType type, int x, int y, int width, int height) {
		this.type = type;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void render(Graphics g) {
		Color old = g.getColor();
		
		int roundness = width / 2;
		
		Color borderColor = null;
		
		//Use gray if the button is being hovered and not selected, or red if its selected
		if(isHovered() && !selected) borderColor = Color.gray;
		if(selected) borderColor = Color.red;
		
		//If the border colour isn't null (there is a border), render it
		if(borderColor != null) {
			g.setColor(borderColor);
			int indent = (int) (width * 0.06f);
			g.fillRoundRect(x - indent, y - indent, width + (indent * 2), height + (indent * 2), roundness, roundness);
		}
		
		//Render the background for the button
		g.setColor(Color.white);
		g.fillRoundRect(x, y, width, height, roundness, roundness);
		
		//Render the icon of the piece while maintaining its aspect ratio
		GraphicUtils.renderMaintainAspect(g, type.getIcon(), x, y, width, height);
		g.setColor(old);
	}
	
	public void update() {}
	
	/**
	 * @return Whether the mouse in inside the bounds of the button
	 */
	public boolean isHovered() {
		Mouse mouse = Monopoly.getGameInstance().getDisplay().getMouse();
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		Rectangle rect = new Rectangle(x, y, width, height);
		return mouseRect.intersects(rect);
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public PieceType getType() {
		return type;
	}
}
