package edu.monopoly.screen.entername;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import com.tobi.guibase.Mouse;

import edu.monopoly.Difficulty;
import edu.monopoly.Monopoly;
import edu.monopoly.graphic.CharacterSet;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.graphic.TextBox;
import edu.monopoly.graphic.Transition;
import edu.monopoly.player.PieceType;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.Screens;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.ButtonUtils;

public class ScreenEnterName extends Screen {

	private TextBox nameBox; //The TextBox where the player will enter their name in
	private Difficulty difficulty = Difficulty.EASY; //Difficulty variable forwarded to this screen and to the play screen
	private List<PieceButton> pieceButtons = new ArrayList<>(); //List of buttons to allow the player to select a piece
	private OptionButton<Object> buttonStart, buttonCancel; //The start and cancel buttons
	
	public ScreenEnterName(Monopoly game) {
		super(game);
	}

	@Override
	public void init() {
		//The name enter box only will only allow letters and numbers to be entered
		nameBox = new TextBox(10, 120, 900, 100, Color.black, Color.white, CharacterSet.LETTERS_NUMBERS);
		nameBox.setCharacterLimit(16); //Maximum limit of 16 characters for the text box.
		
		int size = 180;
		int gap = 50;
		int startX = 20;
		int startY = 400;
		
		//Add piece buttons onto the screen for each of the pieces
		pieceButtons.add(new PieceButton(PieceType.CAR, startX, startY, size, size));
		pieceButtons.add(new PieceButton(PieceType.DOG, startX + gap + size, startY, size, size));
		pieceButtons.add(new PieceButton(PieceType.HAT, startX + (gap * 2) + (size * 2), startY, size, size));
		
		pieceButtons.add(new PieceButton(PieceType.IRON, startX, startY + gap + size, size, size));
		pieceButtons.add(new PieceButton(PieceType.THIMBLE, startX + gap + size, startY + gap + size, size, size));
		pieceButtons.add(new PieceButton(PieceType.WHEELBARROW, startX + (gap * 2) + (size * 2), startY + gap + size, size, size));
		
		//Initially set the first piece button to select
		pieceButtons.get(0).setSelected(true);
		
		//Initialise the start and cancel buttons
		buttonStart = new OptionButton<>(getGame(), 467 + 50, getGame().getDisplay().getInitialHeight() - 100 - 50, 467, 100, Color.black, Color.red, Color.white, "Start Game", new Object[0]);
		buttonStart.setOnClick(this::startClicked); //When clicked, calls the startClicked method
		
		buttonCancel = new OptionButton<>(getGame(), 20, getGame().getDisplay().getInitialHeight() - 100 - 50, 467, 100, Color.black, Color.red, Color.white, "Back", new Object[0]);
		//When the cancel button is clicked, return to the select difficulty screen
		buttonCancel.setOnClick(() -> Screen.setSelectedScreen(Screens.SELECT_DIFFICULTY));
		
		//Verify the options initially (will disable the start button as there won't be any text in the text box)
		validateInput();
	}
	
	private void startClicked() {
		//Loop through each of the piece buttons. If any of them are selected, set the piece to use to the corresponding piece
		PieceType piece = null;
		for(PieceButton button : pieceButtons) {
			if(button.isSelected()) {
				piece = button.getType();
				break;
			}
		}
		//If no piece button was selected, default to using the Hat piece
		if(piece == null) piece = PieceType.HAT;
		
		//Create a new play screen, using the name in the text box for the player name and the piece selected above
		ScreenPlay play = new ScreenPlay(getGame(), nameBox.getText().trim(), piece, difficulty);
		
		//Enter into the loading screen so that the user can see the progress of the game being loaded
		Screens.LOADING.setLoading(0f);
		Screens.LOADING.setLoadingMessage("Loading board");
		Screen.setSelectedScreen(Screens.LOADING);
		
		//Create a new thread to load the game asynchronously.
		new Thread(() -> {
			//Initialise the play screen and load it
			play.init();
			getGame().getDisplay().resize(1920, 1080);
			
			//Enter into the play screen and add a fade in animation lasting 1 second
			Screen.setSelectedScreen(play);
			Transition transition = new Transition();
			transition.fadeIn(getGame().getDisplay().getRenderFps());
			transition.startTransition();
			Sounds.BACKGROUND_MUSIC.play();
			Sounds.BACKGROUND_MUSIC.setLoop(true);
			
			//Show an introduction message to the player 1.5 seconds after entering the screen
			play.runDelayedTask(() -> {
				String message = "Welcome to\nEducational Monopoly!";
				message += "\n\nIf this is your first time";
				message += "\nplaying, be sure to check the";
				message += "\ninstruction manual.\n\nHave fun!";
				Screen.showConfirmMessage(message, null);
			}, 1.5f);
		}).start();
	}

	@Override
	public void render(Graphics g) {
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 75));
		nameBox.render(g); //Render the text box
		
		//Render text on the screen using white size 50 font giving instructions
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 50));
		g.setColor(Color.white);
		g.drawString("Enter your name:", 20, 90);
		g.drawString("Select a piece:", 20, 350);
		
		for(PieceButton button : pieceButtons) {
			button.render(g);
		}
		
		//Render the start and cancel buttons
		buttonStart.render(g);
		buttonCancel.render(g);
	}

	@Override
	public void update(double delta) {
		nameBox.update();
	}
	
	/**
	 * Validates the input and disables the start button if required
	 */
	private void validateInput() {
		boolean any = false;
		for(PieceButton button : pieceButtons) {
			if(button.isSelected()) {
				any = true;
				break;
			}
		}
		String error = null; //Error message. Will be initialised later, or if still null no errors
		if(!any) error = "Select a piece"; //If no piece is selected, the message is that the user needs to select a piece
		if(error == null) {
			//If there is no name entered in the text box, the message tells the user they need to enter a name
			String name = nameBox.getText() == null ? "" : nameBox.getText().trim();
			if(name.isEmpty()) {
				error = "Enter a name";
			}
		}
		//Disable the button if there is an error message, otherwise enable it
		ButtonUtils.setEnabled(buttonStart, Color.red, Color.gray, error, error == null);
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		nameBox.keyPress(key, ch);
		validateInput();
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		
		//Forward the mouse click onto the start and cancel buttons, as well as the text box
		buttonStart.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		buttonCancel.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		nameBox.mouseClick(mouseButton);
		
		//Loop through each PieceButton.
		PieceButton clicked = null;
		for(PieceButton button : pieceButtons) {
			//If the button is hovered, play the click sound and set the button which is clicked to this button
			if(button.isHovered()) {
				clicked = button;
				Sounds.CLICK.play();
				break;
			}
		}
		
		//If any button was clicked, set the button to selected and verify the input
		if(clicked == null) return;
		for(PieceButton button : pieceButtons) button.setSelected(false);
		clicked.setSelected(true);
		validateInput();
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		//When the screen is entered, reset the textbox and set the first piece button to selected
		nameBox.setText("");
		pieceButtons.get(0).setSelected(true);
	}
	
	@Override
	public void screenExited(Screen nextScreen) {
		//Reset the text box and disable all the piece buttons
		nameBox.setText("");
		for(PieceButton button : pieceButtons) button.setSelected(false);
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}
}
