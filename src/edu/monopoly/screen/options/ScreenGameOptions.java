package edu.monopoly.screen.options;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.saving.SavingManager;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenFileNameInput;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.ScreenYesNo;
import edu.monopoly.screen.Screens;
import edu.monopoly.sound.Sounds;
import edu.monopoly.util.ButtonUtils;
import edu.monopoly.util.FileUtils;

public class ScreenGameOptions extends Screen {

	private ScreenPlay previousScreen;
	private List<OptionButton<?>> buttons = new ArrayList<>();
	
	public ScreenGameOptions(Monopoly game) {
		super(game);
	}

	@Override
	public void init() {
		buttons.clear();
		
		//Temporary variables to calculate the positions of the buttons
		int width = 800;
		int height = 150;
		int x = (getGame().getDisplay().getInitialWidth() / 2) - (width / 2);
		int gapY = 50;
		int totalHeight = (4 * (height + gapY)) - gapY;
		int startY = (getGame().getDisplay().getInitialHeight() / 2) - (totalHeight / 2);
		
		Color color = Color.red;
		Color textColor = Color.white;
		
		//Instantiate the buttons for the menu.
		OptionButton<Object> buttonResume = new OptionButton<>(getGame(), x, startY, width, height, color, color, textColor, "Resume Game", new Object[]{});
		OptionButton<Object> buttonSave = new OptionButton<>(getGame(), x, (height + gapY) + startY, width, height, color, color, textColor, "Save Game", new Object[] {});
		OptionButton<Object> buttonOptions = new OptionButton<>(getGame(), x, (2 * (height + gapY)) + startY, width, height, color, color, textColor, "Options", new Object[] {});
		OptionButton<Object> buttonQuit = new OptionButton<>(getGame(), x, (3 * (height + gapY)) + startY, width, height, color, color, textColor, "Quit to Title", new Object[] {});
	
		//When resume game button is clicked, return to the previous screen
		buttonResume.setOnClick(() -> Screen.setSelectedScreen(previousScreen));
		
		//When the save button is clicked:
		buttonSave.setOnClick(() -> {
			//If a save name hasn't been set, go to the file name enter screen and then save the game with the name entered
			if(previousScreen.getSaveName() == null) {
				ScreenFileNameInput screen = new ScreenFileNameInput(getGame(), previousScreen, this::save);
				screen.init();
				Screen.setSelectedScreen(screen);
			} else save(); //Otherwise, save the game
		});
		
		//When the options button is clicked, enter into the Options screen
		buttonOptions.setOnClick(() -> {
			getGame().getDisplay().setBackgroundColor(Color.black);
			ScreenOptions options = new ScreenOptions(getGame(), this);
			options.init();
			Screen.setSelectedScreen(options);
		});
		
		//When the quit button is clicked, prompt the user to ensure they do want to quit the game. If they click yes, return to the title screen
		buttonQuit.setOnClick(() -> {
			ScreenYesNo screen = new ScreenYesNo(getGame(), "Are you sure you want to quit?\nYou will lose everything you\nhave not saved!", () -> {
				Screen.setSelectedScreen(Screens.TITLE);
				Sounds.BACKGROUND_MUSIC.play();
			}, null);
			screen.init();
			Screen.setSelectedScreen(screen);
		});
		
		//Disable the save button if it isn't the player's turn. This is due to complications from the AI
		if(previousScreen != null && previousScreen.getBoard().getCurrentPlayer() != previousScreen.getBoard().getHumanPlayer()) {
			ButtonUtils.setEnabled(buttonSave, Color.white, new Color(80, 80, 80), "Must be your turn.", false);
		} 
		
		//Add all the buttons to the list of buttons on the screen
		buttons.add(buttonResume);
		buttons.add(buttonSave);
		buttons.add(buttonOptions);
		buttons.add(buttonQuit);
	}
	
	/**
	 * Saves the game using the file name set in the play screen. 
	 */
	private void save() {
		//Directory of [gamedirectory]/saves/[savename].dat
		File saveFile = new File(FileUtils.getGameDirectory(), "saves/" + previousScreen.getSaveName() + ".dat");
		try {
			//Save the game using the file, then show the user a message telling them the game was successfully saved
			SavingManager.save(previousScreen, saveFile);
			Screen.showConfirmMessage("Saved!", null);
		} catch (Exception e) {
			//If there is an exception thrown, the game failed to save. Tell the user it failed to save
			e.printStackTrace();
			Screen.showConfirmMessage("Failed to save game:\n" + e.getMessage() + "\n\nTry again later", null);
		}
	}

	@Override
	public void render(Graphics g) {
		if(previousScreen != null) previousScreen.render(g); //Render the previous screen as a background
		
		//Draw a semi-transparent rectangle over the screen which has the effect of darkening it
		g.setColor(new Color(0, 0, 0, 100));
		g.fillRect(0, 0, getGame().getDisplay().getInitialWidth(), getGame().getDisplay().getInitialHeight());
		
		int width = 900;
		int height = 850;
		
		//Draw a round rectangle on the screen behind the buttons
		g.setColor(new Color(0, 0, 0, 120));
		g.fillRoundRect((getGame().getDisplay().getInitialWidth() / 2) - (width / 2), (getGame().getDisplay().getInitialHeight() / 2) - (height / 2), width, height, 50, 50);
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 50));
		//Loop through all the buttons and render them
		for(OptionButton<?> button : buttons) {
			button.render(g);
		}
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		//Forward the mouse click onto each button
		for(OptionButton<?> button : buttons) button.mouseClick(mouseButton, mouse.getX(), mouse.getY());
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		//Return to the previous screen when Escape is pressed
		if(key == KeyEvent.VK_ESCAPE) Screen.setSelectedScreen(previousScreen);
	}

	@Override
	public void update(double delta) {
		
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		if(lastScreen instanceof ScreenPlay) {
			this.previousScreen = (ScreenPlay) lastScreen;
			init(); //If the previous screen was the play screen, re-initialise for the save button
		}
		getGame().getDisplay().resize(1920, 1080);
		getGame().getDisplay().setBackgroundColor(new Color(10, 10, 10));
	}
}
