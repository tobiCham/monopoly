package edu.monopoly.screen.options;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tobi.guibase.RenderSetting;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.saving.GameProperties;
import edu.monopoly.saving.PropertiesFile;
import edu.monopoly.screen.Screen;
import edu.monopoly.sound.SoundOptions;

public class ScreenOptions extends Screen {
	
	private Map<String, Object> settingsMap; //Temporary store of settings so the main game properties is not modified
	private List<OptionButton<?>> buttons; //List of buttons on the screen
	
	private OptionButton<Boolean> musicButton, soundsButton;
	private OptionButton<RenderSetting> qualityButton;
	private Screen goTo; //Screen to go to after returning
	
	//Array of all the render settings which can be used in the game
	public static final RenderSetting[] RENDER_SETTINGS = {
		RenderSetting.PERFORMANCE, RenderSetting.DEFAULT, RenderSetting.BETTER, RenderSetting.GOOD, RenderSetting.FANTASTIC
	};
	
	public ScreenOptions(Monopoly game, Screen goToScreen) {
		super(game);
		this.goTo = goToScreen;
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		load(); //Refresh settings when the screen is entered
		getGame().getDisplay().resize(1024, 1024); //Use aspect ratio of 1024 x 1024 for the screen
	}
	
	/**
	 * Puts the values from the buttons into the settingsMap and then applies the changes in game
	 */
	private void save() {
		//Put the settings from the buttons values
		settingsMap.put("music", musicButton.getValue());
		settingsMap.put("sound", soundsButton.getValue());
		settingsMap.put("quality", qualityButton.getValue().toString());

		//Enable or disable music and sound
		SoundOptions.setMusicEnabled(musicButton.getValue());
		SoundOptions.setSoundEnabled(soundsButton.getValue());
		
		//Use the render setting currently in the button in the game
		getGame().getDisplay().setRenderSetting(qualityButton.getValue());
	}
	
	/**
	 * Loads the current settings and sets the buttons to the corresponding values
	 */
	private void load() {
		try {
			musicButton.setValue(GameProperties.getProperties().getBoolean("music"));
			soundsButton.setValue(GameProperties.getProperties().getBoolean("sound"));
			RenderSetting settings = getSetting(GameProperties.getProperties().getString("quality"));
			qualityButton.setValue(settings == null ? RenderSetting.GOOD : settings);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void init() {
		settingsMap = new LinkedHashMap<>();
		
		PropertiesFile prop = GameProperties.getProperties();
		//Copy the data from the game properties into the local settingsMap
		for(Object key : prop.getProperties().keySet()) {
			settingsMap.put(key.toString(), prop.get(key.toString()));
		}
		
		buttons = new ArrayList<>();
		
		musicButton = new OptionButton<>(getGame(), 25, 50, 462, 100, Color.black, Color.red, Color.white, "Music Enabled", new Boolean[]{true, false});
		buttons.add(musicButton);
		
		soundsButton = new OptionButton<>(getGame(), 462 + 50 + 25, 50, 462, 100, Color.black, Color.red, Color.white, "Sounds Enabled", new Boolean[]{true, false});
		buttons.add(soundsButton);
		
		qualityButton = new OptionButton<>(getGame(), 25, 200, 462, 100, Color.black, Color.red, Color.white, "Quality", RENDER_SETTINGS);
		buttons.add(qualityButton);

		OptionButton<Object> saveChanges = new OptionButton<>(getGame(), 20, getGame().getDisplay().getInitialHeight() - 200, 467, 100, Color.black, Color.red, Color.white, "Save", new Object[0]);
		saveChanges.setOnClick(() -> {
			//When the save button is clicked, call the save method which writes the values from the buttons into the settingsMap
			save();
			
			//Copy the data from the settingsMap into the game properties
			for(String key : settingsMap.keySet()) {
				prop.put(key, settingsMap.get(key));
			}
			try {
				//Finally save the properties to the player's computer
				GameProperties.getProperties().save();
			} catch (Exception e) {
				System.err.println("Error saving to properties file.");
				e.printStackTrace();
			}
			//Enter into the screen specified in the constructor
			Screen.setSelectedScreen(goTo);
		});
		buttons.add(saveChanges);

		OptionButton<Object> discardChanges = new OptionButton<>(getGame(), 467 + 50, getGame().getDisplay().getInitialHeight() - 200, 467, 100, Color.black, Color.red, Color.white, "Cancel", new Object[0]);
		discardChanges.setOnClick(() -> {
			//When the discard button is clicked, just return to the screen specified in the constructor. No properties will be set.
			Screen.setSelectedScreen(goTo);
		});
		buttons.add(discardChanges);
	}
	
	@Override
	public void render(Graphics g) {
		g.setFont(new Font(g.getFont().getFamily(), Font.PLAIN, 42));
		
		boolean any = false; //Indicates whether any of the buttons are being hovered
		//Loop through all the buttons and render them.
		for (OptionButton<?> button : buttons) {
			button.render(g);
			if (button.isHovered()) any = true;
		}
		//Use the hand cursor if any of the buttons were hovered over, or the default cursor if not
		Cursor cursor = Cursor.getPredefinedCursor(any ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR);
		getGame().getDisplay().getRenderPane().setCursor(cursor);
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		//Loop through all the buttons and forward the mouse click event on
		for (OptionButton<?> button : buttons) {
			button.mouseClick(mouseButton, getGame().getDisplay().getMouse().getX(), getGame().getDisplay().getMouse().getY());
		}
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		//If the escape key is pressed return to the screen specified in the constructor.
		//Is the same as clicking the discard button
		if (key == KeyEvent.VK_ESCAPE) Screen.setSelectedScreen(goTo);
	}
	
	@Override
	public void update(double delta) {}
	
	/**
	 * @return The RenderSetting with that name in the list of RenderSettings
	 * which can be used in the game, or null if none exist
	 */
	public static RenderSetting getSetting(String name) {
		for(RenderSetting setting : RENDER_SETTINGS) {
			if(setting.toString().equalsIgnoreCase(name)) {
				return setting;
			}
		}
		return null;
	}
}
