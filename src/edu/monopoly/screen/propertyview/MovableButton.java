package edu.monopoly.screen.propertyview;

import java.awt.Color;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;

public class MovableButton<T> extends OptionButton<T> {

	private final int startX, startY;
	
	public MovableButton(Monopoly game, int x, int y, int width, int height, Color edgeColor, Color insideColor, Color textColor, String text, T[] values) {
		super(game, x, y, width, height, edgeColor, insideColor, textColor, text, values);
		this.startX = x;
		this.startY = y;
	}
	
	/**
	 * Moves the button a certain amount from the start coords
	 */
	public void setOffSet(int dx, int dy) {
		setBounds(startX + dx, startY + dy, getWidth(), getHeight());
	}
	
	public int getStartX() {
		return startX;
	}

	public int getStartY() {
		return startY;
	}
}
