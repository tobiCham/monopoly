package edu.monopoly.screen.propertyview;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.board.Board;
import edu.monopoly.player.ComputerPlayer;
import edu.monopoly.player.HumanPlayer;
import edu.monopoly.player.Player;
import edu.monopoly.property.ColoredProperty;
import edu.monopoly.property.PropertyColors;
import edu.monopoly.property.PurchasableProperty;
import edu.monopoly.property.TransportProperty;
import edu.monopoly.property.UtilityProperty;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.build.ScreenBuild;
import edu.monopoly.screen.question.ScreenQuestion;
import edu.monopoly.screen.trade.ScreenTrade;
import edu.monopoly.util.ButtonUtils;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.NumberUtils;
import edu.monopoly.util.StringUtils;

//A screen which shows information about a property, and also allows a player to buy the property
public class ScreenPropertyView extends Screen {

	private PurchasableProperty property; //Property currently viewing
	private Point movePoint, mousePoint;
	private static int xOffset = 610, yOffset = 240;
	private ScreenPlay prevScreen; //Screen to return to
	private Board board; //Instance of the board for the current game
	private List<MovableButton<?>> buttons = new ArrayList<>(); //List of all the buttons on the screen to render
	private boolean landed; //Whether or not the player has landed on this space. Affects which buttons are shown
	
	/**
	 * @param landed Whether the player has landed on the property or just clicked it. If set to true they will be
	 * able to buy the property
	 */
	public ScreenPropertyView(Monopoly game, Board board, PurchasableProperty property, boolean landed) {
		super(game);
		this.property = property;
		this.landed = landed;
		this.board = board;
	}

	@Override
	public void init() {
		Player human = board.getHumanPlayer();
		Player owner = board.getOwner(property);
		
		if(landed) { //If the player has landed on the property, add the buy property functionality
			
			Color c = PropertyColors.LIGHT_BLUE.getColor(); //Use a light blue colour for the button
			
			MovableButton<?> buttonBuy = new MovableButton<>(getGame(), 375, 400, 300, 50, c, c, Color.black, "Buy for " + StringUtils.POUND_SIGN + property.getCost(), new Object[]{});
			MovableButton<?> buttonBack = new MovableButton<>(getGame(), 375, 500, 300, 50, PropertyColors.LIGHT_BLUE.getColor(), PropertyColors.LIGHT_BLUE.getColor(), Color.black, "Back", new Object[]{});
			
			//Enable the buy button is the player has enough money. Otherwise disable it telling the user they don't have enough money
			ButtonUtils.setEnabled(buttonBuy, PropertyColors.LIGHT_BLUE.getColor(), Color.gray, "Not enough money!", human.getMoney() >= property.getCost());
			buttonBuy.setOnClick(this::buyClicked);
			buttonBack.setOnClick(() -> Screen.setSelectedScreen(prevScreen)); //Back button is clicked, return to the previous screen
			
			//Add the two buttons to the list of buttons
			buttons.add(buttonBuy);
			buttons.add(buttonBack);
		} else {
			//Otherwise, the player hasn't landed on the property so can't buy. Add the close button
			MovableButton<?> buttonClose = new MovableButton<>(getGame(), 375, 520, 300, 50, PropertyColors.LIGHT_BLUE.getColor(), PropertyColors.LIGHT_BLUE.getColor(), Color.black, "Close", new Object[]{});
			buttonClose.setOnClick(() -> Screen.setSelectedScreen(prevScreen)); //When clicked return to the previous screen
			buttons.add(buttonClose);
			
			//If another player owns the property and it is the player's turn, add a button to allow them to trade for it
			if(owner != null && owner != board.getHumanPlayer() && board.getCurrentPlayer() == board.getHumanPlayer()) {
				MovableButton<?> buttonTrade = new MovableButton<>(getGame(), 375, 435, 300, 50, PropertyColors.LIGHT_BLUE.getColor(), PropertyColors.LIGHT_BLUE.getColor(), Color.black, "Trade for this", new Object[]{});
				buttonTrade.setOnClick(this::tradeClicked);
				buttons.add(buttonTrade);
			}
			
			//If the player owns the property and is currently the player's turn, add the buttons for Mortgaging/Unmortgaging and Building
			if(owner == board.getHumanPlayer() && board.getCurrentPlayer() == board.getHumanPlayer()) {
				int mortgageY = 425; //Y coordinate of the mortgage property
				if(property instanceof ColoredProperty) { //If the property is a ColoredProperty, add the build button
					
					String problem = null; //Problem message to display to the user if set
					PropertyColors color = board.getColor(property);
					int required = board.getProperties(color).size();
					List<PurchasableProperty> owned = board.getPropertiesOwned(color, human);
					int numbOwned = owned.size();
					
					for(PurchasableProperty p : owned) { //If any of the properties are mortgaged, the player can't build
						if(p.isMortgaged()) problem = "Cannot have mortgaged properties";
					}
					//If the player doesn't own all the properties of the colour
					if(numbOwned < required) problem = "Not enough properties owned";
					
					//Create the build button. Only enable the button if there wasn't a problem which would prevent the player from building
					MovableButton<?> buttonBuild = new MovableButton<>(getGame(), 375, 370, 300, 50, PropertyColors.LIGHT_BLUE.getColor(), PropertyColors.LIGHT_BLUE.getColor(), Color.black, "Build / Demolish", new Object[]{});
					ButtonUtils.setEnabled(buttonBuild, PropertyColors.LIGHT_BLUE.getColor(), Color.gray, problem, problem == null);
					buttonBuild.setOnClick(this::buildClicked);
					buttons.add(buttonBuild);
					
					//Move the mortgage button down 25 pixels so that all the buttons fit
					mortgageY += 25;
				}
				//Create the mortgage button at the mortgageY y-coordinate.
				MovableButton<?> buttonMortgage = new MovableButton<>(getGame(), 375, mortgageY, 300, 50, PropertyColors.LIGHT_BLUE.getColor(), PropertyColors.LIGHT_BLUE.getColor(), Color.black, "Mortgage for " + StringUtils.POUND_SIGN + property.getMortgageValue(), new Object[]{});
				
				String problem = null; //Problem message to display to the user if set
				if(property.isMortgaged()) {
					//If the property is mortgaged, set the text of the button to show the cost of unmortgaging
					int unmortgageCost = (int) (property.getMortgageValue() * 1.1f);
					buttonMortgage.setText("Unmortgage for " + StringUtils.POUND_SIGN + unmortgageCost);
					
					//If the player doesn't have enough money to unmortgage, set the problem to this message
					if(human.getMoney() < unmortgageCost) problem = "Not enough money";
				} else if(property instanceof ColoredProperty && human.hasBuildings(((ColoredProperty) property).getColor())) {
					//If any buildings are built on any of the properties of this colour, set the message to tell the user that they
					//can't mortgage with buildings on
					problem = "Property has buildings";
				}
				
				//Enable the mortgage button if the problem is equal to null
				ButtonUtils.setEnabled(buttonMortgage, PropertyColors.LIGHT_BLUE.getColor(), Color.gray, problem, problem == null);
				buttonMortgage.setOnClick(this::mortgageClicked); //Call the mortgageClicked method when it is clicked
				buttons.add(buttonMortgage);
			}
		}
	}
	
	/**
	 * Called when the mortgage or unmortgage button is clicked (same button, different text)
	 */
	private void mortgageClicked() {
		HumanPlayer human = board.getHumanPlayer();
		
		if(property.isMortgaged()) { //Unmortgage
			int unmortgageCost = (int) (property.getMortgageValue() * 1.1f);
			if(unmortgageCost > human.getMoney()) return; //Return if the player doesn't have enough money, return
			
			//Create a question asking the user how much money they have after unmortgaging
			String ques = "You currently have " + StringUtils.POUND_SIGN + human.getMoney() + " and you have to pay " + StringUtils.POUND_SIGN + unmortgageCost + "\n";
			ques += "for unmortgaging " + property.getName() + ".\n\n";
			ques += "How much money do you now have?";
			ScreenQuestion question = new ScreenQuestion(getGame(), ques, human.getMoney() - unmortgageCost, () -> {
				//After getting the question correct, remove the cost to unmortgage from the player's account and set the property to unmortgaged
				board.getHumanPlayer().setMoney(human.getMoney() - unmortgageCost);
				property.setMortgaged(false);
				Screen.setSelectedScreen(prevScreen);
			});
			question.init();
			Screen.setSelectedScreen(question);
		} else { //Mortgage
			
			//Create a question asking the user how much money they have after mortgaging
			String ques = "You currently have " + StringUtils.POUND_SIGN + human.getMoney();
			ques += " and you gain " + StringUtils.POUND_SIGN + property.getMortgageValue() + "\n";
			ques += "from mortgaging " + property.getName() + "\n\n";
			ques += "How much money do you now have?";
			ScreenQuestion question = new ScreenQuestion(getGame(), ques, human.getMoney() + property.getMortgageValue(), () -> {
				//After getting the question correct, add the money gained from mortgaging to the player's account and set the property to mortgaged
				human.setMoney(human.getMoney() + property.getMortgageValue());
				property.setMortgaged(true);
				Screen.setSelectedScreen(prevScreen);
			});
			question.init();
			Screen.setSelectedScreen(question);
		}
	}

	/**
	 * Called when the buy property button is clicked
	 */
	private void buyClicked() {
		//If the player doesn't have enough money or the property is mortgaged return
		if(board.getHumanPlayer().getMoney() < property.getCost() || property.isMortgaged()) return;
		
		//Create a question asking the user how much money they have after purchasing
		String question = "You have bought " + property.getName() + "!\n";
		question += "You have " + StringUtils.POUND_SIGN + board.getHumanPlayer().getMoney() + " and " + property.getName() + " costs " + StringUtils.POUND_SIGN + property.getCost() + "\n";
		question += "After buying, how much money do you have?";
		ScreenQuestion screen = new ScreenQuestion(getGame(), question, board.getHumanPlayer().getMoney() - property.getCost(), () -> {
			//On getting the question correct, remove the cost of the property from the player's account and set them as the owner
			board.getHumanPlayer().setMoney(board.getHumanPlayer().getMoney() - property.getCost());
			board.setOwner(property, board.getHumanPlayer());
			Screen.setSelectedScreen(prevScreen);
		});
		screen.init();
		Screen.setSelectedScreen(screen);
	}
	
	/**
	 * Called when the trade button is clicked
	 */
	private void tradeClicked() {
		ComputerPlayer owner = (ComputerPlayer) board.getOwner(property); //Get the owner of the property
		if(owner == null) return;
		
		//Create the trade screen
		ScreenTrade trade = new ScreenTrade(getGame(), board, prevScreen, owner);
		trade.init();
		
		//Add the property to the list of properties being traded
		trade.setSelectedProperty(property);
		trade.addOtherTrade(property);
		
		Screen.setSelectedScreen(prevScreen); //Briefly enter into the previous screen so that the trade screen will return to it and not this screen
		Screen.setSelectedScreen(trade);
	}
	
	/**
	 * Called when the build button is clicked
	 */
	private void buildClicked() {
		//Create the build screen
		ScreenBuild screen = new ScreenBuild(getGame(), board.getColor(property), board);
		screen.init();
		Screen.setSelectedScreen(prevScreen); //Briefly enter into the previous screen so that the build screen returns to it
		Screen.setSelectedScreen(screen);
	}
	
	public static void renderPropertyCard(Graphics g, int x, int y, PurchasableProperty property, Board board) {
		
		//Render the background for the card
		g.setColor(Color.white);
		g.fillRect(x, y, 700, 600);
		
		//Draw a black border around the property name box
		g.setColor(Color.black);
		g.fillRect(x + 5, y + 5, 340, 85);
		
		//Set the colour for the name box background to the colour of the property, or grey if the property isn't a ColoredProperty
		if(property instanceof ColoredProperty) g.setColor(((ColoredProperty) property).getColor().getColor());
		else g.setColor(Color.gray);
		//Draw the background for the name of the property
		g.fillRect(x + 10, y + 10, 330, 75);
		
		//Draw the name of the property as a title in the centre of the box using black size 38 text
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 38));
		g.setColor(Color.black);
		GraphicUtils.drawCenteredString(g, property.getName(), y + 62, x + 175);
		
		//Draw a border around the property information area (which is a shade of green)
		g.fillRect(x + 5, y + 95, 340, 495);
		
		//Use a shade of green for the background, but use a darker colour if the property is mortgaged
		if(!property.isMortgaged()) g.setColor(new Color(135, 200, 150));
		else g.setColor(new Color(160, 175, 165));
		
		g.fillRect(x + 10, y + 100, 330, 485); //Draw the background for the property information
		
		g.setColor(Color.white);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 30));
		
		//Map of the lines of text to display. Use a LinkedHashMap to keep ordering. Key is the text on the left hand side, value 
		//if the text on the right hand side per line. E.g. Owner -> Computer Player 1. Due to the map not being able to contain
		//duplicate keys however, each blank line has to have an extra space as the key.
		Map<String, String> lines = new LinkedHashMap<>();
		
		//Add information about the price of the property, the money gained by mortgaging and the owner of the property
		lines.put("Price:", property.getCost() + "");
		lines.put("Mortgage:", property.getMortgageValue() + "");
		Player owner = board.getOwner(property);
		lines.put("Owner:", owner == null ? "Unowned" : owner.getName());
		lines.put("", "");
		
		//If the property is a ColoredProperty is can have buildings on it. Add information about the cost of houses
		if(property instanceof ColoredProperty) {
			ColoredProperty colored = (ColoredProperty) property;
			lines.put("Rent:", "");
			lines.put("   Normal:", property.getRent() + "");
			lines.put("   Monopoly:", property.getRent() * 2 + "");
			lines.put("   1 House:", colored.getBuildingRent(1) + "");
			for(int i = 2; i <= 4; i++) lines.put("   " + i + " Houses:", colored.getBuildingRent(i) + "");
			lines.put("   Hotel:", colored.getBuildingRent(5) + "");
			lines.put("  ", "");
			lines.put("Building Cost: ", colored.getBuildingCost() + "");
		}
		//If the property is a Utility property, add information about rent based on dice throw
		if(property instanceof UtilityProperty) {
			lines.put("Rent:", "");
			lines.put("   If 1 owned:", "");
			lines.put("      " + StringUtils.POUND_SIGN + "4 x Dice Roll", "");
			lines.put(" ", "");
			lines.put("   If both owned:", "");
			lines.put("      " +  + StringUtils.POUND_SIGN + "10 x Dice Roll", "");
		}
		//If the property is a Transport property add information on rent based on number owned
		if(property instanceof TransportProperty) {
			lines.put("Rent:", "");
			lines.put("   1 Owned:", property.getRent(1) + "");
			lines.put("   2 Owned:", property.getRent(2) + "");
			lines.put("   3 Owned:", property.getRent(3) + "");
			lines.put("   4 Owned:", property.getRent(4) + "");
		}
		
		int gap = 33; //Gap in pixels between each line
		int i = 0; //Line counter
		for(String line : lines.keySet()) { //Loop through each line in the map
			if(!line.trim().isEmpty()) { //If there is text on the line:
				g.drawString(line, x + 20, y + 140 + (i * gap)); //Draw the line text on the left of the box
				
				String value = lines.get(line); //Corresponding value of the line
				if(!value.trim().isEmpty()) { //If there is a value of the line:
					if(NumberUtils.isInt(value)) value = StringUtils.POUND_SIGN + value; //If the value if a number, append a pound sign on the start
					
					//Gets the width of the text and right justifies it relative to the box
					int strWidth = g.getFontMetrics(g.getFont()).getStringBounds(value, g).getBounds().width; 
					g.drawString(value, x + 10 + 330 - strWidth - 10, y + 140 + (i * gap));
				}
			}
			i++; //Add 1 to the line counter
		}
		
		//Draw a dividing line down the centre of the card
		g.setColor(Color.black);
		g.fillRect(x + 349, y + 4, 2, 586);
		
		int imgSize = 332; //Image size in pixels
		int imgY = y + (350 / 2) - (imgSize / 2);
		g.fillRect(x + 350 + (350 / 2) - (imgSize / 2) - 5, imgY - 5, imgSize + 10, imgSize + 10); //Draw a black border around the image
		//Draw the property image on the top right of the card
		g.drawImage(property.getImage(), x + 350 + (350 / 2) - (imgSize / 2), imgY, imgSize, imgSize, null);
	}
	
	@Override
	public void render(Graphics g) {
		//Render the previous screen as a background
		if(prevScreen != null) prevScreen.render(g);
		
		//Render the property card at the x and y offsets
		renderPropertyCard(g, xOffset, yOffset, property, board);
		
		//Render the buttons at the x and y offsets
		for(MovableButton<?> button : buttons) {
			button.setOffSet(xOffset, yOffset);
			button.render(g);
		}
	}

	@Override
	public void update(double delta) {
		Mouse mouse = getGame().getDisplay().getMouse();
		
		//If the movePoint and mousePoint aren't null, the property card is being dragged. Set the offset to the mouse coordinates
		//and check to make sure that the card doesn't go out of bounds of the board
		if(movePoint != null && mousePoint != null) {
			int xDiff = (mouse.getX() - mousePoint.x);
			int yDiff = (mouse.getY() - mousePoint.y);
			xOffset = movePoint.x + xDiff;
			yOffset = movePoint.y + yDiff;
			
			if(xOffset < 420) xOffset = 420;
			if(xOffset > 1920 - 420 - 700) xOffset = 1920 - 420 - 700;
			
			if(yOffset < 0) yOffset = 0;
			if(yOffset + 600 > 1080) yOffset = 1080 - 600;
		}
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		Rectangle rect = new Rectangle(xOffset, yOffset, 700, 600); //Bounds for the card
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		
		//If the mouse is clicked on the property card
		if(rect.intersects(mouseRect)) {
			movePoint = new Point(xOffset, yOffset);
			mousePoint = new Point(mouse.getX(), mouse.getY());
		} 
		//Otherwise, return to the previous screen if the mouse was clicked outside the bounds to "hide" the card
		else if(!landed) Screen.setSelectedScreen(prevScreen);
	}
	
	@Override
	public void mouseReleased(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		
		//When the mouse is released, set the mouse and move points to null to disable dragging
		if(movePoint != null && mousePoint != null) {
			int xDiff = mouse.getX() - mousePoint.x;
			int yDiff = mouse.getY() - mousePoint.y;
			
			movePoint = null;
			mousePoint = null;
			
			//If the card has moved less than 5 pixels in both the x and y directions, forward the click to all the buttons.
			//This prevents the buttons being clicked when the user intended to move the card
			if(Math.abs(xDiff) <= 5 && Math.abs(yDiff) <= 5) {
				for(MovableButton<?> button : buttons) {
					button.mouseClick(mouseButton, mouse.getX(), mouse.getY());
				}
			}
		}
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		//Set the previousScreen to the last screen. (Will be the screen returned to)
		if(lastScreen instanceof ScreenPlay) this.prevScreen = (ScreenPlay) lastScreen;
	}
}
