package edu.monopoly.screen.question;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.CharacterSet;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.graphic.TextBox;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenInfo;
import edu.monopoly.util.ButtonUtils;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.NumberUtils;

public class ScreenQuestion extends Screen {

	private String question;
	private int correctAnswer;
	private int attemptsLeft;
	private Screen previousScreen;
	private TextBox box;
	private OptionButton<Object> buttonAnswer, buttonReset;
	private List<Integer> answers = new ArrayList<>();
	private Runnable onSucceed, onFail;
	private String succeedMessage = "Correct!";
	private String failMessage = "Incorrect Answer";
	
	/**
	 * For a limited number of attempts
	 */
	public ScreenQuestion(Monopoly game, String question, int answer, int maxAttempts, Runnable onSucceed, Runnable onFail) {
		super(game);
		this.question = question;
		this.correctAnswer = answer;
		this.attemptsLeft = maxAttempts;
		this.onSucceed = onSucceed;
		this.onFail = onFail;
	}
	
	/**
	 * For unlimited attempts
	 */
	public ScreenQuestion(Monopoly game, String question, int answer, Runnable onSucceed) {
		this(game, question, answer, -1, onSucceed, null); //Call the constructor using -1 as the number of attempts which will indicate there isn't a limit
	}

	@Override
	public void init() {
		
		Color c = new Color(180, 180, 180); //Colour to use for the text box and buttons
		box = new TextBox(735, (1080 / 2) - (75 / 2), 450, 75, Color.black, c, CharacterSet.NUMBERS); //Create the text box in the centre of the window
		
		//Temporary variables to calculate the positions of the buttons
		int gap = 50;
		int width = (920 - (gap * 3)) / 2; 
		
		buttonAnswer = new OptionButton<>(getGame(), (1920 / 2) + (gap / 2), 850, width, 75, c, c, Color.black, "Confirm Anwer", new Object[] {});
		buttonAnswer.setOnClick(this::answer); //Call the answer method when the answer button is clicked
		
		buttonReset = new OptionButton<>(getGame(), (1920 / 2) - (gap / 2) - width, 850, width, 75, c, c, Color.black, "Reset", new Object[] {});
		buttonReset.setOnClick(this::reset); //Call the reset method when the reset button is clicked
		
		reset(); //Call the reset method here to initialise the text box to default values
	}
	
	/**
	 * Resets the text box, removing all text and setting it to selected
	 */
	private void reset() {
		box.setText("");
		box.setCursorPosition(0);
		box.setBlankText("Answer Here");
		box.setCharacterLimit(5);
		box.setSelected(true);
		validateInput(); //Validates the input again
	}

	
	private void answer() {
		
		//If the text box value isn't an Integer, return out the method. This should never happen.
		if(!NumberUtils.isInt(box.getText())) return;
		
		int answer = Integer.parseInt(box.getText());
		
		//If the user has already tried this answer, return as they shouldn't be able to guess the same number twice.
		if(this.answers.contains(answer)) return;
		answers.add(answer);
		
		if(answer != this.correctAnswer) {
			//Remove an attempt. If it is 0, the player failed the question. Using a direct comparison here, so that if the attemptsLeft was initially -1 (unlimited), then
			//it will keep decreasing and never equal 0
			attemptsLeft--;
			if(attemptsLeft == 0) {
				//Failing the question, send the user a message then return to the previous screen and run the onFail runnable if it isn't null
				Screen.showConfirmMessage(failMessage + "\nYou failed this question.", () -> {
					Screen.setSelectedScreen(previousScreen);
					if(onFail != null) onFail.run();
				});
			} else { //User failed the question but still has attempts left
				String message = new String(failMessage); //Clone the string so that the original string isn't being modified

				//Append on text telling the user how many attempts they have left if its more than 0 (less than 0 would be unlimited attempts)
				if(attemptsLeft >= 0) message += "\nYou have " + attemptsLeft + " " + (attemptsLeft == 1 ? "attempt" : "attempts") + " left";
				
				//When the user clicks Ok, validate the input again. This will usually have the effect of disabling the "Confirm Answer" button as the asnwer has
				//already been tried
				Screen.showConfirmMessage(message, this::validateInput);
			}
		} else {
			//User got the question correct. Send them a message and run the onSucceed runnable if it isn't null
			Screen.showConfirmMessage(succeedMessage, () -> {
				Screen.setSelectedScreen(previousScreen);
				if(onSucceed != null) onSucceed.run();
			});
			
		}
	}
	
	@Override
	public void render(Graphics g) {
		//Render the previous screen as a background
		if(previousScreen != null) previousScreen.render(g);
		
		//Draw a white box with a black outline
		g.setColor(Color.black);
		g.fillRect(495, 75, 930, 930);
		g.setColor(Color.white);
		g.fillRect(500, 80, 920, 920);
		g.setColor(Color.black);
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 50));
		
		//Get the width of the text in pixels, then draw it in the centre of the screen
		int width = g.getFontMetrics(g.getFont()).getStringBounds("Question", g).getBounds().width;
		int startX = (1920 / 2) - (width / 2);
		g.drawString("Question", startX, 200);
		
		//Draw a black rectangle under the "Question" text as an underline
		g.fillRect(startX, 202, width, 5);
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 36));
		
		//Draw the question text in the box
		int lineDiff = (int) (g.getFont().getSize() * 1.2f);
		GraphicUtils.renderText(g, question, getGame().getDisplay().getInitialWidth() / 2, 285, 50, lineDiff);
		
		//Use font size 50, then render the answer input text box
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 50));
		box.render(g);
		
		//Use font size 42, then render the answer and reset buttons
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 42));
		buttonAnswer.render(g);
		buttonReset.render(g);
	}

	@Override
	public void update(double delta) {
		box.update();
	}
	
	/**
	 * Checks the input the user has given to ensure it is valid. Will disable the "Confirm Answer" button if not
	 */
	private void validateInput() {
		String problem = null;
		
		String txt = box.getText();
		int answer = -1;
		if(txt != null) {
			//Set the answer value to the int value of the text box if it is a valid integer
			if(NumberUtils.isInt(txt)) answer = Integer.parseInt(txt);
		}
		if(answer < 0) problem = "Enter an answer!"; //If the answer is less than 0, then there is not a valid answer given
		if(answers.contains(answer)) problem = "You have already tried that answer"; //If the user has already tried that answer
		if(attemptsLeft == 0) problem = "No more attempts";
		
		//Only enable the button if there isn't a problem
		ButtonUtils.setEnabled(buttonAnswer, new Color(180, 180, 180), new Color(80, 80, 80), problem, problem == null);
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		//Forward the key press event to the text box then validate the input
		box.keyPress(key, ch);
		validateInput();
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		
		//Forward the mouse click onto the text box and buttons
		box.mouseClick(mouseButton);
		buttonAnswer.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		buttonReset.mouseClick(mouseButton, mouse.getX(), mouse.getY());
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		//Set the previous screen to the last screen entered as long as the last screen isn't an InfoScreen.
		//This is because the info screen is shown to tell the user if they got an answer correct or not, and shouldn't be returned to
		//after failing or answering a question
		if(!(lastScreen instanceof ScreenInfo)) previousScreen = lastScreen;
	}

	public String getSucceedMessage() {
		return succeedMessage;
	}

	public void setSucceedMessage(String succeedMessage) {
		this.succeedMessage = succeedMessage;
	}

	public String getFailMessage() {
		return failMessage;
	}

	public void setFailMessage(String failMessage) {
		this.failMessage = failMessage;
	}
}
