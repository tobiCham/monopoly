package edu.monopoly.screen.selectdifficulty;

import java.awt.Color;

import edu.monopoly.Difficulty;
import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.Screens;
import edu.monopoly.util.StringUtils;

public class ButtonSelectDifficulty extends OptionButton<Object> {
	
	private Difficulty difficulty;
	
	public ButtonSelectDifficulty(Monopoly game, int x, int y, int width, int height, Difficulty difficulty) {
		super(game, x, y, width, height, Color.red, Color.red, Color.white, StringUtils.format(difficulty.toString()), new Object[0]);
		this.difficulty = difficulty;
		setOnClick(this::click); //When the button is clicked, call the private click method
	}
	
	private void click() {
		//When the button is clicked, enter into the name enter screen and forward on the difficulty
		Screen.setSelectedScreen(Screens.ENTER_NAME);
		Screens.ENTER_NAME.setDifficulty(difficulty);
	}
	
	/**
	 * @return The difficulty represented by this button
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}
}
