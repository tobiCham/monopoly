package edu.monopoly.screen.selectdifficulty;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import com.tobi.guibase.Mouse;

import edu.monopoly.Difficulty;
import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.Screens;

public class ScreenSelectDifficulty extends Screen {
	
	private List<OptionButton<?>> buttons;
	private final int NUMB_BUTTONS = 4;
	
	public ScreenSelectDifficulty(Monopoly game) {
		super(game);
	}
	
	@Override
	public void init() {
		buttons = new ArrayList<>();
		 
		//Local variables used to calculate the positions of the buttons
		int width = 800;
		int height = 200;
		int x = (getGame().getDisplay().getInitialWidth() - width) / 2;
		int gapY = 50;
		int totalHeight = (NUMB_BUTTONS * (height + gapY)) - gapY;
		int startY = (getGame().getDisplay().getInitialHeight() - totalHeight) / 2;
		
		//Loop from 0 to the number of difficulties there are (3)
		int counter = 0;
		for (; counter < Difficulty.values().length; counter++) {
			Difficulty difficulty = Difficulty.values()[counter]; //Get the difficulty at the specified index
			
			//Add a SelectDifficulty button to the list of buttons
			ButtonSelectDifficulty button = new ButtonSelectDifficulty(getGame(), x, (counter * (height + gapY)) + startY, width, height, difficulty);
			buttons.add(button);
		}
		
		//Back buttons
		OptionButton<Object> button = new OptionButton<>(getGame(), x, (counter * (height + gapY)) + startY, width, height, Color.red, Color.red, Color.white, "Back", new Object[0]);
		button.setOnClick(() -> Screen.setSelectedScreen(Screens.TITLE)); //When clicked return to the title screen
		buttons.add(button);
	}
	
	@Override
	public void render(Graphics g) {
		Font oldFont = g.getFont();
		g.setFont(new Font(oldFont.getFontName(), Font.PLAIN, 100));
		
		boolean any = false;
		//Loop through each button on the screen and render it. If the button is hovered set any to true
		for (OptionButton<?> button : buttons) {
			button.render(g);
			if (button.isHovered()) any = true;
		}
		//Use the hand cursor if any of the buttons is hovered, otherwise use the default cursor
		getGame().getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(any ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR));
		g.setFont(oldFont);
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		//If the escape key is pressed, return to the title
		if (key == KeyEvent.VK_ESCAPE) Screen.setSelectedScreen(Screens.TITLE);
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		//Forward the mouse click event onto all of the buttons on the screen
		for (OptionButton<?> button : buttons) {
			button.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		}
	}
	
	@Override
	public void update(double delta) {}
}
