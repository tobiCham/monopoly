package edu.monopoly.screen.title;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.Transition;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.Screens;
import edu.monopoly.screen.options.ScreenOptions;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.ImageLoader;

public class ScreenTitle extends Screen {
	
	private Image image; //Title screen image to show
	private List<TitleButton> buttons; //List containing all the buttons on the screen
	
	public ScreenTitle(Monopoly game) {
		super(game);
	}
	
	@Override
	public void init() {
		buttons = new ArrayList<>();
		image = ImageLoader.loadImage("title_man.png");
		
		int width = 525;
		int height = 100;
		int gap = 150;
		
		//New Game button, when clicked enter into the select difficulty screen
		buttons.add(new TitleButton(400, width, height, "New Game", () -> Screen.setSelectedScreen(Screens.SELECT_DIFFICULTY)));
		
		//Load Game button, when clicked enter into the load game screen
		buttons.add(new TitleButton(400 + gap, width, height, "Load Game", () -> Screen.setSelectedScreen(Screens.LOAD_GAME)));
		
		//Settings button, when clicked enter into the options screen
		buttons.add(new TitleButton(400 + (gap * 2), width, height, "Settings", () -> {
			ScreenOptions options = new ScreenOptions(Monopoly.getGameInstance(), this);
			options.init();
			Screen.setSelectedScreen(options);
		}));
		
		//Quit game button, when clicked exit the application
		buttons.add(new TitleButton(400 + (gap * 3), width, height, "Quit Game", () -> System.exit(0)));
	}
	
	@Override
	public void render(Graphics g) {
		//Draw a black background
		g.setColor(Color.black);
		g.fillRect(0, 0, getGame().getDisplay().getInitialWidth(), getGame().getDisplay().getInitialHeight());
		
		//Render the game title in the centre of the screen
		g.setColor(Color.white);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 180));
		GraphicUtils.drawCenteredString(g, "Monopoly", 275);
		
		g.drawImage(image, 550, 350, 450, 650, null); //Draw the title image
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 60));
		
		boolean any = false; //Used to check if any of the buttons were hovered over to set the cursor
		for (TitleButton button : buttons) { //Loop through each button and render it
			if(button.isHovered()) any = true;
			button.render(g);
		}
		
		//If any of the buttons were hovered over, use the hand cursor. Otherwise, use the default cursor
		Cursor cursor = Cursor.getPredefinedCursor(any ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR);
		getGame().getDisplay().getRenderPane().setCursor(cursor);
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		for (TitleButton button : buttons) {
			button.mouseClick(mouseButton);
		}
	}
	
	@Override
	public void update(double delta) {}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		//Use a size of 1024x1024 pixels for the screen size, and use black as a background color
		getGame().getDisplay().resize(1024, 1024);
		getGame().getDisplay().setMaintainAspectRatio(true);
		getGame().getDisplay().setBackgroundColor(Color.black);
		
		//Start a fade in transition which lasts the FPS number of updates (so should last one second)
		Transition transition = new Transition();
		transition.fadeIn(getGame().getDisplay().getRenderFps());
		transition.startTransition();
	}
	
}
