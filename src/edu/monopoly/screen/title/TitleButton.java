package edu.monopoly.screen.title;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.sound.Sounds;

public class TitleButton {
	
	private int y, width, height;
	private String text; //Text for the button
	private Runnable onClick; //Code to run when the button is clicked
	
	public TitleButton(int y, int width, int height, String text, Runnable click) {
		this.y = y;
		this.width = width;
		this.height = height;
		this.text = text;
		this.onClick = click;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(0, y, width, height);
	}
	
	/**
	 * Should be called on a mouse click event
	 * @return Whether or not the button was clicked
	 */
	public boolean mouseClick(int button) {
		if (!isHovered()) return false; //The mouse isn't hovered over the button, so shouldn't be clicked
		
		//Release mouse button to prevent any code which only checks for mouse down to be run after the button is clicked
		getMouse().releaseAllButtons();
		
		//Play the click sound and run onClick if it isn't null
		Sounds.CLICK.play();
		if (onClick != null) onClick.run();
		return true;
	}
	
	public void render(Graphics g) {
		g.setColor(Color.red);
		
		//Render a round rectangle for the option button at -100 x so that the user only sees a rounded edge on the right hand side
		//If the button is being hovered, use the normal width + 50, and indent the button text
		if(isHovered()) {
			g.fillRoundRect(-100, y, width + 150, height, 150, 100);
			g.setColor(Color.white);
			g.drawString(getText(), 70, y + 70);
		}
		else {
			g.fillRoundRect(-100, y, width + 100, height, 150, 100);
			g.setColor(Color.white);
			g.drawString(getText(), 20, y + 70);
		}
	}
	
	private Mouse getMouse() {
		return Monopoly.getGameInstance().getDisplay().getMouse();
	}
	
	public boolean isHovered() {
		Mouse mouse = getMouse();
		Rectangle mouseBounds = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		return mouseBounds.intersects(getBounds());
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
}
