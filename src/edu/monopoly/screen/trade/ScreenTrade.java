package edu.monopoly.screen.trade;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.board.Board;
import edu.monopoly.graphic.CharacterSet;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.graphic.TextBox;
import edu.monopoly.player.ComputerPlayer;
import edu.monopoly.player.Player;
import edu.monopoly.property.ColoredProperty;
import edu.monopoly.property.PropertyColors;
import edu.monopoly.property.PurchasableProperty;
import edu.monopoly.screen.PropertyInfo;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenInfo;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.propertyview.MovableButton;
import edu.monopoly.screen.propertyview.ScreenPropertyView;
import edu.monopoly.util.ButtonUtils;
import edu.monopoly.util.GraphicUtils;
import edu.monopoly.util.NumberUtils;
import edu.monopoly.util.StringUtils;

public class ScreenTrade extends Screen {

	private ComputerPlayer tradePlayer; //Player to trade with
	private Board board;
	private ScreenPlay playScreen;
	private static int xOffset = 1030, yOffset = 240; //Selected property card offset to render at
	private Point movePoint, mousePoint; //Used to be able to move the property card
	private PurchasableProperty property; //Currently selected property to render as a card
	private MovableButton<Object> buttonEditProperty; //Either adds or removes the selected property to the trade
	
	private List<PurchasableProperty> playerTrades = new ArrayList<>(); //Properties the player is giving
	private List<PurchasableProperty> otherPlayerTrades = new ArrayList<>(); //Properties the player is receiving
	private OptionButton<Object> buttonTrade, buttonCancel; //Buttons to either trade or cancel the trade
	private Screen previousScreen; //Previous screen to return to on cancel
	private Map<Player, Point> playerInfoRenderPoints = new HashMap<>();
	private Point givePoint, givePoint2; //Location for where the boxes showing properties being traded are rendered
	private TextBox giveMoney, giveMoney2; //Text boxes allowing the player to enter the money to trade
	
	public ScreenTrade(Monopoly game, Board board, ScreenPlay playScreen, ComputerPlayer tradePlayer) {
		super(game);
		this.tradePlayer = tradePlayer;
		this.board = board;
		this.playScreen = playScreen;
	}

	@Override
	public void init() {
		buttonCancel = new OptionButton<>(getGame(), 47, 980, 350, 60, Color.red, Color.red, Color.white, "Cancel", new Object[] {});
		buttonCancel.setOnClick(() -> Screen.setSelectedScreen(previousScreen)); //When the cancel button is clicked, return to the previous screen
		
		buttonTrade = new OptionButton<>(getGame(), 443, 980, 350, 60, Color.red, Color.red, Color.white, "Trade", new Object[] {});
		buttonTrade.setOnClick(this::tradeClicked); //When the trade button is clicked, call the tradeClicked method
		
		playerInfoRenderPoints.put(board.getHumanPlayer(), new Point(40, 170)); //Render information about the player at 40, 170
		playerInfoRenderPoints.put(tradePlayer, new Point(460, 170)); //Render information about the player trading with at 460, 170
		
		//Points where the properties being traded are rendered
		givePoint = new Point(40, 550);
		givePoint2 = new Point(460, 550);
		
		buttonEditProperty = new MovableButton<>(getGame(), 370, 500, 310, 50, PropertyColors.LIGHT_BLUE.getColor(), PropertyColors.LIGHT_BLUE.getColor(), Color.black, "Remove from Trade", new Object[] {});
		buttonEditProperty.setOnClick(this::editButtonClicked); //When the button is clicked, call the editButtonClicked method
		
		//Only allow numbers (0 to 9)
		List<Character> chars = new ArrayList<>();
		for(char c = '0'; c <= '9'; c++) chars.add(c);
		
		//Create a text box for each money input, limit them both to only numbers and a maximum of 5 characters
		giveMoney = new TextBox(70, 830, 320, 60, Color.black, Color.white, CharacterSet.loadSet(chars));
		giveMoney.setCharacterLimit(5);
		giveMoney.setSelected(false);
		
		giveMoney2 = new TextBox(490, 830, 320, 60, Color.black, Color.white, CharacterSet.loadSet(chars));
		giveMoney2.setCharacterLimit(5);
		giveMoney2.setSelected(false); 
	}
	
	/**
	 * Called when the button to trade is clicked. Will validate the trade with the AI and show a message to the user telling them
	 * the result of the trade
	 */
	private void tradeClicked() {
		Trade playerTrade = getPlayerTrade();
		Trade otherTrade = getOtherTrade();
		
		//Verify the trade with the trading player's AI. True means accepted, false means rejected
		boolean result = tradePlayer.verifyTrade(board.getHumanPlayer(), playerTrade, otherTrade, board, true);
		if(!result) {
			//If the trade has been declined, show a message to the user telling them this.
			Screen.showConfirmMessage(tradePlayer.getName() + " has\ndeclined your offer.", null);
		}
		else {
			//Otherwise, the trade has been accepted, tell the user the result, then:
			Screen.showConfirmMessage(tradePlayer.getName() + " has\naccepted your offer!", () -> {
				//Apply the trade, giving the appropiate amount of money to each player then changing the ownership of the properties.
				Player human = board.getCurrentPlayer();
				human.setMoney(human.getMoney() - playerTrade.getMoney());
				human.setMoney(human.getMoney() + otherTrade.getMoney());
				
				tradePlayer.setMoney(tradePlayer.getMoney() - otherTrade.getMoney());
				tradePlayer.setMoney(tradePlayer.getMoney() + playerTrade.getMoney());
				
				for(PurchasableProperty prop : playerTrade.getProperties()) board.setOwner(prop, tradePlayer);
				for(PurchasableProperty prop : otherTrade.getProperties()) board.setOwner(prop, human);
				Screen.setSelectedScreen(previousScreen); //Finally return to the previous screen
			});
		}
	}
	
	/**
	 * Called when the button on the selected property is clicked. Will either add or remove the selected property to the trade
	 */
	private void editButtonClicked() {
		if(playerTrades.contains(property) || otherPlayerTrades.contains(property)) {
			 //Remove the property from list to trade and deselect it
			playerTrades.remove(property);
			otherPlayerTrades.remove(property);
			property = null;
		} else {
			//Otherwise, the property isn't being traded. Add it to the correct trade depending on who owns it
			if(board.getHumanPlayer().getProperties().contains(property)) playerTrades.add(property);
			if(tradePlayer.getProperties().contains(property)) otherPlayerTrades.add(property);
		}
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, getGame().getDisplay().getInitialWidth(), getGame().getDisplay().getInitialHeight()); //Draw a black background
		
		g.setColor(Color.white);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 40));
		GraphicUtils.drawCenteredString(g, "Trade for Properties", 75, 420); //Draw the title text
		
		//Draw text next to the text boxes to enter money to trade
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 35));
		g.drawString("You give:", 42, 535);
		g.drawString(tradePlayer.getName() + " Gives:", 462, 535);
		
		//Translate and scale the board, then render it so it is rendered on the right hand side of the screen
		float newScale = 1080f / 1920f;
		float boardXOffset = (1080f / 1920f) + 840;
		float boardYOffset = 1080f / 1920f;
		
		((Graphics2D) g).translate(boardXOffset, boardYOffset);
		((Graphics2D) g).scale(newScale, newScale);
		board.render(g);
		((Graphics2D) g).scale(1f/newScale, 1f/newScale);
		((Graphics2D) g).translate(-boardXOffset, -boardYOffset);
		
		if(property != null) { //If there is a selected card, render a property card for it over the top of the board
			g.setColor(Color.black);
			g.fillRect(xOffset - 6, yOffset - 6, 712, 612);
			ScreenPropertyView.renderPropertyCard(g, xOffset, yOffset, property, board);
			buttonEditProperty.render(g);
		}
		
		//Render the buttons
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 40));
		buttonTrade.render(g);
		buttonCancel.render(g);
		
		//Render the information boxes for the players
		renderPlayerInfo(g, board.getHumanPlayer(), playerInfoRenderPoints.get(board.getHumanPlayer()), 285);
		renderPlayerInfo(g, tradePlayer, playerInfoRenderPoints.get(tradePlayer), 285);

		int indent = 8; //Temporary variable used to calculate where to render the borders
		
		//Render the border for the properties being traded box for the player
		g.setColor(board.getHumanPlayer().getColor());
		g.fillRect(givePoint.x - indent, givePoint.y - indent, 340 + (indent * 2), 235 + (indent * 2));
		
		//Render the box for the properties being traded for the player
		g.setColor(Color.white);
		g.fillRect(givePoint.x, givePoint.y, 340, 235);
		playScreen.renderPropertyList(g, playerTrades, givePoint.x, givePoint.y - 70);
		
		//Render the border for the properties being traded box for the other player
		g.setColor(tradePlayer.getColor());
		g.fillRect(givePoint2.x - indent, givePoint2.y - indent, 340 + (indent * 2), 235 + (indent * 2));
		
		//Render the box for the properties being traded by the other player
		g.setColor(Color.white);
		g.fillRect(givePoint2.x, givePoint2.y, 340, 235);
		playScreen.renderPropertyList(g, otherPlayerTrades, givePoint2.x, givePoint2.y - 70);
		
		//Render the text boxes
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 55));
		giveMoney.render(g);
		giveMoney2.render(g);
		g.setColor(Color.white);
		g.drawString(StringUtils.POUND_SIGN + "", 35, 885);
		g.drawString(StringUtils.POUND_SIGN + "", 455, 885);
		
		//Draw the text for the value of each player's trade
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 36));
		GraphicUtils.drawCenteredString(g, "Trade Value: " + StringUtils.POUND_SIGN + getPlayerTrade().getEstimatedValue(board.getHumanPlayer()), 940, 222);
		GraphicUtils.drawCenteredString(g, "Trade Value: " + StringUtils.POUND_SIGN + getOtherTrade().getEstimatedValue(tradePlayer), 940, 618);
	}
	
	private Trade getPlayerTrade() {
		//return the playerTrades, with 0 money if the text box is blank, or the value in the text box
		return new Trade(playerTrades, NumberUtils.isInt(giveMoney.getText()) ? Integer.parseInt(giveMoney.getText()) : 0);
	}
	
	private Trade getOtherTrade() {
		//return the otherPlayerTrades, with 0 money if the text box is blank, or the value in the text box
		return new Trade(otherPlayerTrades, NumberUtils.isInt(giveMoney2.getText()) ? Integer.parseInt(giveMoney2.getText()) : 0);
	}
	
	/**
	 * Render player information on the screen, showing money and properties which are not being traded.
	 * This is mostly a copy-paste from the Play Screen, but with some modifications, which only shows
	 * properties which the player owns and isn't trading
	 * @return The property which is hovered, or null if none
	 */
	public PurchasableProperty renderPlayerInfo(Graphics g, Player player, Point p, int height) {
		int x = p.x;
		int y = p.y;
		
		//Draw the border using the player's colour
		int indent = 8;
		g.setColor(player.getColor());
		g.fillRect(x - indent, y - indent, 340 + (indent * 2), height + (indent * 2));
		
		g.setColor(Color.white);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 40));
		g.drawString(player.getName(), x + 2, y - (g.getFont().getSize() / 3)); //Draw the player's name
		g.fillRect(x, y, 340, height);
		int size = 75;
		GraphicUtils.renderMaintainAspect(g, player.getPiece().getIcon(), x + (340 - size), y - size, size, size); //Draw the player's piece icon
		
		g.setColor(Color.black);
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 30));
		g.drawString("Cash:  " + StringUtils.POUND_SIGN + player.getMoney(), x + 10, y + 35); //Draw the amount of money the player has
		
		g.drawString("Properties:", x + 10, y + 70); //Draw the label above the list of properties
		
		//Create a list containing all the properties the player owns, then loop through removing any properties that are being traded.
		//This is because if a property is being traded then the player should not be able to see it in the top information box,
		//instead they should see it in the list of properties being traded box
		List<PurchasableProperty> properties = new ArrayList<>(player.getProperties());
		Iterator<PurchasableProperty> it = properties.iterator();
		while(it.hasNext()) {
			PurchasableProperty prop = it.next();
			if(playerTrades.contains(prop) || otherPlayerTrades.contains(prop)) it.remove();
		}
		
		return playScreen.renderPropertyList(g, properties, x, y); //Render the list of properties which aren't being traded
	}
	
	@Override
	public void update(double delta) {
		Mouse mouse = getGame().getDisplay().getMouse();
		if(property != null && movePoint != null && mousePoint != null && property != null) { //If current property info card is being dragged
			int xDiff = (mouse.getX() - mousePoint.x);
			int yDiff = (mouse.getY() - mousePoint.y);
			//Offset the card by the amount moved by the mouse
			xOffset = movePoint.x + xDiff;
			yOffset = movePoint.y + yDiff;
			
			//Limit the offsets to ensure it doesn't go out of bounds
			if(xOffset < 846) xOffset = 846;
			if(xOffset + 706 > 1920) xOffset = 1920 - 706;
			
			if(yOffset < 6) yOffset = 6;
			if(yOffset + 606 > 1080) yOffset = 1080 - 606;
		}
		
		if(property != null) { //If property info is being rendered
			buttonEditProperty.setOffSet(xOffset, yOffset); //Updates the position of the button
			
			//If the property is being traded, set its text so that it can be Removed from the Trade
			//Otherwise, change it text so it can be Added to a Trade
			if(playerTrades.contains(property) || otherPlayerTrades.contains(property)) buttonEditProperty.setText("Remove from Trade");
			else buttonEditProperty.setText("Add to Trade");
		}
		
		//Update the text boxes where money is inputed
		giveMoney.update(); 
		giveMoney2.update();
		
		String problem = null;
		//Gets the int value of the text in the text boxes, or 0 if there isn't a value
		int money1 = NumberUtils.isInt(giveMoney.getText()) ? Integer.parseInt(giveMoney.getText()) : 0;
		int money2 = NumberUtils.isInt(giveMoney2.getText()) ? Integer.parseInt(giveMoney2.getText()) : 0;
		
		//If no money is specified and there are no trades
		if(money1 == 0 && money2 == 0 && playerTrades.isEmpty() && otherPlayerTrades.isEmpty()) {
			problem = "Select something to trade";
		}
		
		//A list of the property colours which have buildings on them and are being traded
		Set<PropertyColors> built = new HashSet<>();
		for(ColoredProperty prop : board.getHumanPlayer().getBuildings()) {
			if(playerTrades.contains(prop)) built.add(prop.getColor());
		}
		for(ColoredProperty prop : tradePlayer.getBuildings()) {
			if(otherPlayerTrades.contains(prop)) built.add(prop.getColor());
		}
		
		//Loop through all the property colours which have buildings on them
		for(PropertyColors color : built) {
			List<PurchasableProperty> required = board.getProperties(color);
			boolean b1 = playerTrades.containsAll(required);
			boolean b2 = otherPlayerTrades.containsAll(required);
			
			//If the neither trade has all of the properties required, tell the user they need all the current colour to trade
			if(!b1 && !b2) {
				problem = "Need all " + StringUtils.format(color.name().replace("_", " ") + " properties");
				break;
			}
		}
		//If the money to trade is larger than the money the player has
		if(money1 > board.getHumanPlayer().getMoney()) problem = "Not enough money";
		else if(money2 > tradePlayer.getMoney()) problem = "Not enough money";
		
		//Enable the trade button if the problem is still null
		ButtonUtils.setEnabled(buttonTrade, Color.red, Color.gray, problem, problem == null);
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		//Set the previous screen to return to as long as it isn't the info Screen
		//This is because this is the screen which is shown to either accept or deny the trade
		//And shouldn't be returned to.
		if(!(lastScreen instanceof ScreenInfo)) previousScreen = lastScreen;
	}
	
	@Override
	public void screenExited(Screen nextScreen) {
		//Remove property overlay on the board
		board.setOverlay(null);
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		//Forward the key press to the text boxes
		giveMoney.keyPress(key, ch);
		giveMoney2.keyPress(key, ch);
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		
		//If there is a property card being shown
		if(property != null) {
			Rectangle cardRect = new Rectangle(xOffset, yOffset, 700, 600);
			
			//If the mouse in inside the card bounding box, set the mouse and move points to allow the
			//Card to be moved
			if(cardRect.intersects(mouseRect)) {
				movePoint = new Point(xOffset, yOffset);
				mousePoint = new Point(mouse.getX(), mouse.getY());
			} 
			//Forward the click event to the button
			buttonEditProperty.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		}
		
		//Forward the click event to the trade and cancel buttons
		buttonTrade.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		buttonCancel.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		
		PurchasableProperty clicked = null;
		
		//Create a temporary array containing the two players to loop through them. Same operation is
		//done for each of the two players, so this just cuts down on the amount of code needed.
		for(Player p : new Player[] {board.getHumanPlayer(), tradePlayer}) {
			int x = playerInfoRenderPoints.get(p).x;
			int y = playerInfoRenderPoints.get(p).y;
			
			//Clone the array of the properties the player owns (copied the references not the obejcts)
			List<PurchasableProperty> properties = new ArrayList<>(p.getProperties());
			Iterator<PurchasableProperty> it = properties.iterator();
			//Loop through each property and remove it from the list if it is being traded
			while(it.hasNext()) {
				PurchasableProperty prop = it.next();
				if(playerTrades.contains(prop) || otherPlayerTrades.contains(prop)) it.remove();
			}
			
			//Get the locations where the remaining properties which aren't being traded are being rendered
			//Loop through and if the mouse is in the bounding box, set the selected property
			Map<Rectangle, PropertyInfo> propInfo = playScreen.getPropertyInfoLocations(properties, x, y);
			for(Rectangle r : propInfo.keySet()) {
				PropertyInfo prop = propInfo.get(r);
				if(mouseRect.intersects(r)) {
					clicked = prop.getProperty();
					break;
				}
			}
			
			//Loop through the locations where the properties being traded are being rendered
			//Do the same as the above again, looping through and checking if the mouse interesects the bounding box
			List<PurchasableProperty> props = (p == tradePlayer ? otherPlayerTrades : playerTrades);
			Point point = (p == tradePlayer ? givePoint2 : givePoint);
			
			propInfo = playScreen.getPropertyInfoLocations(new ArrayList<>(props), point.x, point.y - 70);
			for(Rectangle r : propInfo.keySet()) {
				PropertyInfo prop = propInfo.get(r);
				if(mouseRect.intersects(r)) {
					clicked = prop.getProperty();
					break;
				}
			}
		}
		
		//If a property has been clicked, set the currently displayed property to the one clicked
		if(clicked != null) property = clicked;
		
		//Forward the mouse click event to the two text boxes
		giveMoney.mouseClick(mouseButton);
		giveMoney2.mouseClick(mouseButton);
	}
	
	public PurchasableProperty getSelectedProperty() {
		return property;
	}

	public void setSelectedProperty(PurchasableProperty property) {
		this.property = property;
	}
	
	/**
	 * Adds the property to the list of properties the player is trading.
	 */
	public void addTrade(PurchasableProperty property) {
		if(!board.getHumanPlayer().getProperties().contains(property) || playerTrades.contains(property)) return;
		playerTrades.add(property);
	}
	
	/**
	 * Adds the property to the list of properties the player trading with is trading
	 */
	public void addOtherTrade(PurchasableProperty property) {
		if(!tradePlayer.getProperties().contains(property) || otherPlayerTrades.contains(property)) return;
		otherPlayerTrades.add(property);
	}

	public List<PurchasableProperty> getPlayerTrades() {
		return playerTrades;
	}

	public List<PurchasableProperty> getOtherPlayerTrades() {
		return otherPlayerTrades;
	}
	
	@Override
	public void mouseReleased(int mouseButton) {
		//Resets the mouse moving points to stop the property card being dragged
		movePoint = null;
		mousePoint = null;
	}
}
