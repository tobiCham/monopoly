package edu.monopoly.screen.trade;

import java.util.ArrayList;
import java.util.List;

import edu.monopoly.player.Player;
import edu.monopoly.property.ColoredProperty;
import edu.monopoly.property.PurchasableProperty;

public class Trade {

	private List<PurchasableProperty> properties = new ArrayList<>(); //List of properties which are being traded
	private int money; //Money being sent in the trade

	public Trade(List<PurchasableProperty> props, int money) {
		this.money = money;
		this.properties.addAll(props);
	}

	public List<PurchasableProperty> getProperties() {
		return properties;
	}

	public int getMoney() {
		return money;
	}
	
	/**
	 * @param player Player who owns the properties in the trade
	 * @return An estimated value for the trade, taking in account if any of the properties have buildings or are mortgaged
	 */
	public int getEstimatedValue(Player player) {
		int money = this.money;
		
		//Loop through each property in the list and add on a calculated value to the total
		for(PurchasableProperty prop : properties) { 
			if(prop.isMortgaged()) money += prop.getMortgageValue();
			else {
				money += prop.getCost(); //Add the cost of the property
				if(prop instanceof ColoredProperty && player.hasBuildings((ColoredProperty) prop)) {
					//If the property has buildings on it, also add on the cost of the buildings
					int numb = player.getBuildings((ColoredProperty) prop);
					money += ((ColoredProperty) prop).getBuildingCost() * numb;
				} 
			}
		}
		return money;
	}
	
	//Equals and hashCode methods so that the class works properly in a HashMap.
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Trade)) return false;
		Trade trade = (Trade) obj;
		return trade.getProperties().equals(properties) && trade.getMoney() == money;
	}
	
	@Override
	public int hashCode() {
		int total = 0;
		for(PurchasableProperty prop : properties) total += prop.getValue();
		total += money;
		return total;
	}
}
