package edu.monopoly.screen.win;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Confetti {
	
	private float x, y;
	private float dx, dy;
	private Color color;
	
	private static Random random = new Random(); //Random generator for the colour
	
	public Confetti(int x, int y, Color color) {
		this.x = x;
		this.y = y;
		this.color = color;
		this.dx = 0;
		this.dy = 7; //Falling at 7 pixels per update
	}
	
	public Confetti(int x, int y) {
		//Call the constructor with a random colour
		this(x, y, new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
	}
	
	public void update() {
		//Move the confetti position
		x += dx;
		y += dy;
	}
	
	public void render(Graphics g) {
		//Set the colour and draw a rectangle with size 8x8 centered around the x and y position
		g.setColor(color);
		g.fillRect((int) (x - 4), (int) (y - 4), 8, 8);
	}

	
	//--Getter and setter methods--
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getDx() {
		return dx;
	}

	public void setDx(float dx) {
		this.dx = dx;
	}

	public float getDy() {
		return dy;
	}

	public void setDy(float dy) {
		this.dy = dy;
	}

	public Color getColor() {
		return color;
	}
}
