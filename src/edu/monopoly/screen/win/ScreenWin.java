package edu.monopoly.screen.win;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.tobi.guibase.Mouse;

import edu.monopoly.Monopoly;
import edu.monopoly.graphic.OptionButton;
import edu.monopoly.screen.Screen;
import edu.monopoly.screen.ScreenPlay;
import edu.monopoly.screen.Screens;
import edu.monopoly.util.FileUtils;
import edu.monopoly.util.GraphicUtils;

//Win and Lose screen
public class ScreenWin extends Screen {

	private ScreenPlay screen;
	private List<Confetti> confetti = new ArrayList<>(); //List of confetti objects which are being rendered
	private Random rand = new Random(); //Used generate random positions for the confetti
	private float time = 0; //Time in, max value is maxTime
	private float maxTime = 0; //Time before the text and button is shown
	private OptionButton<Object> buttonBack; //Button to return to the main menu and delete the save file
	private String message; //Message to the user depending on whether they win or lose
	
	public ScreenWin(Monopoly game, ScreenPlay screen) {
		super(game);
		this.screen = screen;
		
		//If the player hasn't resigned, then they won the game. Set the message to a congratulations message.
		if(!screen.getBoard().getHumanPlayer().hasResigned()) {
			this.message = "Congratulations you\nwon the game!";
			maxTime = 7.5f; //7.5 seconds of confetti before message is shown
		}
		else {
			this.message = "You lost the game.";
			maxTime = 3; //2 seconds before message is shown
		}
		message += "\n\nClick the button below to return to the main menu where you can load an existing game or start a new game.";
	}

	@Override
	public void init() {
		time = 0;
		confetti.clear();
		
		buttonBack = new OptionButton<>(getGame(), 723, 700, 475, 75, Color.red, Color.red, Color.white, "Back to Main Menu", new Object[] {});
		buttonBack.setOnClick(() -> {
			//When the back button is clicked, delete the save file so the player can't load it again, then return to the main menu
			String name = screen.getSaveName();
			File file = new File(FileUtils.getGameDirectory(), "saves/" + name + ".dat");
			file.delete();
			Screen.setSelectedScreen(Screens.TITLE);
		});
	}

	@Override
	public void render(Graphics g) {
		screen.render(g); //Draw the previous screen as a background
		if(isWin()) { //If the user has won, render all the confetti in the list
			//Clone the list and render each confetti. (Only clones the referencs to the confetti objects)
			//Avoids concurrent modification except as the render and update methods both use the list and run in different threads
			List<Confetti> list = new ArrayList<>(confetti);
			for(Confetti conf:list) {
				if(conf != null) conf.render(g);
			}
		} else {
			//Otherwise the user has lost. Set the alpha value 
			float percentage = time / maxTime;
			
			//Limits the percentage to between 0 and 1
			if(percentage < 0) percentage = 0;
			if(percentage > 1) percentage = 1;
			
			//Transparency is the percentage through the animation * 50. (50 is the maximum transparency)
			int alpha = (int) (percentage * 50f);
			g.setColor(new Color(255, 0, 0, alpha));
			
			////Draw a red rectangle over the background.
			//Will have the effect of slowly turning the background (previous screen) red
			g.fillRect(0, 0, 1920, 1080);
		}
		
		if(time < maxTime) return;
		//Only run the code below if the maxTime has elapsed
		
		int width = 550;
		int height = 550;
		
		//Draw a box with a black outline
		g.setColor(Color.black);
		g.fillRect((1920 / 2) - (width / 2) - 5, (1080 / 2) - (height / 2) - 5, width + 10, height + 10);
		g.setColor(Color.white);
		g.fillRect((1920 / 2) - (width / 2), (1080 / 2) - (height / 2), width, height);
		
		g.setColor(Color.black);
		GraphicUtils.renderText(g, message, getGame().getDisplay().getInitialWidth() / 2, 330, 30, 48);
		
		buttonBack.render(g);
	}

	@Override
	public void update(double delta) {
		if(isWin()) {
			for(int i = 0; i < 8; i++) {
				//Add 8 pieces of confetti onto the screen each update
				confetti.add(new Confetti(rand.nextInt(getGame().getDisplay().getInitialWidth()), 10));
			}
			Iterator<Confetti> it = confetti.iterator();
			while(it.hasNext()) {
				//Loop through each piece of confetti and update it. If it has gone off the screen remove it from the list
				Confetti conf = it.next();
				conf.update();
				if(conf.getY() > getGame().getDisplay().getInitialHeight() + 10) it.remove();
			}
		}
		
		if(time < maxTime) time += (float) (delta / 1000000000f);
		else time = maxTime;
	}
	
	/**
	 * @return Whether the screen is a win or lose screen
	 */
	private boolean isWin() {
		return maxTime > 3;
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getGame().getDisplay().getMouse();
		//If the animation time has elapsed, forward the mouse click onto the button
		if(time >= maxTime) buttonBack.mouseClick(mouseButton, mouse.getX(), mouse.getY());
	}

}
