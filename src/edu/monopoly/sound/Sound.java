package edu.monopoly.sound;

//Methods which all implementations of Sound must contain. For this game, the only implementation is SoundClip.
public interface Sound {
	
	void play();
	void stop();
	void pause();
	
	/**
	 * @param vol A value between 0 and 1. 0 being no volume and 1 being maximum volume
	 */
	void setVolume(float vol);
	
	/**
	 * @return The volume, in a range of 0 and 1. 0 being no volume and 1 being maximum volume
	 */
	float getVolume();
	
	void setPosition(int framePosition);
	
	/**
	 * @return The current play position, in frames
	 */
	int getPosition();
	
	/**
	 * @return The length of the sound, in frames
	 */
	int getLength();
	
	boolean isPlaying();
	
	void setPlayable(boolean enabled);
	boolean isPlayable();
}
