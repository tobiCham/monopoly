package edu.monopoly.sound;

import java.net.URL;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

//Wrapper class for Clip which implements Sound
public class SoundClip implements Sound {
	
	private Clip clip; //The underlying java clip used to play the sound
	private int point = 0; //Frame position to resume from if the sound has been paused
	private boolean loaded = false;
	private boolean playable = true;
	private boolean music;
	
	/**
	 * @param url URL to load the sound from
	 * @param music True if it is music, false if it is a sound effect
	 */
	public SoundClip(URL url, boolean music) {
		this.music = music;
		//Attempt to load music, or print an error if it fails
		try {
			if(url == null) {
				System.err.println("Failed to load sound effect. Unknown URL");
				return;
			}
			clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(url));
			loaded = true;
		} catch (Exception e) {
			System.err.println("Failed to load sound effect " + url + ": " + e.getMessage());
		}
	}
	
	@Override
	public void play() {
		if (!loaded || !playable) return;
		//Set the current playback position to the previously paused point.
		clip.setFramePosition(point);
		clip.start();
	}
	
	@Override
	public void stop() {
		if (!loaded || !playable) return;
		//Resets the playback position to 0 and stops the playback
		clip.setFramePosition(0);
		point = 0;
		clip.stop();
	}
	
	@Override
	public void pause() {
		if (!loaded || !playable) return;
		//Sets the point to the current frame position of the sound, then stops playback
		point = this.clip.getFramePosition();
		clip.stop();
	}
	
	@Override
	public void setVolume(float vol) {
		if (!loaded || !playable) return;
		//Limit value to between 0 and 1
		if(vol > 1) vol = 1;
		if(vol < 0) vol = 0;
		
		FloatControl gainControl = (FloatControl) this.clip.getControl(FloatControl.Type.MASTER_GAIN);
		
		//Due to how Java handles volume, the range of values is between, seemingly, -80 and about 6, and so it isn't a straightforward scale
		//Multiply the volume by the range of values, and add on the minimum value to convert
		vol = vol * (gainControl.getMaximum() - gainControl.getMinimum());
		gainControl.setValue(vol + gainControl.getMinimum());
	}
	
	@Override
	public float getVolume() {
		if (!loaded || !playable) return 0f;
		FloatControl gainControl = (FloatControl) this.clip.getControl(FloatControl.Type.MASTER_GAIN);
		
		//Due to how Java handles volume, the range of values is between, seemingly, -80 and about 6, and so it isn't a straightforward scale
		//To convert from the gain system to a value between 0 and 1, subtract the initial volume and then multiply by the range of values.
		float currentVol = gainControl.getValue() - gainControl.getMinimum();
		currentVol = currentVol / (gainControl.getMaximum() - gainControl.getMinimum());
		return currentVol;
	}
	
	@Override
	public void setPosition(int framePosition) {
		if (!loaded || !playable) return;
		clip.setFramePosition(framePosition);
	}
	
	@Override
	public int getPosition() {
		if (!loaded) return 0;
		return clip.getFramePosition();
	}
	
	@Override
	public int getLength() {
		if(!loaded) return 0;
		return clip.getFrameLength();
	}
	
	@Override
	public boolean isPlaying() {
		return loaded && playable && clip.isRunning();
	}
	
	/**
	 * @return The underlying clip used to play the sound
	 */
	public Clip getClip() {
		return clip;
	}
	
	public void setLoop(boolean loop) {
		boolean playing = isPlaying();
		//If should constantly loop, set the number of loops to the maximum value an integer can take. (This is 2^31 - 1)
		clip.loop(loop ? Integer.MAX_VALUE : 0);
		if(!playing) clip.stop();
	}
	
	@Override
	public void setPlayable(boolean enabled) {
		this.playable = enabled;
	}
	
	@Override
	public boolean isPlayable() {
		return playable;
	}
	
	public boolean hasLoaded() {
		return loaded;
	}
	
	public boolean isMusic() {
		return music;
	}
}
