package edu.monopoly.sound;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class SoundOptions {

	/**
	 * Enables or disables all the sound effects defined in the Sounds class
	 */
	public static void setSoundEnabled(boolean enabled) {
		for(SoundClip clip : getClips()) {
			if(clip.isMusic()) continue;
			//If the sound clip should be disabled and it is currently playing, stop playback
			if(!enabled && clip.isPlaying()) clip.stop();
			clip.setPlayable(enabled);
		}
	}
	
	/**
	 * Enables or disables all the music defined in the Sounds class
	 */
	public static void setMusicEnabled(boolean enabled) {
		for(SoundClip clip : getClips()) {
			if(!clip.isMusic()) continue;
			//If the music should be disabled and it is currently playing, stop playback
			if(!enabled && clip.isPlaying()) clip.stop();
			clip.setPlayable(enabled);
		}
	}
	
	/**
	 * @return A list of all the sound clips defined in the Sounds class
	 */
	private static Set<SoundClip> getClips() {
		Set<SoundClip> clips = new HashSet<>();
		//Use Java's reflection API to loop through all the fields in the class.
		Field[] fields = Sounds.class.getDeclaredFields();
		for(Field field : fields) {
			try {
				//If the field is of type SoundClip, attempt to get its value and add it to the set
				if(field.getType().isAssignableFrom(SoundClip.class)) {
					SoundClip clip = (SoundClip) field.get(null);
					if(clip != null) clips.add(clip);
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return clips;
	}
}
