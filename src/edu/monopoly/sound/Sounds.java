package edu.monopoly.sound;

import edu.monopoly.util.SoundLoader;

//Class holding all the sound effects used in the game.
public class Sounds {

	public static SoundClip BACKGROUND_MUSIC;
	public static SoundClip CLICK, SIREN;
	public static SoundClip KEY1, KEY2, KEY3;
	
	public static void loadSounds() {
		CLICK = SoundLoader.loadSound("click.wav");
		SIREN = SoundLoader.loadSound("siren.wav");
		
		//Key press sound effects
		KEY1 = SoundLoader.loadSound("key1.wav");
		KEY2 = SoundLoader.loadSound("key2.wav");
		KEY3 = SoundLoader.loadSound("key3.wav");
	}
	
	public static void loadMusic() {
		BACKGROUND_MUSIC = SoundLoader.loadMusic("background.wav");
		BACKGROUND_MUSIC.setLoop(true);
	}
}
