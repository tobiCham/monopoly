package edu.monopoly.util;

import java.awt.Color;

import edu.monopoly.graphic.OptionButton;

public class ButtonUtils {

	/**
	 * Sets a button to enabled or disabled
	 */
	public static void setEnabled(OptionButton<?> button, Color enabledColor, Color disabledColor, boolean enabled) {
		if(enabled) {
			button.setEdgeColor(enabledColor);
			button.setInsideColor(enabledColor);
		} else {
			button.setEdgeColor(disabledColor);
			button.setInsideColor(disabledColor);
		}
		button.setEnabled(enabled);
	}
	
	/**
	 * @param disabledReason Hover text to show if the button is disabled
	 * Sets a button to enabled or disabled
	 */
	public static void setEnabled(OptionButton<?> button, Color enabledColor, Color disabledColor, String disabledReason, boolean enabled) {
		if(enabled) {
			button.setHoverText(null);
			button.setEdgeColor(enabledColor);
			button.setInsideColor(enabledColor);
		} else {
			button.setHoverText(disabledReason);
			button.setEdgeColor(disabledColor);
			button.setInsideColor(disabledColor);
		}
		button.setEnabled(enabled);
	}

}
