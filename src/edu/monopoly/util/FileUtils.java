package edu.monopoly.util;

import java.io.File;

public class FileUtils {

	/**
	 * @return The file for the game directory.</br>
	 * For windows:
	 * Appdata/Roaming/Edu-Monopoly
	 * </br>
	 * For Mac or Linux:
	 * Library/Application Support/Edu-Monopoly
	 */
	public static File getGameDirectory() {
		File saveFolder;
		
		String os = System.getProperty("os.name");
		if (os.toLowerCase().contains("win")) { //OS is windows, use AppData/Roaming
			saveFolder = new File(System.getenv("AppData"));
		} else { //OS is Linux or Mac, use Library/Application Support
			File userHome = new File(System.getProperty("user.home"));
			saveFolder = new File(userHome, "Library/Application Support");
		}
		
		saveFolder = new File(saveFolder, "Edu-Monopoly"); //Edu-Monopoly folder
		
		return saveFolder;
	}

	/**
	 * @return The name of the file without the extension
	 */
	public static String getName(File file) {
		String name = file.getName();
		int lastIndex = name.lastIndexOf("."); //Index of the final '.' in the file name
		//If the index is less than 0, no file extension, return the name. 
		//Otherwise, return a substring of the name up the last index
		return lastIndex == -1 ? name : name.substring(0, lastIndex); 
	}
	
}
