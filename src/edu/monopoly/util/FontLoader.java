package edu.monopoly.util;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.net.URL;

import edu.monopoly.Monopoly;

public class FontLoader {

	/**
	 * Loads a font from the resources by name
	 */
	public static Font loadFont(String name) {
		try {
			URL fontUrl = Monopoly.class.getResource("/font/" + name);
			Font font = Font.createFont(Font.TRUETYPE_FONT, fontUrl.openStream());
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font); //Registers the font so the application can use it
			return new Font(font.getName(), font.getStyle(), 12);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
