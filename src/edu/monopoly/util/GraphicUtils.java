package edu.monopoly.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import edu.monopoly.Monopoly;

public class GraphicUtils {
	
	/**
	 * Draws and image but maintains the aspect ratio of the image. Will centre the image in the bounds specified.
	 */
	public static void renderMaintainAspect(Graphics g, Image image, int x, int y, int width, int height) {
		double scaleFactor = (float) width / image.getWidth(null); //The multiplier to scale the image by
		
		int newHeight = (int) (image.getHeight(null) * scaleFactor); //The new height of the image
		
		//If the newHeight calculated is larger than the height specified, then the image of this size would not fit into the bounds.
		//Therefore, instead the scale factor should be the height specified divided by the image size.
		if (newHeight > height) scaleFactor = (float) height / image.getHeight(null); 
		
		//Calculate the new width and height for the image to render
		int newWidth = (int) (scaleFactor * image.getWidth(null));
		newHeight = (int) (scaleFactor * image.getHeight(null));

		//Calculate the indents so that the image is centered
		int renderX = (width / 2) - (newWidth / 2); 
		int renderY = (height / 2) - (newHeight / 2);

		g.drawImage(image, x + renderX, y + renderY, newWidth, newHeight, null); //Draw the image onto the screen, resizing and indenting to fit the area specified.
	}
	
	/**
	 * @param centerX Centre x coordinate to render each line of text at
	 * @param startY The top of where the text is being rendered
	 * @param maxLength The maximum length, in characters, for each line
	 * @param textGap The number of pixels between each line of text
	 * Renders the text in the position specified downwards. Will move onto new lines if a line exceeds the maxLength number of characters
	 */
	public static void renderText(Graphics g, String text, int centerX, int startY, int maxLength, int textGap) {
		StringBuilder builder = new StringBuilder();

		//Loop through each sub string split at a new line
		for(String newMessage : text.split("\n")) {
			while(true) {
				//If the current line is longer than the maximum length allowed
				if(newMessage.length() > maxLength) {
					if(!newMessage.contains(" ")) {
						//If there isn't a space to split at, simply slit the string at the furthest point along the string. Add the first part to the 
						//StringBuilder which will show the text, and update the current message to remove the substring
						builder.append(newMessage.substring(0, maxLength)).append("\n");
						newMessage = newMessage.substring(maxLength, newMessage.length());
					} else {
						//Otherwise, there is a space. Get the index of the furthest space in the string which isn't past the maximum length
						//Add the substring to the StringBuilder which will show the text, and update the current message to remove the substring
						int index = newMessage.lastIndexOf(" ", maxLength);
						builder.append(newMessage.substring(0, index)).append("\n");
						newMessage = newMessage.substring(index, newMessage.length());
					}
				} else {
					//Otherwise, the message fits so simply add it to the builder
					builder.append(newMessage);
					break;
				}
			}
			//Add a new line after each line
			builder.append("\n");
		}
		
		//Loop through all the different lines in the string to display, and draw each line moving down the page in the centre of the x-coordinate
		String[] parts = builder.toString().split("\n");
		for(int i = 0; i < parts.length; i++) {
			String part = parts[i];
			GraphicUtils.drawCenteredString(g, part, startY + (i * textGap), centerX);
		}
	}

	/**
	 * Draws a textbox graphic with text, using the colors specified.
	 */
	public static void drawTextBox(Graphics g, String text, int x, int y, Color textColor, Color boxColor) {
		//Temporary variables to hold the old font and color so it can be restored after drawing the textbox
		Font originalFont = g.getFont();
		Color originalColor = g.getColor();

		
		FontMetrics fontMetrics = g.getFontMetrics(g.getFont());
		int textWidth = fontMetrics.getStringBounds(text, g).getBounds().width;
		int textHeight = fontMetrics.getStringBounds(text, g).getBounds().height;

		//Draws a button as a background for the textbox, with width 1.1 times the text width. This is some padding due to the curves edges.
		drawButton(g, x, y, (int) (textWidth * 1.1), textHeight, boxColor);
		//Draws the text in the textbox. Add 0.05 times the text width to center it in the text box.
		g.setColor(textColor); //Use the color specified for the text color
		g.drawString(text, (int) (x + (textWidth * 0.05)), (int) (y + fontMetrics.getAscent()));
		
		//Restores the old font and color
		g.setFont(originalFont);
		g.setColor(originalColor);
	}

	/**
	 * Draws a round button with an outline using the color at the specified coordinates
	 */
	public static void drawButton(Graphics g, int x, int y, int width, int height, Color color) {
		Color originalColor = g.getColor();
		g.setColor(color);
		
		g.fillRoundRect(x, y, width, height, 30, 30); //Draw the outline for the button

		//Darkens the color specified to draw the inside of the text box. Limit method prevents the value from being too small or too large
		int newRed = limit(color.getRed() - 20, 0, 255);
		int newGreen = limit(color.getGreen() - 20, 0, 255);
		int newBlue = limit(color.getBlue() - 20, 0, 255);
		g.setColor(new Color(newRed, newGreen, newBlue));
		
		g.fillRoundRect(x + (width / 160), y + (height / 20), width - (width / 80), height - (height / 10), 30, 30); //Draw the inside of the button
		g.setColor(originalColor); //Reset the color to what it was before the method was called
	}
	
	/**
	 * @param min The lower bound
	 * @param max The upper bound
	 * @return the value, but prevents it from being larger than max, or smaller than min
	 */
	private static int limit(int val, int min, int max) {
		if(val > max) val = max;
		if(val < min) val = min;
		return val;
	}

	/**
	 * Draws the outline of the rectangle specified.
	 */
	public static void drawRectangle(Graphics g, Rectangle r) {
		g.drawRect(r.x, r.y, r.width, r.height);
	}

	/**
	 * Draws the rectangle specified.
	 */
	public static void fillRectangle(Graphics g, Rectangle r) {
		g.fillRect(r.x, r.y, r.width, r.height);
	}

	/**
	 * Draws the string in the centre of the screen at the height specified
	 */
	public static void drawCenteredString(Graphics g, String string, int y) {
		int displayWidth = Monopoly.getGameInstance().getDisplay().getInitialWidth();
		drawCenteredString(g, string, y, displayWidth / 2);
	}

	/**
	 * Draws the string onto the screen so that the centre of the string is the centerX
	 */
	public static void drawCenteredString(Graphics g, String string, int y, int centerX) {
		int width = g.getFontMetrics(g.getFont()).getStringBounds(string, g).getBounds().width; //Gets the width of the text in pixels
		g.drawString(string, centerX - width / 2, y);
	}
}
