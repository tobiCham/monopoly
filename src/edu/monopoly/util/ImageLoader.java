package edu.monopoly.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.net.URL;

import javax.imageio.ImageIO;

import com.tobi.guibase.GUIBase;
import com.tobi.guibase.RenderSetting;

import edu.monopoly.Monopoly;

public class ImageLoader {

	/**
	 * Attempts to load an image from the /image folder in the resources. The file extension should be included.
	 * @return The image, or null if there was an error loading, such as the image not existing.
	 */
	public static BufferedImage loadImage(String imageName) {
		try {
			URL url = Monopoly.class.getResource("/image/" + imageName);
			if(url == null) {
				throw new FileNotFoundException("Image '" + imageName + "' is missing");
			}
			return ImageIO.read(url);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @return A scaled version of the image specified.
	 * @param setting The render settings to use. Will determine the quality of the image returned.
	 * 				  For pictures, use a good quality setting, for intentionally pixelated images, use a low setting.
	 */
	public static BufferedImage resize(BufferedImage image, RenderSetting setting, int width, int height) {
		//Create a new image of the correct size with transparent background
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB); 
		
		//Get the graphics of the new image, and draw the old image onto it scaling to the size of the new image.
		Graphics2D g2d = newImage.createGraphics();
		GUIBase.applySettings(g2d, setting);
		g2d.drawImage(image, 0, 0, width, height, null); 
		g2d.dispose();
		
		return newImage;
	}
	
	/**
	 * Returns a scaled version of the image, using ultra settings.
	 */
	public static BufferedImage resize(BufferedImage image, int width, int height) {
		return resize(image, RenderSetting.ULTRA, width, height);
	}
}
