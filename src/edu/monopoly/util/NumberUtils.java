package edu.monopoly.util;

public class NumberUtils {

	/**
	 * @return True if the string is a valid integer, or false if not
	 */
	public static boolean isInt(String val) {
		try {
			Integer.parseInt(val);
			return true;
		} catch(Exception e) {}
		return false;
	}
	
	/**
	 * @return True if the string is a valid floating point number, or false if not
	 */
	public static boolean isDouble(String val) {
		try {
			Double.parseDouble(val);
			return true;
		} catch(Exception e) {}
		return false;
	}
}
