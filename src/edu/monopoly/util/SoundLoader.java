package edu.monopoly.util;

import java.net.URL;

import edu.monopoly.Monopoly;
import edu.monopoly.sound.SoundClip;

public class SoundLoader {

	/**
	 * @return Attempts to load a sound clip from the /sound folder in the resources. The file extension should be included. 
	 * If there are any errors while loading, a SoundClip will still be returned, however it will not be able to play sounds.
	 * You can check this with {@link SoundClip#hasLoaded()}
	 */
	public static SoundClip loadSound(String name) {
		URL url = Monopoly.class.getResource("/sound/" + name);
		return new SoundClip(url, false);
	}
	
	/**
	 * @return Attempts to load music from the /sound /music folder in the resources. The file extension should be included. 
	 * If there are any errors while loading, a SoundClip will still be returned, however it will not be able to play music.
	 * You can check this with {@link SoundClip#hasLoaded()}
	 */
	public static SoundClip loadMusic(String name) {
		URL url = Monopoly.class.getResource("/sound/music/" + name);
		return new SoundClip(url, true);
	}
}
