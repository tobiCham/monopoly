package edu.monopoly.util;

public class StringUtils {

	public static final char POUND_SIGN = '�';
	
	/**
	 * @return A string which has the first letter of each word capitalised. For example: </br>
	 * 'the pink zebra' would be converted to 'The Pink Zebra'
	 */
	public static String format(String string) {
		if(string == null) return null;
		String[] parts = string.split(" ");
		StringBuilder resultant = new StringBuilder();
		for(String part : parts) {
			resultant.append(part.substring(0, 1).toUpperCase()).append(part.substring(1, part.length()).toLowerCase()).append(" ");
		}
		return resultant.toString().trim();
	}
}
